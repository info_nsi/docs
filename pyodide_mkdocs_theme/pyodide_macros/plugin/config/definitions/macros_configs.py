"""
pyodide-mkdocs-theme
Copyleft GNU GPLv3 🄯 2024 Frédéric Zinelli

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.
If not, see <https://www.gnu.org/licenses/>.
"""

from mkdocs.config import config_options as C

from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import HtmlClass, IdeMode

from ..config_option_src import ConfigOptionIdeLink, ConfigOptionSrc, VAR_ARGS
from ..sub_config_src import CommonTreeSrc, SubConfigSrc
from ..macro_config_src import MacroConfigSrc

from .docs_dirs_config import (
    to_page,
    DOCS_CONFIG,
    DOCS_FIGURES,
    DOCS_IDE_DETAILS,
    DOCS_PY_BTNS,
    DOCS_QCMS,
    DOCS_RESUME,
    DOCS_TERMINALS,
)




OP,CLO = '{{', '}}'



CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_IDE_DETAILS) / '#IDE-{name}'

PY_GLOBAL = SubConfigSrc(
    '', elements=(

    ConfigOptionIdeLink(
        'py_name', str, default="", index=0, in_yaml_docs=False,
        docs = """
            Chemin relatif (sans l'extension du fichier) vers le fichier `{exo}.py` et les
            éventuels autres fichiers annexes, sur lesquels baser l'IDE.
        """,
        yaml_desc="""
            Relative path (no extension) toward the `{exo}.py` file for an IDE, terminal, ...
        """
    ),
    ConfigOptionIdeLink(
        'ID', int, in_config=False, docs_type="None|int",
        docs = """
            À utiliser pour différencier deux IDEs utilisant les mêmes fichiers
            [{{annexes()}}](--ide-files), afin de différencier leurs sauvegardes
            (nota: $ID \\ge 0$).
        """,
        yaml_desc="Disambiguate different macro calls using the same underlying files."
    ),
    ConfigOptionIdeLink(
        'SANS', str, default="",
        docs="""
            Pour interdire des fonctions builtins, des méthodes ou des modules : chaîne de noms
            séparés par des virgules et/ou espaces.
        """,
        yaml_desc="""
            Used to forbid the use of builtins, methods or packages in the python code
            (space or comma separated identifiers).
        """
    ),
    ConfigOptionIdeLink(
        'WHITE', str, default="",
        docs="""
            (_\"White list\"_) Ensemble de noms de modules/packages à pré-importer avant que les
            interdictions ne soient mises en place (voir argument `SANS`. _L'argument `WHITE` est
            normalement {{ orange('**obsolète**') }}_).
        """,
        yaml_desc="""
            Names of packages to import automatically in the global scope (to avoid troubles with
            forbidden modules).
        """,
        # yaml_desc="""
        #     Noms de modules/packages à pré-importer avant que les interdictions ne soient mises
        #     en place.
        # """,
    ),
    ConfigOptionIdeLink(
        'REC_LIMIT', int, default=-1,
        docs="""
            Pour imposer une profondeur de récursion maximale. Nota: ne jamais descendre en-dessous
            de 20. La valeur par défaut, `#!py -1`, signifie que l'argument n'est pas utilisé.
        """,
        yaml_desc="Limit the recursion depth (do not use values below 20).",
        # yaml_desc="Limite de la profondeur de récursion (ne pas descendre en-dessous de 20).",
    ),
    ConfigOptionIdeLink(
        'MERMAID', bool, default=False,
        docs="""
            Signale qu'un rendu de graphe mermaid sera attendu à un moment ou un autre des exécutions.
        """,
        yaml_desc="Mark a page as containing dynamic Mermaid graphs built during executions."
    ),
))




MOST_LIKELY_USELESS_ID = PY_GLOBAL.ID.copy_with(docs="""
    À utiliser pour différencier deux appels de macros différents, dans le cas où vous tomberiez
    sur une collision d'id (très improbable, car des hachages sont utilisés. Cet argument ne
    devrait normalement pas être nécessaire pour cette macro).
""")



def _py_globals_copy_gen(**replacements:ConfigOptionIdeLink):
    return (
        (arg if name not in replacements else replacements[name]).copy_with()
        for name,arg in PY_GLOBAL.subs_dct.items()
    )





#----------------------------------------------------------------------------------------




BS_MACRO = '" + back_slash() + "'
"""
The pretty well named... XD
Necessary to bypass the jinja deprecation warning when using backslashes where it
doesn't like it...
"""



IDE = MacroConfigSrc(
    'IDE',
    docs = "Valeurs par défaut pour les arguments des macros `IDE` et `IDEv`.",
    yaml_desc = "Default values for arguments used in the `IDE` and `IDEv` macros.",
    docs_page_url = to_page(DOCS_IDE_DETAILS),
    elements = (

    *PY_GLOBAL.subs_dct.values(),
    ConfigOptionIdeLink(
        'MAX', int, default=5, docs_type="int|'+'",
        docs="""
            Nombre maximal d'essais de validation avant de rendre la correction et/ou les
            remarques disponibles.
        """,
        yaml_desc="Maximum number of attempts before revealing correction and remarks.",
    ),
    ConfigOptionIdeLink(
        'LOGS', bool, default=True,
        docs="""
            {{ red('Durant des tests de validation') }}, si LOGS est `True`, le code complet
            d'une assertion est utilisé comme message d'erreur, quand l'assertion a été écrite
            sans message.
        """,
        yaml_desc = """
            Build or not missing assertion messages for failed assertions in the secret tests
        """,
        # yaml_desc="""
        #     Construit ou non les messages manquant pour les assertions échouées lors des
        #     validations.
        # """,
    ),
    ConfigOptionIdeLink(
        'MODE', str, is_optional=True,
        conf_type = C.Choice((IdeMode.no_reveal, IdeMode.no_valid)),
        docs_type='None|str',
        docs_default_as_type=True,
        line_feed_link=False,
        docs = f"""
            Change le mode d'exécution des codes python. Les modes disponibles sont :<br>
            { OP } ul_li([
                "`#!py None` : exécutions normales.",
                "`#!py {IdeMode.no_reveal!r}` : exécutions normales, mais les solutions et
                remarques, si elles existent, ne sont jamais révélées, même en cas de succès.
                Le compteur d'essais est ${ BS_MACRO }infty$.",
                "`#!py {IdeMode.no_valid!r}` : quels que soient les fichiers/sections
                disponibles, le bouton et les raccourcis de validations sont inactifs.
                Le compteur d'essais est ${ BS_MACRO }infty$.",
            ]) { CLO }
        """,
        yaml_desc = f"""
            Change the execution  mode of an IDE (`{IdeMode.no_reveal!r}`, `{IdeMode.no_valid!r}`,
            by default: `null`).
        """,
        # yaml_desc = f"""
        #     Change le mode d'exécution de l'IDE (`{IdeMode.no_reveal!r}`, `{IdeMode.no_valid!r}`,
        #     `null` par défaut).
        # """,
    ),
    ConfigOptionIdeLink(
        'MIN_SIZE', int, default=3,
        docs = "Nombre de lignes minimal de l'éditeur.",
        yaml_desc = "Minimum number of lines of an editor.",
    ),
    ConfigOptionIdeLink(
        'MAX_SIZE', int, default=30,
        docs = "Impose la hauteur maximale possible pour un éditeur, en nombres de lignes.",
        yaml_desc = "Maximum number of lines of an editor.",
    ),
    ConfigOptionIdeLink(
        'TERM_H', int, default=10,
        docs = "Nombre de lignes initiales utilisées pour la hauteur du terminal (approximatif).",
        yaml_desc="Initial number of lines of a terminal (approximative).",
    ),
))








CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_TERMINALS) / '#signature'


TERMINAL = MacroConfigSrc(
    'terminal',
    docs = "Valeurs par défaut pour les arguments de la macro `terminal`.",
    yaml_desc = "Default values for arguments used in the `terminal` macro.",
    elements=(

    *_py_globals_copy_gen(
        ID = MOST_LIKELY_USELESS_ID,
        py_name = PY_GLOBAL.py_name.copy_with(docs="""
            Crée un terminal isolé utilisant le fichier python correspondant (sections
            autorisées: `env`, `env_term`, `post_term`, `post` et `ignore`).
        """)
    ),
    ConfigOptionIdeLink(
        'TERM_H', int, default=10,
        docs = "Nombre de lignes initiales utilisées pour la hauteur du terminal (approximatif).",
        yaml_desc="Initial number of lines of a terminal (approximative).",
    ),
    ConfigOptionSrc(
        'FILL', str, default='',
        docs = """
            Commande à afficher dans le terminal lors de sa création.
            <br>{{ red('Uniquement pour les terminaux isolés.') }}
        """,
        yaml_desc="Command used to prefill the terminal (isolated terminals only).",
        # yaml_desc="Commande pour préremplir le terminal (terminaux isolés uniquement).",
    ),
))






*PY_BTN_POSITIONAL, PY_BTN_MERMAID= *(
        arg.copy_with(in_macros_docs=arg.name in ('py_name', 'ID', 'MERMAID'))
        for arg in _py_globals_copy_gen(
            ID = MOST_LIKELY_USELESS_ID,
            py_name = PY_GLOBAL.py_name.copy_with(docs="""
                Crée un bouton isolé utilisant le fichier python correspondant
                (uniquement `env` et `ignore`).
            """)
        )
    ),




CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_PY_BTNS) / '#signature'


PY_BTN = MacroConfigSrc(
    'py_btn',
    docs = "Valeurs par défaut pour les arguments de la macro `py_btn`.",
    yaml_desc = "Default values for arguments used in the `py_btn` macro.",
    elements=(

    *PY_BTN_POSITIONAL,
    ConfigOptionSrc(
        'ICON', str, default="",
        docs = """
            Par défaut, le bouton \"play\" des tests publics des IDE est utilisé.
            <br>Peut également être une icône `mkdocs-material`, une adresse vers une image
            (lien ou fichier), ou du code html.<br>Si un fichier est utiliser, l'adresse doit
            être relative au `docs_dir` du site construit.
        """,
        yaml_desc="Image of the button (by default: `play`  / file path / :icon-material: / url).",
        # yaml_desc="Image pour le bouton (`play` par défaut / fichier / :icon-material: / lien).",
    ),
    ConfigOptionSrc(
        'HEIGHT', int, is_optional=True, docs_type="None|int",
        docs = "Hauteur par défaut du bouton.",
        yaml_desc="Default height for the button",
    ),
    ConfigOptionSrc(
        'WIDTH', int, is_optional=True, docs_type="None|int",
        docs = "Largeur par défaut du bouton.",
        yaml_desc="Default width for the button",
    ),
    ConfigOptionSrc(
        'SIZE', int, is_optional=True, docs_type="None|int",
        docs = "Si définie, utilisé pour la largeur __et__ la hauteur du bouton.",
        yaml_desc="If given, define the height and the width for the button",
    ),
    ConfigOptionSrc(
        'TIP', str, lang_default_access='py_btn.msg',
        docs = "Message à utiliser pour l'info-bulle.",
        yaml_desc="Tooltip message",
    ),
    ConfigOptionSrc(
        'TIP_SHIFT', int, default=50,
        docs = """
            Décalage horizontal de l'info-bulle par rapport au bouton, en `%` (C'est le
            décalage vers la gauche de l'info-bulle par rapport à son point d'encrage de
            la flèche au-dessus de celle-ci. `50%` correspond à un centrage).
        """,
        yaml_desc="Horizontal leftward shifting of the tooltip (%)",
        # yaml_desc="Décalage horizontal de l'info-bulle vers la gauche (%)",
    ),
    ConfigOptionSrc(
        'TIP_WIDTH', float, default=0.0,
        docs = "Largeur de l'info-bulle, en `em` (`#!py 0` correspond à une largeur automatique).",
        yaml_desc="Tooltip width (in em units. Use `0` for automatic width)",
    ),
    ConfigOptionSrc(
        'WRAPPER', str, default='div',
        docs = "Type de balise dans laquelle mettre le bouton.",
        yaml_desc = "Tag type the button will be inserted into",
    ),
    PY_BTN_MERMAID
))





CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_RESUME) / '#section'

SECTION = MacroConfigSrc(
    'section',
    docs = "Valeurs par défaut pour les arguments de la macro `terminal`.",
    yaml_desc = "Default values for arguments used in the `section` macro.",
    in_yaml_docs = False,
    elements = (

    # Required on the python side, but should never be given through "meta", so it has to be
    # non blocking on the config side:
    PY_GLOBAL.py_name.copy_with(
        docs="[Fichier python {{ annexe() }}](--ide-files).",
    ),
    ConfigOptionSrc(
        'section', str, index=1, is_optional=True,
        docs = "Nom de la section à extraire.",
        yaml_desc="Name of the section to extract.",
    ),
))





CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_RESUME) / '#py'

PY = MacroConfigSrc(
    'py',
    docs = "Valeurs par défaut pour les arguments de la macro `py`.",
    yaml_desc = "Default values for arguments used in the `py` macro.",
    in_yaml_docs = False,
    elements = (

    # Required on the python side, but should never be given through "meta", so it has to be
    # non blocking on the config side:
    ConfigOptionSrc(
        'py_name', str, is_optional=True, index=0,
        docs = "Fichier source à utiliser (sans l'extension).",
        yaml_desc="Relative path, but without extension, of the python file to use.",
    ),
))





CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_QCMS) / '#arguments'


MULTI_QCM = MacroConfigSrc(
    'multi_qcm',
    docs = "Valeurs par défaut pour les arguments de la macro `multi_qcm`.",
    yaml_desc = "Default values for arguments used in the `multi_qcm` macro.",
    elements = (

    # Required on the python side, but should never be given through "meta": must not be blocking:
    ConfigOptionSrc(
        '*inputs', list, index=VAR_ARGS, in_config=False, docs_default_as_type=False,
        docs = """
            Chaque argument individuel est une [liste décrivant une question avec ses choix
            et réponses](--qcm_question).
        """,
        yaml_desc="Varags: each element is a list representing the data for one question (3 or 4 elements).",
    ),
    ConfigOptionSrc(
        'description', str, default='',
        docs = """
            Texte d'introduction (markdown) d'un QCM, ajouté au début de l'admonition, avant
            la première question. Cet argument est optionnel.
        """,
        yaml_desc="Introduction text at the beginning of the quiz admonition.",
        # yaml_desc="Texte d'introduction au début de l'admonition du QCM.",
    ),
    ConfigOptionSrc(
        'hide', bool, default=False,
        docs = """
            Si `#!py True`, un masque apparaît au-dessus des boutons pour signaler à l'utilisateur
            que les réponses resteront cachées après validation.
        """,
        yaml_desc="Indicates whether correct/incorrect answers are visible or not after validation.",
        # yaml_desc="Indique si les réponses correctes/incorrects sont visibles à la correction.",
    ),
    ConfigOptionSrc(
        'multi', bool, default=False,
        docs = """
            Réglage pour toutes les questions du qcms ayant à ou un seul choix comme bonne
            réponse, indiquant si elles sont à choix simples ou multiples.
        """,
        yaml_desc="Disambiguate MCQ and SCQ if not automatically decidable.",
        # yaml_desc="Permet de clarifier entre QCM et QCU quand ambiguë.",
    ),
    ConfigOptionSrc(
        'shuffle', bool, default=False,
        docs = "Mélange les questions et leurs choix ou pas, à chaque fois que le qcm est joué.",
        yaml_desc="Shuffle questions and their items or not.",
    ),
    ConfigOptionSrc(
        'shuffle_questions', bool, default=False,
        docs = "Change l'ordre des questions uniquement, à chaque fois que le qcm est joué.",
        yaml_desc="Shuffling or not, questions only.",
    ),
    ConfigOptionSrc(
        'shuffle_items', bool, default=False,
        docs = "Mélange seulement les items des questions, à chaque fois que le qcm est joué.",
        yaml_desc="Shuffling the items of each question or not.",
    ),
    ConfigOptionSrc(
        'admo_kind', str, default="!!!",
        docs = """
            Type d'admonition dans laquelle les questions seront rassemblées (`'???'` et `'???+'`
            sont également utilisables, pour des qcms repliés ou \"dépliés\").
        """,
        yaml_desc="Type of the admonition wrapping the whole MCQ (`!!!`, ...).",
        # yaml_desc="Type d'admonition pour le QCM complet (`!!!`, ...).",
    ),
    ConfigOptionSrc(
        'admo_class', str, default="tip",
        docs = """
            Pour changer la classe d'admonition. Il est également possible d'ajouter d'autres
            classes si besoin, en les séparant par des espaces (ex: `#!py 'warning my-class'`).
        """,
        yaml_desc="Html class(es) for the admonition wrapping the whole MCQ (default: `tip`).",
        # yaml_desc="Classe(s) utilisée(s) pour l'admonition du QCM complet (défaut: `tip`).",
    ),
    ConfigOptionSrc(
        'qcm_title', str, lang_default_access="qcm_title.msg",
        docs = "Pour changer le titre de l'admonition.",
        yaml_desc="Override the default title of the MCQ admonition.",
    ),
    ConfigOptionSrc(
        'tag_list_of_qs', str, conf_type=C.Choice(('ul', 'ol')), is_optional=True,
        docs = """
            {{ ul_li([
                '`#!py None` : automatique (défaut).',
                '`#!py "ol"` : questions numérotées.',
                '`#!py "ul"` : questions avec puces.',
            ]) }}
            Définit le type de liste html utilisée pour construire les questions.
            <br>Si la valeur est `None`, '`#!py "ol"` est utilisé, sauf s'il n'y a qu'une
            seule question pour le qcm, où c'est alors `#!py "ul"` qui est utilisé.
        """,
        yaml_desc="Enforce the list tag used to build the questions in a MCQ.",
    ),
    ConfigOptionSrc(
        'DEBUG', bool, default=False,
        docs = "Si `True`, affiche dans la console le code markdown généré pour ce qcm.",
        yaml_desc="""
            If `True`, the generated markdown of the MCQ will be printed to the console
            during mkdocs build.
        """,
    ),
))







CommonTreeSrc.DEFAULT_DOCS_URL_TEMPLATE = to_page(DOCS_FIGURES) / '#signature'


FIGURE = MacroConfigSrc(
    'figure',
    docs = "Valeurs par défaut pour les arguments de la macro `figure`.",
    yaml_desc = "Default values for arguments used in the `figure` macro.",
    elements = (

    # Required on the python side, but should never be given through "meta": must not be blocking:
    ConfigOptionSrc(
        'div_id', str, default="figure1", index=0,
        docs = """
            Id html de la div qui accueillera la figure ou l'élément inséré dynamiquement.
            <br>À modifier s'il y a plusieurs figures insérées dans la même page.
        """,
        yaml_desc="""
            Html id of the `div` tag that will hold the dynamically generated figure
            (default: `\"figure1\"`).
        """,
    ),
    ConfigOptionSrc(
        'div_class', str, default=HtmlClass.py_mk_figure,
        docs = """
            Classe html à donner à la div qui accueillera la figure.<br>Il est possible d'en
            changer ou d'en mettre plusieurs, selon les besoins. Il est aussi possible de
            surcharger les règles css de la classe par défaut, pour obtenir l'affichage voulu.
        """,
        yaml_desc="Html class of  `div` tag that will hold the dynamically generated figure.",
        # yaml_desc="Classe html à donner à la div qui accueillera la figure.",
    ),
    ConfigOptionSrc(
        'inner_text', str, lang_default_access="figure_text.msg",
        docs = "Texte qui sera affiché avant qu'une figure ne soit tracée.",
        yaml_desc="Text used as placeholder before any figure is inserted.",
    ),
    ConfigOptionSrc(
        'admo_kind', str, default="!!!",
        docs = """
            Type d'admonition dans laquelle la figure sera affichée (`'???'` et `'???+'`
            sont également utilisables, pour des qcms repliés ou \"dépliés\").
            <br>Si `admo_kind` est `''`, la `<div>` sera ajoutée sans admonition (et les
            arguments suivants seront alors ignorés).
        """,
        yaml_desc="Type of the admonition wrapping the generated figure (`!!!`, ...).",
        # yaml_desc="Type d'admonition pour la figure (`!!!`, ...).",
    ),
    ConfigOptionSrc(
        'admo_class', str, default="tip",
        docs = """
            Pour changer la classe d'admonition. Il est également possible d'ajouter d'autres
            classes si besoin, en les séparant par des espaces (ex: `#!py 'warning my-class'`).
        """,
        yaml_desc="Html class(es) of the admonition wrapping the generated figure (default: `tip`).",
        # yaml_desc="Classe(s) utilisée(s) pour l'admonition de la figure (défaut: `tip`)."
    ),
    ConfigOptionSrc(
        'admo_title', str, lang_default_access="figure_admo_title.msg",
        docs = "Pour changer le titre de l'admonition.",
        yaml_desc="Admonition title.",
    ),
))






ARGS_MACRO_CONFIG = SubConfigSrc(
    'args',
    docs_page_url = to_page(DOCS_CONFIG) / '#{py_macros_path}',
    docs = """
    Réglages des arguments par défaut accessibles pour les différentes macros du thème.

    Explications détaillées dans la page [Aide rédacteurs/Résumé](--redactors/resume/).
    """,
    yaml_desc = """
        Configurations of default values for arguments used in `PyodideMacrosPlugin` macros.
    """,
    # yaml_desc = "Configurations des arguments par défaut pour les différentes macros du thème.",
     elements = (
        IDE,
        TERMINAL,
        PY_BTN,
        SECTION,
        MULTI_QCM,
        PY,
        FIGURE,
     )
)
