# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code --- #
# First run fails in secrets, second passes

# --- PYODIDE:corr --- #
print('...yup!')

# --- PYODIDE:tests --- #
if do_it(0): assert False

# --- PYODIDE:secrets --- #
if do_it(0): assert False
if do_it(1,True): print("reset!")