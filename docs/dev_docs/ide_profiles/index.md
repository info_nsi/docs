---
ides:
    deactivate_stdout_for_secrets: false
---


_Note: stdout visible in secrets !_

---

## `MODE = IdeMode.no_reveal`

{{ IDE_py('exo', MODE='no_reveal', before='!!!', admo_kls='tip w45 inline end')}}

!!! tip "Ce qui est testé"

    * Compteur $\infty$
    * Bouton de validation présent
    * {{red("__Pas de révélation même sur un succès__")}}
    * `corr_btn` présent et fonctionnel {{red("avec révélation !")}}



## `MODE = IdeMode.no_reveal`

{{ IDE_py('exo', MODE='no_valid', ID=2, before='!!!', admo_kls='tip w45 inline end')}}


!!! tip "Ce qui est testé"

    * Compteur $\infty$
    * Bouton de validation ABSENT et ++ctrl+enter++ non fonctionnel
    * `corr_btn` présent et fonctionnel {{red("avec révélation !")}}
