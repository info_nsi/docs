---
author:
    - Charles Poulmaire
    - Franck Chambon
hide:
    - navigation
    - toc
title: Tabs checks
tags:
    - booléen
---


!!! tip "raisons..."
    Les codes html générés pour les corr et/ou rem doivent rester valides malgré le bordel que material-mkdocs y met quand il génère le code des admonitions: il ferme automatiquement la div interne à la remarque (celle utilisée pour le formatage), et ce quelle que soit la façon de rédiger le md de départ, dans `Ide.__build_corr_and_rems(...)`.

    Donc cette méthode _**doit**_ générer un html invalide, pour qu'il devienne valide par la suite

    NOTE:

    * il est probable que le problème ne se poserait pas s'il n'y avait pas la div à l'intérieur de l'admonition
    * le problème ne se révèle que lors de l'utilisation d'outils complexes, comme les tabs, ou l'utilisation d'un outil comme beautifulsoup, qui va fermer automatiquement tous les tags invalides...
    * cela pourrait vouloir dire que le problème se pose aussi plus loin si on imbrique plus de niveaux...?

!!! tip "Ce qui est testé"

    * Les tabs doivent marcher quelle que soit la config corr/rem d'un IDE.
    * on utilise un second jeu de tabs plus bas pour vérifier que tout est bon


=== "Corr+Rem with àccénts"
    {{ IDE('tab1', MAX_SIZE=2, MAX=1) }}
=== "Rem"
    {{ IDE('tab2', MAX_SIZE=2, MAX=1) }}
=== "Corr"
    {{ IDE('tab3', MAX_SIZE=2, MAX=1) }}
=== "Neither..."
    {{ IDE('tab4', MAX_SIZE=2, MAX=1) }}

---

!!! tip "Nesting more..."
    === "Corr+Rem"
        {{ IDE('tab1', ID = 2, MAX_SIZE=2, MAX=1) }}
    === "Rem"
        {{ IDE('tab2', ID = 2, MAX_SIZE=2, MAX=1) }}

---

=== "This should..."
    does it?
=== "...still work"
    does it?

### This title should be visible