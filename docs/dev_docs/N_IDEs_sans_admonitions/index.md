---
author:
    - Nicolas Revéret
    - Romain Janvier
hide:
    - navigation
    - toc
difficulty: 50
title: N IDEs sans admonitions
tags:
    - programmation orientée objet
---

# Structure de fichiers avec le découpage du script de Romain (cas complexe)


!!! tip "ce qu'on devrait voir"
    * les 7 IDEs + le dernier avec le fichier complet
    * les boutons d'un IDE n'affectent pas les autres IDE
    * le dernier IDe devrait pouvoir être exécuté indépendamment des autres (aka, le HDR est déjà "enregistré")

exo_1

{{ IDE('exo_1') }}

exo_2

{{ IDE('exo_2') }}

exo_3

{{ IDE('exo_3') }}

exo_4

{{ IDE('exo_4') }}

exo_5

{{ IDE('exo_5') }}

exo_6

{{ IDE('exo_6') }}

exo_7

{{ IDE('exo_7') }}

---

# Import exo.py complet


!!! tip "ce qu'on devrait voir"
    * l'IDE complet avec le code demandant d'abord de `# Créez un personnage nommé "Guybrush"...`
    * le code de la partie HDR du fichier ne doit pas apparaître
    * le code doit passer le premier test (ie le contenu de HDR doit être en mémoire)

{{ IDE('exo') }}


### This title should be visible