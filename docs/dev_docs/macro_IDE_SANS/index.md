---
author:
    - Charles Poulmaire
    - Franck Chambon
hide:
    - navigation
    - toc
title: Macro IDE exclusions
tags:
    - à trous
    - boucle
    - booléen
---

# Vérifie que les différents arguments de la macro IDE sont utilisés correctement

!!! warning "Exclusions posant problèmes"
    * quand une erreur est levée, son formatage se fait encore dans l'environnement avec les restrictions
    => il ne faut pas utiliser un truc susceptible d'être interdit dans le code pyodide des exclusions !
    * `SANS='str type isinstance any'`

    {{ IDE_py('str', SANS='str any isinstance type') }}


=== "REC_LIMIT+SANS=funcs"

    !!! tip "Ce qui est testé"
        * `REC_LIMIT=50, SANS = "exec eval min max reversed .find"`
        * eval call fails
        * exec call not used => OK
        * pas de `white_list` => numpy import fails
        * les erreurs levée dans le code ne doivent pas poser de problèmes avec pyodide

        {{ IDE_py('exo', TERM_H=17, REC_LIMIT=50, SANS="exec eval min max reversed .find")}}


=== "SANS=import"

    !!! tip "Ce qui est testé"
        * `SANS="numpy heapq"`
        * Interdit un module déjà installé
        * toutes formes d'imports de numpy sont interdites
        * autres modules accessibles (maths)

    {{ IDE_py('import_numpy', SANS="numpy heapq", TERM_H=20)}}


=== "WHITE='numpy'"

    !!! tip "Ce qui est testé"
        * `SANS="numpy heapq"`
        * L'installation de numpy marche via WHITE
        * Installé une seule fois
        * malgré tout impossible d'importer explicitement dans `code`
        * mais module disponible (le dernier test doit planter)

    {{ IDE_py('import_numpy', ID=2, SANS="numpy heapq", TERM_H=20, WHITE='numpy')}}


=== "SANS='pydantic'"

    !!! tip "Ce qui est testé"
        * N'installe pas un module interdit
        * mais installe tout de même un autre module (bs4)

    {{ IDE_py('import_bs4_pydantic', SANS="pydantic")}}


=== "SANS='pydantic', env as WHITE"

    !!! tip "Ce qui est testé"
        * N'installe pas un module interdit
        * mais installe tout de même un autre module (bs4)

    {{ IDE_py('import_env_pydantic', SANS="pydantic")}}



### This title should be visible