# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code --- #
def todo():
    return eval('32')       # definition is harmless

def rec(limit=80, n=0):
    return rec(limit, n+1) if n<limit else 1

available(lambda: eval)                 # No execution
available(eval)                         # No execution
should_raise(todo, ExclusionError)
f = min                                 # No execution
available(f)                            # No execution
should_raise(f, ExclusionError)
should_raise(rec, RecursionError)
available(rec(20))                      # Works below the rec limit
available(exec)                         # No execution

