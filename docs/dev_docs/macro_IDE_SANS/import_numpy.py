# --------- PYODIDE:env --------- #
from py_libs import *
auto_N(globals())

# --------- PYODIDE:code --------- #

should_raise(lambda: __import__('heapq'), ExclusionError)
not_defined('heapq', "shouldn't be in scope")

def f1(): import numpy
def f2(): import numpy as np
def f3(): from numpy import array
def f4(): from numpy.random import bit_generator
def f5(): __import__('numpy')
def f6(): import math ; return math

for f in  (f1,f2,f3,f4,f5):
    should_raise(f, ExclusionError)
available(f6())

print("fail if WHITE:")
not_defined('numpy')
    # WHITE: Fails IF run before the previous tab. Doesn't if run after,
    # because micropp doesn't reinstall, so no "force import in scope" of PMT
