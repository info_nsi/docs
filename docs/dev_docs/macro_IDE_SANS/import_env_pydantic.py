# --------- PYODIDE:env --------- #
from py_libs import *

def import_in_env_func():
    import pydantic

    def extractor():
        return pydantic
    return extractor

pydantic_extractor = import_in_env_func()

# --------- PYODIDE:code --------- #
# pydantic imported from env (inside a function)
not_defined('pydantic')                             # imported, but not in scope
should_raise(import_in_env_func, ExclusionError)    # still not importable
available(pydantic_extractor())                     # ... available in env

