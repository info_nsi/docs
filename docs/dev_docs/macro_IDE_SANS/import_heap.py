# --- PYODIDE:env -- #
from py_libs import *


# --- PYODIDE:code -- #
should_raise(lambda: __import__('heapq'), ExclusionError)
should_raise(lambda: heapq, NameError)
