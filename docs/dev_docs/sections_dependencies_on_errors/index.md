---
ides:
    deactivate_stdout_for_secrets: false
---

# Env & Post

## Comportements des sections `env` et `post` avec AssertionError ou d'autres erreurs.

!!! tip "Ce qui est testé"

    * Utiliser ++ctrl+enter++ uniquement.
    * Teste quelles sections sont effectivement exécutées ou pas, en faisant des asserts sur le contenu de la stdout => semi automatisé: il n'y a qu'(à faire Ctrl+Enter jusqu' voir X=8 dans un ValueError)
    * des sécurités sont posées de façons à ce qu'une section qui n'est pas sensée s'exécuter lève une erreur (sans doute plus très utile vu comment j'ai upgradé les tests sur la stdout, mais bon...)

    Les erreurs levées sont les suivantes selon la valeur de `X`:

    1. aucune
    1. `env` : `AssertionError`{.orange}
    1. `env` : `KeyError`{.red}
    1. `secrets` : `AssertionError`{.orange}
    1. `secrets` : `KeyError`{.red}
    1. `post` : `AssertionError`{.orange}
    1. `post` : `KeyError`{.red}
    8. `ValueError` et "loop back".

<br>



{{ IDE_py('exo', MAX=45, TERM_H=15) }}
