# --------- PYODIDE:env_term --------- #
print('env_term!')
# --------- PYODIDE:post_term --------- #
print('post_term!')

# --------- PYODIDE:env --------- #

from py_libs import *
auto_N(globals(), 'X', reset_on=8)
if X==8:
    print("X=1 au tour suivant!")
    raise ValueError(f'>>>OK | {X=}<<<')


std = StdOutAsserter()
def should_never_go_here(*vs):
    assert X not in vs, f"This should never have been executed... ({X=})"

if do_it(): print('Nothing done: just skip installation messages')

print('env', end='')
std.gather()
threw = False
try:
    if do_it(): assert False, f"assertion from env >>>OK | {X=}<<<"
    if do_it(): raise KeyError(f"RAISING! from env >>>OK | {X=}<<<")
except:
    threw=True
    raise
else:
    print('+')
    should_never_go_here(2,3)
finally:
    std.gather()
    if X in (2,3):
        std.check('envenv', True)

# --------- PYODIDE:code --------- #
print('code', X)
std.gather()
should_never_go_here(2,3)

EXPS=[
    '',
    '',
    'env env post post+',
    'env',     # Actually not used. Some tests done directly in env
    'env env+ code 4 secrets post post+',
    'env env+ code 5 secrets post post+',
    'env env+ code 6 secrets secrets+ post post',
    'env env+ code 7 secrets secrets+ post post',
]
# --------- PYODIDE:secrets --------- #
print('secrets', end='')
std.gather()
if do_it(): assert False, f"assertion from secrets >>>OK | {X=}<<<"
if do_it(): raise KeyError(f"RAISING! from secrets >>>OK | {X=}<<<")
print('+')
std.gather()
should_never_go_here(2,3)

# --------- PYODIDE:post --------- #
print('post', end='')
std.gather()

should_never_go_here(3)

try:
    if do_it(): assert False, f"assertion from post >>>OK | {X=}<<<"
    if do_it(): raise KeyError(f"RAISING! from post >>>OK | {X=}<<<")
except: raise
else:   print('+')
finally:
    std.gather()

    if X<len(EXPS):
        exp =EXPS[X]
        if exp:
            exp = ''.join(c for c in exp if not c.isspace())
            std.check(exp, True)



# --------- PYODIDE:corr --------- #
# (needed for validation)