

# --------- PYODIDE:code --------- #

def nb_lignes(grille):
    return len(...)

def ligne(grille, i):
    return ...

def nb_colonnes(grille):
    return ...

def colonne(grille, j):
    return [grille[...][...] for i in range(...)]

def transposition(grille):
    return [... for j in range(...)]

# --------- PYODIDE:corr --------- #

def nb_lignes(grille):
    return len(grille)

def ligne(grille, i):
    return grille[i]

def nb_colonnes(grille):
    return len(grille[0])

def colonne(grille, j):
    return [grille[i][j] for i in range(nb_lignes(grille))]

def transposition(grille):
    return [colonne(grille, j) for j in range(nb_colonnes(grille))]



# tests

grille = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]
assert nb_lignes(grille) == 3
assert ligne(grille, 0) == [0, 1, 1, 1]
assert nb_colonnes(grille) == 4
assert colonne(grille, 1) == [1, 0, 0]
t_grille = [
    [0, 1, 0],
    [1, 0, 0],
    [1, 1, 0],
    [1, 0, 1],
]
assert transposition(grille) == t_grille
assert transposition(t_grille) == grille

grille = [
    ['a', 'b', 'c'],
    ['d', 'e', 'f'],
    ['g', 'h', 'i'],
    ['j', 'k', 'l'],
]
assert nb_lignes(grille) == 4
assert ligne(grille, 3) == ['j', 'k', 'l']
assert nb_colonnes(grille) == 3
assert colonne(grille, 0) == ['a', 'd', 'g', 'j']
t_grille = [
    ['a', 'd', 'g', 'j'],
    ['b', 'e', 'h', 'k'],
    ['c', 'f', 'i', 'l'],
]
assert transposition(grille) == t_grille
assert transposition(t_grille) == grille

# --------- PYODIDE:tests --------- #

grille = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]
assert nb_lignes(grille) == 3
assert ligne(grille, 0) == [0, 1, 1, 1]
assert nb_colonnes(grille) == 4
assert colonne(grille, 1) == [1, 0, 0]
t_grille = [
    [0, 1, 0],
    [1, 0, 0],
    [1, 1, 0],
    [1, 0, 1],
]
assert transposition(grille) == t_grille
assert transposition(t_grille) == grille

grille = [
    ['a', 'b', 'c'],
    ['d', 'e', 'f'],
    ['g', 'h', 'i'],
    ['j', 'k', 'l'],
]
assert nb_lignes(grille) == 4
assert ligne(grille, 3) == ['j', 'k', 'l']
assert nb_colonnes(grille) == 3
assert colonne(grille, 0) == ['a', 'd', 'g', 'j']
t_grille = [
    ['a', 'd', 'g', 'j'],
    ['b', 'e', 'h', 'k'],
    ['c', 'f', 'i', 'l'],
]
assert transposition(grille) == t_grille
assert transposition(t_grille) == grille

# --------- PYODIDE:secrets --------- #


# autres tests


grille = [
    ['#', '#', '#', '#', '#'],
    ['#', '#', '#', '#', '#'],
    ['.', '.', '#', '#', '#'],
    ['#', '.', '#', '#', '#'],
]
assert nb_lignes(grille) == 4
assert ligne(grille, 3) == ['#', '.', '#', '#', '#']
assert nb_colonnes(grille) == 5
assert colonne(grille, 0) == ['#', '#', '.', '#']
t_grille = [
    ['#', '#', '.', '#'],
    ['#', '#', '.', '.'],
    ['#', '#', '#', '#'],
    ['#', '#', '#', '#'],
    ['#', '#', '#', '#'],
]
assert transposition(grille) == t_grille
assert transposition(t_grille) == grille



une_colonne = [
    [(1, 2, 9)],
    [(3, 4, 9)],
    [(5, 6, 9)],
    [(7, 8, 9)],
]
une_ligne = [
    [(1, 2, 9), (3, 4, 9), (5, 6, 9), (7, 8, 9)],
]
assert nb_lignes(une_colonne) == len(une_colonne)
assert nb_colonnes(une_colonne) == len(une_colonne[0])
assert ligne(une_colonne, 0) == une_colonne[0]
assert colonne(une_colonne, 0) == [une_colonne[i][0] for i in range(len(une_colonne))]
assert transposition(une_ligne) == une_colonne
assert transposition(une_colonne) == une_ligne