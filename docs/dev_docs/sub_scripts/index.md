---
hide:
    - navigation
    - toc
title: Alternative hdd + no tests failure
tags:
    - à trous
    - en travaux
    - programmation orientée objet
---

!!! tip "Ce qui est testé"
    * Les macros arrivent à trouver les fichiers dans `./scripts/nom_exo...`
    * Durant le build, un message d'erreur doit apparaître comme quoi un IDE avec un fichier de REM est utilisé, mais sans fichier de tests (ce qui veut dire que pas de bouton de validation, donc pas possible de voir la remarque).

    Message attendu: `exercices/sub_scripts/: a correction or remark file exists but there is no "exo_test.py" file.`


Here they are...


=== "Theme way"

    {{ IDE("exo") }}


    !!! tip "Teste d'autres macros"

        `py('exo')` :

        {{ py('exo') }}

        `section('exo', 'code')` :
        {{ section('exo', 'code') }}


=== "Old way"

    {{ IDE("exo_old_way") }}


    !!! tip "Teste d'autres macros"

        `py('exo_old_way')` :

        {{ py('exo_old_way') }}

        `section('exo_old_way', 'env')` :
        {{ section('exo_old_way', 'env') }}



### This title should appear
