---
ide_py_admo: '!!!'
ides:
    forbid_corr_and_REMs_with_infinite_attempt: false
    forbid_hidden_corr_and_REMs_without_secrets: false
    forbid_secrets_without_corr_or_REMs: false
    forbid_corr_and_REMs_with_infinite_attempts: false
---

## _RESET THE LOCALE STORAGE FIRST_ { .red }


Tabs names: `ABCDE` as `VIS_REM, REM, corr, secrets, py_file`




=== "00000"

    !!! tip "ce qu'on devrait voir"
        * un IDE vide
        * les tests publics marchent
        * pas de bouton `Valider`
        * pas de corr btn

    {{ IDE(MAX=1) }}



=== "00001"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide
        * pas de bouton de validation
        * compteur d'essais infini (même si MAX=1)
        * les tests publics marchent
        * pas de correction ni remarque
        * pas de corr btn

    {{ IDE_py('exo00001', MAX=1) }}



=== "00011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide
        * avec tous les boutons, y compris celui de validation
        * compteur d'essais à l'infini, car rien à révéler (bien que MAX=1)
        * les tests publics marchent
        * la validation marche
        * pas de correction ni remarque
        * En renvoyant `True` lors des validations, le message `Bravo!` doit apparaître à chaque fois.
        * pas de corr btn
        * doit générer un message d'erreur dans la console lors du build avec la config par défaut du plugin (`ides.forbid_corr_and_REMs_with_infinite_attempts`)

    {{ IDE_py('exo00011', MAX=1) }}



=== "00111"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * correction visible, mais pas de remarque
        * MAX = 2
        * corr btn visible

    {{ IDE_py('exo00111', MAX=2) }}



=== "00101, MAX=1"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, pas de bouton check
        * les tests publics marchent
        * doit générer un message d'erreur dans la console lors du build, avec la config par défaut du plugin (`ides.forbid_hidden_corr_and_REMs_without_secrets`)
        * MAX=1 (arg), mais compteur d'essais à $\infty$ (car pas de secrets)
        * corr btn visible (nota: aucun test nulle part => passe)

    {{ IDE_py('exo00101', MAX=1) }}



=== "00111, MAX=$\infty$"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * doit générer un message d'erreur dans la console lors du build avec la config par défaut du plugin (`ides.forbid_corr_and_REMs_with_infinite_attempts`)
        * MAX = $\infty$
        * corr btn visible (but tests fails cause invalid)

    {{ IDE_py('exo00111', MAX="+", ID=1) }}



=== "01011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * la remarque visible, mais pas de correction
        * MAX = 1
        * pas de corr btn
        * On validation: `Dommage ! Les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo01011', MAX=1) }}



=== "01111"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * correction et remarque visibles
        * MAX = 1
        * corr btn visible
        * On validation: `Dommage ! Le corrigé et les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo01111', MAX=1) }}



=== "10011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * VIS_REM visibles
        * message dans le terminal tient compte de VIS_REM seulement
        * MAX = 1
        * pas de corr btn
        * On validation: `Dommage ! Les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo10011', MAX=1) }}


=== "11011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * REM et VIS_REM visibles
        * messages dans le terminal et l'admonition corrects
        * MAX = 1
        * pas de corr btn
        * On validation: `Dommage ! Les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo11011', MAX=1) }}


=== "10111"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * correction et VIS_REM visibles
        * message dans le terminal tient compte de VIS_REM
        * MAX = 1
        * corr btn
        * On validation: `Dommage !Le corrigé et les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo10111', MAX=1) }}


### This title should be visible
