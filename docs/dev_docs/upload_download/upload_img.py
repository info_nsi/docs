# --- PYODIDE:env --- #
print("env!")

def upload(content:str):
    import js
    js.document.getElementById("img-target2").src = content
    return content


bytes_img = await pyodide_uploader_async(upload, read_as='img')

# --- PYODIDE:code --- #
print(bytes_img)
