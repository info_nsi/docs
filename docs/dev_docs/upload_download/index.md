---
ides:
    deactivate_stdout_for_secrets: false
---

## download csv data

{{ IDE_py('csv') }}

## `copy_from_server`

{{ IDE_py('copy') }}


## uploaders

### Upload async - text - multi

{{ IDE_py("upload-async", before='!!!') }}


### Upload async - bytes

{{ IDE_py("upload_bytes", before='!!!') }}


### Upload async - img

{{ IDE_py("upload_img", before='!!!') }}

{{ figure("img-target2") }}


### Upload sync

{{ IDE_py("upload", before='!!!') }}


### Downloads

Voir [la page de la docs](--pyodide-download)