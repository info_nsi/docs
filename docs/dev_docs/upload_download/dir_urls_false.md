---
ides:
    deactivate_stdout_for_secrets: false
---

## download csv data

{{ IDE_py('csv', ID=22) }}

## `copy_from_server`

{{ IDE_py('copy', ID=22) }}
