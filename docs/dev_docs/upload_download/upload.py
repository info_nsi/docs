# --- PYODIDE:env --- #
print("env!")

# --- PYODIDE:code --- #
if 'data_sync' not in globals():
    data_sync = None

def upload(content:str):
    global data_sync
    data_sync = content[:30]+' [...]'  # available on next run
    print("This will show up in the JS console! (because run from an event)")

pyodide_uploader(upload)  # This is synch, but run later from an asynch context !

print('Not waiting...')
print("data_sync:", data_sync or "NOTHING YET!")      # you see content of previous run!



# --- PYODIDE:post --- #
print("post!")
