---
ides:
    forbid_secrets_without_corr_or_REMs: false
---


## BUT

Tester le comportement de l'extraction automatique des messages d'erreur, en vérifiant au passage que les override/merge de configs via les meta fonctionnent correctement.


{{ meta_tests_config(
    args_IDE_LOGS                           = True,
    args_IDE_MAX                            = 5,
    python_libs                             = ['py_libs','other_py_libs/libxyz'],
    deactivate_stdout_for_secrets           = True,
    decrease_attempts_on_user_code_failure  = "editor",
    show_only_assertion_errors_for_secrets  = False,
) }}



## Pour les tests secrets

!!! tip "AssertionError sur les tests de validation"
    * public: comportement normal quel que soit le réglage
    * secrets:
        - True: construit le message d'AssertionError automatiquement
        - False: pas de message

=== "LOGS=True (always)"
    {{ IDE_py('exo_fail', ID=1, MAX=15, LOGS=True, after="!!!") }}

=== "LOGS=False (never)"
    {{ IDE_py('exo_fail', ID=2, MAX=15, LOGS=False, after="!!!") }}

---

<br>

!!! tip "Effet de `deactivate_stdout_for_secrets`"

    `ides.deactivate_stdout_for_secrets = {{ Env().deactivate_stdout_for_secrets }}`

    `True`, donc, sur ++ctrl+enter++ :

    | section | message| visible |
    |-|-|-|
    | `code` | `#!py 'YEAH!'` | {{ qcm_svg("single correct") }} |
    | `tests` | `#!py 'public'` | {{ qcm_svg("single correct") }} |
    | `secrets` | `#!py 'secrets...'` | {{ qcm_svg("single incorrect") }} |

{{ IDE_py('exo_stdout', ID=1, before="!!!", admo_kls="tip inline end w45") }}



---

<br>

!!! tip "Effet de `decrease_attempts_on_user_code_failure`"

    `ides.decrease_attempts_on_user_code_failure = {{ Env().decrease_attempts_on_user_code_failure }}`

    `True`, donc, sur ++ctrl+enter++ :

    | message| visible |
    |-|-|
    | `#!py `M=1` | `decrease`{.red} |
    | `#!py `M=2` | `decrease`{.red} |
    | `#!py `M=3` | `success`{.green} |
    | `#!py `M=2` | `constant` |


{{ IDE_py('exo_decrease', ID=42, before="!!!", MAX=15, admo_kls="tip inline end w45") }}



---

<br>

!!! tip "Effet de `show_only_assertion_errors_for_secrets`"

    `ides.show_only_assertion_errors_for_secrets = {{ Env().show_only_assertion_errors_for_secrets }}`

    `False`, donc :

    - `tests`:
        - `AssertionError: X = 1, message!`
        - `KeyError: 'X = 2, message!'`

    - `secrets`:
        - `AssertionError: X = 3, message!`
        - `KeyError: 'X = 4, message!'`


{{ IDE_py('exo_trace', before="!!!", admo_kls="tip inline end w45") }}



### This title should be visible