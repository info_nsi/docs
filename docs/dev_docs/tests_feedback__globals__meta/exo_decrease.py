# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals(),'M')

# --- PYODIDE:code --- #
print(f'{M = }')

# --------- PYODIDE:tests --------- #
assert M!=1
# --------- PYODIDE:secrets --------- #
assert M!=2
assert M<4

# --------- PYODIDE:corr --------- #
# Needs a corr...
