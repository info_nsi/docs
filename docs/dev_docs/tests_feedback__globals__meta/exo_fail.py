# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code --- #
print(f'{N = }')

# --------- PYODIDE:tests --------- #
assert N!=1, "ce message est tjrs visible"
assert N!=2  # ce commentaire devrait tjrs être extrait
assert [
    "tjrs visible aussi, extraction multilignes ok."
] and N!=3
assert (
    [
    "tjrs visible",
    "marche avec un formatage bizarre."
] and N!=4
    )

# --------- PYODIDE:secrets --------- #

assert N!=5, "ce message est tjrs visible"
assert N!=6  # ce commentaire devrait être extrait seulement si LOGS est True
assert [
    "extraction multilignes ok., Visible si LOGS=True"
] and N!=7
assert (
    [
    "tjrs visible",
    "marche avec un formatage bizarre."
] and N!=8
    )

# --------- PYODIDE:post --------- #
if N>8: N=0 ; print('(restarted)')
# --------- PYODIDE:corr --------- #
# Need the validation button!
