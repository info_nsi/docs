
# Using {{meta()}} file, no headers


{{ meta_tests_config(
    args_IDE_LOGS                           = True,
    args_IDE_MAX                            = 3,
    python_libs                             = ['py_libs'],
    deactivate_stdout_for_secrets           = False,
    decrease_attempts_on_user_code_failure  = "secrets",
    show_only_assertion_errors_for_secrets  = True,
) }}

Contenu de `docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml`:

```yaml
--8<-- "docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml"
```



---



<br>

!!! tip "Effet de `deactivate_stdout_for_secrets`"

    `ides.deactivate_stdout_for_secrets = {{ Env().deactivate_stdout_for_secrets }}`, donc, sur ++ctrl+enter++ :

    | section | message| visible |
    |-|-|-|
    | `code` | `#!py 'YEAH!'` | {{ qcm_svg("single correct") }} |
    | `tests` | `#!py 'public'` (x2) | {{ qcm_svg("single correct") }} |
    | `secrets` | `#!py 'secrets...'` | {{ qcm_svg("single correct") }} |

{{ IDE_py('../exo_stdout', ID=1, before="!!!", admo_kls="tip inline end w45") }}

---

<br>

!!! tip "Effet de `decrease_attempts_on_user_code_failure`"

    `ides.decrease_attempts_on_user_code_failure = {{ Env().decrease_attempts_on_user_code_failure }}` donc, sur ++ctrl+enter++ :

    | message| visible |
    |-|-|
    | `#!py `M=1` | `constant` |
    | `#!py `M=2` | `decrease`{.red} |
    | `#!py `M=3` | `success`{.green} |
    | `#!py `M=2` | `constant` |



{{ IDE_py('../exo_decrease', ID=1, before="!!!", admo_kls="tip inline end w45") }}



---

<br>

!!! tip "Effet de `show_only_assertion_errors_for_secrets`"

    `ides.show_only_assertion_errors_for_secrets = {{ Env().show_only_assertion_errors_for_secrets }}`, donc, en utilisant ++ctrl+enter++ :

    - Erreurs dans `tests`:
        - `AssertionError: X = 1, message!` + stacktrace
        - `KeyError: 'X = 2, message!'` + stacktrace

    - `secrets`:
        - `AssertionError: X = 3, message!` SANS stacktrace
        - `KeyError has been raised` SANS stacktrace


{{ IDE_py('../exo_trace', ID=1, before="!!!", admo_kls="tip inline end w45") }}





### This title should be visible