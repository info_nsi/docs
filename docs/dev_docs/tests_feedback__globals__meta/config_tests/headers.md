---
args:
  IDE:
    LOGS: false
    MAX: 45
build:
    python_libs: []
ides:
  deactivate_stdout_for_secrets: true
  decrease_attempts_on_user_code_failure: "editor"
  show_only_assertion_errors_for_secrets: false
---

# Using headers (reversing meta file)

{{ meta_tests_config(
    args_IDE_LOGS                           = False,
    args_IDE_MAX                            = 45,
    python_libs                             = [],
    deactivate_stdout_for_secrets           = True,
    decrease_attempts_on_user_code_failure  = "editor",
    show_only_assertion_errors_for_secrets  = False,
) }}


Contenu entêtes:

```yaml
args:
  IDE:
    LOGS: false
    MAX: 45
build:
    python_libs: []
ides:
  deactivate_stdout_for_secrets: true
  decrease_attempts_on_user_code_failure: "editor"
  show_only_assertion_errors_for_secrets: false
```

Contenu de `docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml`:

```yaml
--8<-- "docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml"
```
