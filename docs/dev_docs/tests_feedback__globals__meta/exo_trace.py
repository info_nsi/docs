# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals(), "X")

# --------- PYODIDE:tests --------- #
if do_it():
    assert False, f"{X = }, message!"
if do_it():
    raise KeyError(f"{X = }, message!")

# --------- PYODIDE:secrets --------- #
if do_it():
    assert False, f"{X = }, message!"
if do_it():
    raise KeyError(f"{X = }, message!")

# --------- PYODIDE:corr --------- #
# Needs a corr...
