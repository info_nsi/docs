---
hide:
    - navigation
    - toc
title: Same set of files for multi IDEs (ID disambiguation)
tags:
    - à trous
    - en travaux
    - programmation orientée objet
---

!!! tip "Ce qui est testé"
    Il doit être possible de générer plusieurs IDEs à partir du même jeu de fichiers python si on utilise en plus l'argument ID.


{{ IDE("exo") }}

{{ IDE("exo", ID=1) }}


### This title should appear
