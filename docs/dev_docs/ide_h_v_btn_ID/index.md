---
author: Franck Chambon
hide:
    - navigation
    - toc
title: IDE & IDEv
tags:
    - à trous
    - en travaux
    - grille
---

## PyBtn

{{ md_include("docs_tools/inclusions/py_btns_examples.md") }}


## Fonctionnement des IDE dans les deux layouts (h & v)

!!! tip "ce qu'on devrait voir"
    * l'argument supplémentaire ID ne devrait pas être nécessaire
    * les 2 layouts devraient fonctionner (visuellement)

{{ IDE('exo', MAX=1) }}

{{ IDEv('exo', MAX=1) }}


### This title should be visible
