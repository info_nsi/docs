---
author: Mireille Coilhac
title: QCM Sécurisation des communications
tags:
    - boucle
args:
    multi_qcm:
        hide: false
        multi: false
        shuffle: false
        shuffle_questions: false
        shuffle_items: false
        DEBUG: false
---

## Various formatting tests

### Base case... {.red}


{{ multi_qcm(
    [
        """
```python title=''
n = 8
while n > 1:
    n = n//2
```

`Code block first`{.red}

`(Selecting one unselect the other)`{.orange}

Que vaut `n` après l'exécution du code ?  (correct = 1)
""",
        [
            "1",
            "4",
        ],
        [1]
    ],
    [
        """
        `Table last and backslash in LaTex`{.red} : $a \\times b$ (correct = 1)

        | a | b |
        |-|-|
        | a | b |
        """,
        [
            "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
            "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
            """
            `Table last in item and backslash in LaTex`{ .red } : $a \\times b$

            | a | b |
            |-|-|
            | a | b |
            """,
            "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
        ],
        [1],
    ],
    [
        """
`Code block last`{ .red }

`(Select any number of options)`{.orange}

Quelle expression permet d'accéder au numéro de Tournesol : (correct = 1 & 3)
```python title=''
repertoire = [{'nom': 'Dupont', 'tel': 5234}, {'nom': 'Tournesol', 'tel': 5248}, {'nom': 'Dupond', 'tel': 5237}]
```
        """,
        [
            "`#!python repertoire[1]['tel']`",
            "`#!python repertoire['tel'][1]`",
            "`#!python repertoire['Tournesol']`",
            "`#!python repertoire['Tournesol']['tel']`",
        ],
        [1,3]
    ],
    multi = False,
    qcm_title = "`multi=False, shuffle={} (default)`{}".format(Env().args_multi_qcm_shuffle, '{.red}'),
) }}






### others...

=== "hide?"

    {{ multi_qcm(
        [
            "(corrects: 1&2)",
            [
                "Là aussi on peut mettre du TeX: $F_{n+1}$ ",
                "...",
                "...",
                "...",
            ],
            [1,2]
        ],
        [
            "Blabla... (correct: 4)\n\n`Md lists inside question items + indentation`{.red}",
            [
                "Déchiffrer illégitimement le message",
                "Chiffrer le message",
                """
    On peut aussi mettre de contenus plus complexes, du moment que...:

    * c'est de la syntaxe markdown valide et que...
    * les niveaux d'indentation...
        * dans le contenu...
        * sont cohérents.

    _(Noter que la première ligne vide de cette chaîne de caractère est automatiquement supprimée)_
    """,
                "Dissimuler le message dans un support",
            ],
            [4],
            {'multi': True},
        ],
        [
            """
    ```python title=''
    def block_alone_as_question():    # (Answer is 3)
        return 42
    ```
    """,
            [
                "Alice a besoin de la clé privée de Bob",
                "Alice a besoin de la clé publique de Bob",
                """
    ```python title=''
    # On peut aussi insérer des blocs de code dans les items!
    def func():
        return 26
    ```""",
                "Bob a besoin de la clé privée d'Alice",
            ],
            [3]
        ],
        multi = False,
        hide  = True,
        qcm_title = "`hidden=True, shuffle={} (default)`{}".format(Env().args_multi_qcm_shuffle, '{.red}'),
    ) }}




=== "title singular"

    Automatic title, singular + strings formatting, `multi={{ Env().args_multi_qcm_multi }}, shuffle={{ Env().args_multi_qcm_shuffle }}`{.red}
    {.red}

    {{ multi_qcm(
        [
            "Énoncé première question (1&2 corrects)",
            [
                "```python title='appel inline (illisible)'\n# on pourrait insérer du multi line\ndef func():\n    return 26\n```",
                """
    ```python title='appel avec une multiline string'
    def func():     # Bloc de code indenté au niveau 0
        return 22
    ```
    """,
                """
    Some texte...

    ```python
    #Et même un bloc de code
    ```
    """,
            ],
            [1,2],
        ],

    ) }}



=== "Title plural"

    Automatic title, plural, `multi={{ Env().args_multi_qcm_multi }}, shuffle={{ Env().args_multi_qcm_shuffle }}`{.red}
    {.red}


    {{ multi_qcm(
        [
            """
    Énoncé première question...

    You definitely can put additional text here
    ```python
    # or even
    def some_fun():
        return 43 - 1
    ```
    """,
        [
            "TeXagain: $F_{n+1}$",
            "choix 2 `#!python int(\"42\")`",
            "```python title=''\n# on pourrait insérer du multi line\ndef func():\n    return 26\n```",
            "meh?"
        ], [1]],
        ["Énoncé seconde question...", [
            "1",
            "2",
        ], [2]],
    ) }}


=== "inside amdonition"

    !!! warning "qcm inside another admonition..."

        {{ multi_qcm(
            [
                "TeX: $F_n$ | Blabla... (1&2 corrects)",
                [
                    "TeX: $F_{n+1}$ |La sécurité incertaine lors du transfert de la clé",
                    "La lenteur des opérations de chiffrement et de déchiffrement",
                    "Le chiffrement nécessite un nombre très important de calculs",
                    "Des clés différentes sont utilisées pour chiffrer et déchiffrer"
                ],
                [1,2]
            ],
            [
                "Blabla... (4 correct)",
                [
                    "Déchiffrer illégitimement le message",
                    "Chiffrer le message",
                    "Dissimuler le message dans un support",
                    "Déchiffrer afin de retrouver le message original"
                ],
                [4]
            ],
            [
                """
                multiligne in admo...?

                (hope this works...)
                """,
                [
                    "Alice a besoin de la clé privée de Bob",
                    "Alice a besoin de la clé publique de Bob",
                    "Bob a besoin de la clé privée d'Alice",
                    """
    ```python title=''
    # on pourrait insérer du multi line
    def func():
        return 26
    ```""",
                    "duh..."
                ],
                [4]
            ],
            multi=False,
            shuffle=True,
            qcm_title="Randomized...",
        ) }}

=== "Description"


    {{ multi_qcm(
        [
            "Énoncé première question (correct=both)", ["A","B",], [1,2],
        ],
        multi=False,
        description = '
        Here is a tiny text...

        ```python title="yeaks!" linenums="20"
        def wtf(meh=42):
            raise KeyError()
        ```
        '
    ) }}






## Multi tests


=== "Several correct"

    `multi=False, shuffle={{ Env().args_multi_qcm_shuffle }}`{.red} Always squares
    {.red}

    {{ multi_qcm(
        [
            "Énoncé première question (correct=both)", ["A","B",], [1,2],
        ],
        multi=False,
    ) }}


=== "0 correct"

    `This is causing an error message log in the console:`{.red}

    {{ multi_qcm(
        [
            "Énoncé...",["A","B",], [],
        ],
        multi     = False,
        qcm_title = "`multi=False`{.red}" + red(', rounds'),
    ) }}

    ---

    `This is ok:`{.red}

    {{ multi_qcm(
        [
            "Énoncé...", ["A","B",], [],
        ],
        multi     = True,
        qcm_title = "`multi=True`{.red}" + red(', squares'),
    ) }}


=== "1 correct"

    {{ multi_qcm(
        [
            "Énoncé... (correct=A)", ["A","B",], [1],
        ],
        qcm_title = "`multi=False`{.red}" + red(', rounds'),
    ) }}


    ---

    {{ multi_qcm(
        [
            "Énoncé... (correct=A)", ["A","B",], [1],
        ],
        multi     = True,
        qcm_title = "`multi=True`{.red}" + red(', squares'),
    ) }}


=== "`multi` sources"

    {{ multi_qcm(
        [
            "Macro argument: `rounds`{.red} (correct=A)", ["A","B",], [1],
        ],
        [
            "Question override argument: `squares`{.red} (correct=A)", ["A","B",], [1], {'multi':True}
        ],
        qcm_title = "`multi=False`{.red}" + red(' from header: rounds by default.'),
    ) }}

    ---

    {{ multi_qcm(
        [
            "Macro argument: `squares`{.red} (correct=A)", ["A","B",], [1],
        ],
        [
            "Question override argument: `rounds`{.red} (correct=A)", ["A","B",], [1], {'multi':False}
        ],
        multi     = True,
        qcm_title = "`multi=False`{.red}" + red(' from macro: squares by default.'),
    ) }}






## Shuffle tests

`Also check that the correct answers are properly tracked when shuffling.`{.red}

`CCCC question always has the opposite setting for it's items.`{.red}


=== "None"

    {{ multi_qcm(
        ["AAAA (correct=A)", ["A","B","C","D","E"], [1],],
        ["BBBB (correct=A)", ["A","B","C","D","E"], [1],],
        ["CCCC (correct=A)", ["A","B","C","D","E"], [1], {'shuffle':True}],
    ) }}

=== "All"

    {{ multi_qcm(
        ["AAAA (correct=A)", ["A","B","C","D","E"], [1],],
        ["BBBB (correct=A)", ["A","B","C","D","E"], [1],],
        ["CCCC (correct=A)", ["A","B","C","D","E"], [1], {'shuffle':False}],
        shuffle = True,
    ) }}


=== "Questions"

    {{ multi_qcm(
        ["AAAA (correct=A)", ["A","B","C","D","E"], [1],],
        ["BBBB (correct=A)", ["A","B","C","D","E"], [1],],
        ["CCCC (correct=A)", ["A","B","C","D","E"], [1], {'shuffle':True}],
        shuffle_questions = True,
    ) }}

=== "Items"

    {{ multi_qcm(
        ["AAAA (correct=A)", ["A","B","C","D","E"], [1],],
        ["BBBB (correct=A)", ["A","B","C","D","E"], [1],],
        ["CCCC (correct=A)", ["A","B","C","D","E"], [1], {'shuffle':False}],
        shuffle_items = True,
    ) }}