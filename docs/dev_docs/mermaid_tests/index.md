
Prenons pour exemple le texte `que voulez vous que je vous dise`.

Le nombre d'occurrences de chaque lettre est :

| q   | u   | e   |     | v   | o   | l   | z   | s   | j   | d   | i   |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 2   | 5   | 5   | 6   | 3   | 3   | 1   | 1   | 3   | 1   | 1   | 1   |


On peut alors créer les feuilles du futur arbre.

La donnée associée à chaque nœud est un caractère et son poids qui est son nombre d'occurrences.

```mermaid
graph TD
    N7(l:1):::feuille
    N8(z:1):::feuille
    N90(j:1):::feuille
    N10(d:1):::feuille
    N1(i:1):::feuille
    N11(q:2):::feuille
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    classDef feuille fill:#F88
```

Ces feuilles sont triées selon l'ordre croissant du poids de leur nœud.



L'arbre de Huffman est un arbre construit progressivement à partir des feuilles.

À partir des deux nœuds ayant les plus petits poids, on crée un arbre :

+   la racine aura pour donnée un symbole vide et comme poids la somme des poids des deux nœuds.
+   des sous arbres droit et gauche qui seront les deux nœuds sélectionnés.

Ici les feuilles `l:1` et `z:1` ont été fusionnés de façon à former un nouvel arbre dont le poids de la racine est `1 + 1 = 2`

```mermaid
graph TD
    N90(j:1):::feuille
    N10(d:1):::feuille
    N1(i:1):::feuille
    N11(q:2):::feuille
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    N13(( :2)) --> N7(l:1):::feuille
    N13 --> N8(z:1):::feuille
    classDef feuille fill:#F88
```


Parmi l'ensemble des nœuds, d'une part les feuilles et d'autre part les nœuds fusionnés, on choisit les deux nœuds de poids les plus faibles, pour créer un arbre dont la racine aura pour donnée la somme des poids des deux nœuds et, pour sous arbres gauche et droit, les nœuds choisis.

```mermaid
graph TD
    N1(i:1):::feuille
    N11(q:2):::feuille
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    N12(( :2)) --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13(( :2)) --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    classDef feuille fill:#F88
```

On poursuit jusqu'à ce qu'il ne reste qu'un seul nœud.
On peut remarquer que le poids de ce nœud sera égal à la somme des poids des nœuds initiaux, c'est à dire à la longueur du message.

```mermaid
graph TD
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    N12(( :2)) --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13(( :2)) --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N14(( :3)) --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    classDef feuille fill:#F88
```



Ici on a fusionné deux nœuds déjà construits car leur poids (2) étaient plus petits que celui des feuilles (3 au minimum.)

```mermaid
graph TD
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    N14(( :3)) --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    N15(( :4)) --> N12(( :2))
    N15 --> N13(( :2))
    N12(( :2)) --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13(( :2)) --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille

    classDef feuille fill:#F88
```



```mermaid
graph TD
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    N14(( :3)) --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    N15(( :4)) --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N16(( :6)) --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    classDef feuille fill:#F88
```

Ici les deux nœuds aux poids les plus petits, sont la feuille `s:3` et le nœud construit, qui avait un poids de 3

```mermaid
graph TD
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
    N15(( :4)) --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N16(( :6)) --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    N17(( :6)) --> N9(s:3):::feuille
    N17 --> N14(( :3))
    N14 --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    classDef feuille fill:#F88

```


```mermaid
graph TD
    N3(e:5):::feuille
    N4(_:6):::feuille
    N16(( :6)) --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    N17(( :6)) --> N9(s:3):::feuille
    N17 --> N14(( :3))
    N14 --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    N18(( :9)) --> N15(( :4))
    N18 --> N2(u:5):::feuille
    N15 --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    classDef feuille fill:#F88
```



```mermaid
graph TD
    N16(( :6)) --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    N17(( :6)) --> N9(s:3):::feuille
    N17 --> N14(( :3))
    N14 --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    N18(( :9)) --> N15(( :4))
    N18 --> N2(u:5):::feuille
    N15 --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N19(( :11)) --> N3(e:5):::feuille
    N19 --> N4(_:6):::feuille
    classDef feuille fill:#F88
```


```mermaid
graph TD
    N18(( :9)) --> N15(( :4))
    N18 --> N2(u:5):::feuille
    N15 --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N19(( :11)) --> N3(e:5):::feuille
    N19 --> N4(_:6):::feuille
    N20(( :12)) --> N16(( :6))
    N20 --> N17(( :6))
    N16 --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    N17 --> N9(s:3):::feuille
    N17 --> N14(( :3))
    N14 --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    classDef feuille fill:#F88
```


```mermaid
graph TD
    N20(( :12))
    N21(( :20))
    N20 --> N16(( :6))
    N20 --> N17(( :6))
    N16 --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    N17 --> N9(s:3):::feuille
    N17 --> N14(( :3))
    N14 --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    N21 --> N18(( :9))
    N18 --> N15(( :4))
    N18 --> N2(u:5):::feuille
    N15 --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N21 --> N19(( :11))
    N19 --> N3(e:5):::feuille
    N19 --> N4(_:6):::feuille
    classDef feuille fill:#F88
```


On poursuit jusqu'à ce qu'il ne reste qu'un seul nœud dont le poids sera égal à la somme des poids des nœuds initiaux, c'est à dire à la longueur du message.

```mermaid
graph TD
    N22(( :32)) --> N20(( :12))
    N22 --> N21(( :20))
    N20 --> N16(( :6))
    N20 --> N17(( :6))
    N16 --> N5(v:3):::feuille
    N16 --> N6(o:3):::feuille
    N17 --> N9(s:3):::feuille
    N17 --> N14(( :3))
    N14 --> N1(i:1):::feuille
    N14 --> N11(q:2):::feuille
    N21 --> N18(( :9))
    N18 --> N15(( :4))
    N18 --> N2(u:5):::feuille
    N15 --> N12(( :2))
    N15 --> N13(( :2))
    N12 --> N7(l:1):::feuille
    N12 --> N8(z:1):::feuille
    N13 --> N90(j:1):::feuille
    N13 --> N10(d:1):::feuille
    N21 --> N19(( :11))
    N19 --> N3(e:5):::feuille
    N19 --> N4(_:6):::feuille
    classDef feuille fill:#F88

```


Si l'une des files est vide, c'est le sommet de l'autre file qui est renvoyé.

```mermaid
graph RL

subgraph resul [Plus léger]
R1(l:1):::feuille_resul
end
subgraph sub1 [file 1]
    direction TB
    N7(l:1):::feuille_resul
    N8(z:1):::feuille
    N90(j:1):::feuille
    N10(d:1):::feuille
    N1(i:1):::feuille
    N11(q:2):::feuille
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille

end

subgraph sub2 [file 2]
        direction TB
    IN7( ):::invisible
    IN8( ):::invisible
    IN90( ):::invisible
    IN10( ):::invisible
    IN1( ):::invisible
    IN11( ):::invisible



end
classDef feuille fill:#F88
classDef feuille_resul fill:#FF8
classDef invisible opacity:0
sub1 --> resul
sub2 --> resul
```

```mermaid
graph RL

subgraph resul [Plus léger]
subgraph noeud0 [ ]
        direction TB
            IN16(( :6)):::feuille_resul --> IN5(v:3):::feuille
            IN16 --> IN6(o:3):::feuille
        end

end
subgraph sub1 [file 1]

    direction TB
    IN7( ):::invisible
    IN8( ):::invisible
    IN90( ):::invisible
    IN10( ):::invisible
    IN1( ):::invisible
    IN11( ):::invisible


end

subgraph sub2 [file 2]
        direction TB
        subgraph noeud1 [ ]
        direction TB
            N16(( :6)):::feuille_resul --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
        end
        subgraph noeud2 [ ]
        direction TB
            N17(( :6)) --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
        end
        subgraph noeud3 [ ]
        direction TB
            N18(( :9)) --> N15(( :4))
            N18 --> N2(u:5):::feuille
            N15 --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
        end
        subgraph noeud4 [ ]
        direction TB
            N19(( :11)) --> N3(e:5):::feuille
            N19 --> N4(_:6):::feuille
        end
end
classDef feuille fill:#F88
classDef feuille_resul fill:#FF8
classDef invisible opacity:0
sub1 --> resul
sub2 --> resul
```

C'est le cas général, c'est l'élément au poids le plus petit qui est renvoyé.

```mermaid

graph RL

subgraph resul [Plus léger]
    direction TB
    IN1(i:1):::feuille_resul
end
subgraph sub1 [file 1]
    direction TB
    N1(i:1):::feuille_resul
    N11(q:2):::feuille
    N5(v:3):::feuille
    N6(o:3):::feuille
    N9(s:3):::feuille
    N2(u:5):::feuille
    N3(e:5):::feuille
    N4(_:6):::feuille
end

subgraph sub2 [file 2]
    direction TB
    subgraph noeud1 [ ]
        direction TB
        N12(( :2)) --> N7(l:1):::feuille
        N12 --> N8(z:1):::feuille
    end
    subgraph noeud2 [ ]
        direction TB
        N13(( :2)) --> N90(j:1):::feuille
        N13 --> N10(d:1):::feuille
    end
end
classDef feuille fill:#F88
classDef feuille_resul fill:#FF8
classDef invisible opacity:0
sub1 --> resul
sub2 --> resul
```

```mermaid
graph RL

subgraph resul [Plus léger]
    direction TB
    IN5(v:3):::feuille_resul
end
subgraph sub1 [file 1]
    direction TB
            N5(v:3):::feuille_resul
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
    end
    subgraph sub2 [file 2]
    direction TB
    subgraph noeud1 [ ]
        direction TB
            N14(( :3)) --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
    end
    subgraph noeud2 [ ]
        direction TB
        N15(( :4)) --> N12(( :2))
        N15 --> N13(( :2))
        N12(( :2)) --> N7(l:1):::feuille
        N12 --> N8(z:1):::feuille
        N13(( :2)) --> N90(j:1):::feuille
        N13 --> N10(d:1):::feuille
    end
end
classDef feuille fill:#F88
classDef feuille_resul fill:#FF8
classDef invisible opacity:0
sub1 --> resul
sub2 --> resul
```

{{ IDE() }}

