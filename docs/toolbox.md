

Sont présentés dans cette page divers outils permettant d'aider au développement en local d'une documentation créée avec `pyodide-mkdocs-theme`.

Notez que de nombreuses choses présentées ici sont spécifiques aux IDE de la famille de VSC (VS-codium devrait pouvoir les utiliser également). Il est en général possible de porter la plupart des fonctionnalités à d'autres IDEs, mais il vous faudra trouver la documentation nécessaire.








## `mkdocs.yml` : auto-complétion & validation { #yaml-schemas }

Le thème propose un jeu de schémas de validation du contenu du fichier `mkdocs.yml`, en reprenant les fonctionnalités proposées par [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/creating-your-site/#configuration){: target=_blank } et en les mettant à jour avec les spécificités du thème.

Ils sont donc utilisables dans des éditeurs comme VSC, et permettent d'avoir les suggestions d'auto-complétion, les descriptions des options, des liens vers les pages de la documentation et les valeurs par défaut pour le plugin du thème, `pyodide_macros`, en plus des fonctionnalités proposées par les schémas de mkdocs-material.

<br>

<div style="display:grid; width:100%;">
    <div class="row-of-imgs">
        <div class="c1"><img src="!!yml_schemas_hints_png" /></div>
        <div class="img-legend c1">Messages au survol des options et menus</div>
    </div>
</div>

<br>

<div style="display:grid; width:100%;">
    <div class="row-of-imgs">
        <div class="c1"><img src="!!yaml_schema_auto_completion_hints_png" /></div>
        <div class="c2"><img src="!!yaml_schema_auto_completion_after_png" /></div>
    </div>
    <div class="row-of-imgs">
        <br><div class="img-legend">Suggestions d'auto-complétion et valeurs par défaut</div>
    </div>
</div>




### Mise en place { #yaml-schemas-setup }


La procédure est [très similaire à celle proposée par `mkdocs-material`][material-schemas]{: target=_blank }.

Concernant VSC (ou assimilé), il convient donc d'ouvrir (ou de créer) le fichier `.vscode/settings.json`, puis d'y ajouter le contenu suivant :

<br>


```json
{
    "yaml.schemas": {
        "https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme/-/raw/main/yaml_schemas/schema.json": "mkdocs.yml"
    },
    "yaml.customTags": [
        "!ENV scalar",
        "!ENV sequence",
        "!relative scalar",
        "tag:yaml.org,2002:python/name:material.extensions.emoji.to_svg",
        "tag:yaml.org,2002:python/name:material.extensions.emoji.twemoji",
        "tag:yaml.org,2002:python/name:pymdownx.superfences.fence_code_format",
        "tag:yaml.org,2002:python/object/apply:pymdownx.slugs.slugify"
    ],
}
```




### Remarques { #yaml-schemas-notes }


#### Concernant `markdown_extensions:pymdownx.tabbed.slugify` { #yaml-schemas-notes-tabbed }

La syntaxe yaml n'étant pas totalement équivalente à du json, aucune solution n'a été trouvée jusqu'à présent pour valider la configuration de l'option `slugify` de l'extension markdown `pymdownx.tabbed` quand elle utilise des arguments supplémentaires (1) .
{ .annotate }

1. Ce qui est le réglage recommandé, entre autre par mkdocs-material.


Cette ligne restera donc en rouge dans le fichier, malheureusement, à moins d'utiliser la configuration par défaut pour `slugify` :

<div style="display:grid; width:100%;">
    <div class="row-of-imgs">
        <div class=""><img src="!!tabbed_slugify_not_ok_png" /></div>
        <div class="img-legend">Configuration conseillée, non reconnue avec les sous-éléments.</div>
        <div class=""><img src="!!tabbed_slugify_ok_png" /></div>
        <div class="img-legend">Configuration par défaut : valide, mais déconseillée à l'usage.</div>
    </div>
</div>



#### Concernant la validation des `plugins` { #yaml-schemas-notes-plugins }

Ces schémas héritent des mêmes défauts et avantages que ceux de [`material`][material-schemas]{: target=_blank }.

En l'occurrence, si la validation a bien lieu pour les plugins, vous ne verrez en général aucune ligne soulignée en rouge dans la section `plugins` du fichier `mkdocs.yml`, sauf pour les erreurs de syntaxe et les noms invalides de plugins eux-mêmes.
<br>Par contre, les configurations invalides peuvent être identifiées à la disparition des info-bulles d'un plugin, quand on survole ses options :


<div style="display:grid; width:100%;">
    <div class="row-of-imgs">
        <div class="c1"><img src="!!yml_plugins_tooltips_validation_errors_png" /></div>
        <div class="img-legend">Erreurs de configurations plus ou moins évidentes<br>(trois erreurs et non deux : pas d'info-bulle sur on_error_fail...)</div>
        <div class="c1"><img src="!!yml_plugins_tooltips_silent_errors_png_png" /></div>
        <div class="img-legend">Configuration invalide pour pyodide_macros:<br>Pas d'info-bulle !</div>
        <div class="c1"><img src="!!yml_plugins_tooltip_working_png" /></div>
        <div class="img-legend">Configuration valide pour pyodide_macros.</div>
    </div>
</div>














## Accélérer le travail en `serve`

Les temps de rendu deviennent assez vite problématiques lors du développement, surtout si beaucoup d'appels de macros sont utilisés et que le nombre de pages devient conséquent.

Il y a deux stratégies simples pour rendre le travail plus fluide :



### Changer le mode d'exécution { #serve-dirty }

```
mkdocs serve --dirty
```

<br>L'option `dirty` de la commande ci-dessus active un mode de travail dans lequel seulement les pages qui viennent d'être modifiées sont rendus à nouveau.
<br><br>

| Avantages {{width(20, center=1)}} | Inconvénients {{width(0, center=1)}} |
|-|-|
| {{ ul_li(["Le travail sur le fichier markdown en cours devient très fluide."]) }} | {{ ul_li([
            "Le premier rendu `serve` reste très lent car il est toujours fait sur l'intégralité de la documentation."]) }} |
| {{ ul_li(["Ne nécessite pas de modification du contenu de la documentation elle-même."]) }} | {{ ul_li([
            "Les modifications faites au fichier `mkdocs.yml` durant un `serve --dirty` ne sont pas appliquées (notamment en ce qui concerne le menu de navigation)"]) }} |
| | {{  ul_li([
            "Il peut arriver que suite à des modifications, l'état des données deviennent incorrect, avec un rendu qui ne correspond plus à l'état réel de la page (liens mort, rendus partiels, ... Le menu de navigation est particulièrement touché)."]) }} |




### `exclude_docs` { #exclude_docs-tools }


La propriété `exclude_docs` du fichier `mkdocs.yml` permet d'exclure certains fichiers du rendu. Ceci peut être mis à profit pour ne rendre que la page en cours de travail et ainsi réduire drastiquement la charge de calcul.
<br><br>

| Avantages {{width(20, center=1)}} | Inconvénients {{width(0, center=1)}} |
|-|-|
| {{ ul_li(["Le travail sur le fichier markdown en cours devient très fluide."]) }} | {{ ul_li([
            "Il s'agit bien d'une modification de la documentation, donc il ne faut pas oublier d'annuler cette modification avant de lancer un pipeline pour construire le site en ligne."]) }} |
| {{ ul_li(["Le premier rendu durant le `serve` est également très rapide."]) }} |  |

<br>

La propriété `exclude_docs` est une chaîne multilignes d'expressions régulières, où l'ordre de déclaration des expressions est primordial :

* Les exclusions d'abord.
* Les inclusions ensuite, en préfixant les expression avec `!`.

<br>

___Exemple :___

```yaml
exclude_docs: |
    **/*_REM.md
    **/*.py
    **/*.md
    !custom/config.md
```

La ligne `**/*.md` exclut toutes les pages markdown du rendu, tandis que la suivante inclut uniquement la page `custom/config.md`.

<br>

___Contraintes d'utilisation :___

* Ne pas oublier le caractère `|` sur la ligne `exclude_docs: |` : il permet de travailler en mode multilignes, justement.
* Les motifs ne tiennent pas compte de la profondeur: `!index.md` inclut ___tous___ les fichiers `index.md`, quelle que soit leur localisation dans le `docs_dir`.
* Ne pas mettre de commentaires dans les différentes lignes, car ils ne seraient en fait pas des commentaires, mais feraient partie intégrante de l'expression de cette ligne.








## Déboguer les codes `python` { #debug-python }


### Mise en place & lancement (VSC) { #debug-setup }

1. Vérifier que les extensions classiques pour python sont installées dans VSC (ou assimilé). À priori les deux suivantes devraient suffire :

    * `ms-python.python` (microsoft)
    * `ms-python.debugpy` (microsoft)

    <br>

1. Créer ou mettre à jour le fichier `.vscode/launch.json` avec le code ci-dessous :

    ```json
    {
        // Use IntelliSense to learn about possible attributes.
        // Hover to view descriptions of existing attributes.
        // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
        "version": "0.2.0",
        "configurations": [

            {
                "name": "Mkdocs",
                "type": "debugpy",
                "request": "launch",
                "module": "mkdocs",
                "args": [
                    "serve",
                    "-a",
                    "localhost:8000"
                ],
                "jinja": false,
                "justMyCode": true
            },
            {
                "name": "Python Debugger: Current File",
                "type": "debugpy",
                "request": "launch",
                "program": "${file}",
                "console": "integratedTerminal",
                "jinja": false,
                "justMyCode": true
            }
        ]
    }
    ```

    <br>

    La première configuration permet de lancer un `mkdocs serve` en mode de débogage. Avec donc possibilité d'utiliser des points d'arrêts, de s'arrêter automatiquement en cas d'erreur, ...

    <br>

    La seconde permet de lancer un fichier de manière isolée, ce qui peut permettre de tester certains fichiers python utilisés pour les IDEs, terminaux, ... en dehors du site construit (s'ils contiennent du code compatible : voir plus bas).

    <br>

3. Sélectionner le profil d'exécution voulu dans le panneau du débogueur puis lancer les exécutions, en appuyant sur ++f5++ ou sur le bouton "lecture", en vert dans l'image :

    <div style="display:grid; width:100%;">
        <div class="row-of-imgs">
            <div class="c1"><img src="!!debug_1_png" /></div>
            <div class="c2"><img src="!!debug_2_png" /></div>
        </div>
    </div>



<br>

!!! tip "Les options `jinja` et `justMyCode` des profils de déboguage"

    * `justMyCode` :

        Si cette option est à `true`, le débogueur ne s'arrêtera que dans vos fichiers.

        Cette notion concerne en particulier les arrêts automatiques sur les erreurs levées :
        si cette option est passée à `false`, le débogueur s'arrêtera aussi sur une erreur levée dans les modules utilisés par l'interpréteur python. Cela peut notamment permettre d'explorer les raisons d'une erreur dans mkdocs, le thème lui-même, ou encore un autre plugin utilisé dans le projet...

        Garder cependant en tête que le code s'exécute encore plus lentement, quand cette option est à `false`.<br><br>

    * `jinja` :

        Si à `true`, les rendus des templates Jinja sont visibles dans la pile d'appels du débogueur et il est possible de les explorer, voire même de stopper le débogueur sur les erreurs levées lors des rendus de ces templates.

        Cela peut parfois se révéler utile, mais c'est en général très difficile à comprendre/utiliser et il vaut mieux laisser la valeur à `false`.



### Accélérer le débogage

Le problème essentiel du débogage est que les temps d'exécutions sont souvent multipliés par un facteur 5 à 10.

C'est donc ici que la propriété [`mkdocs.yml:exclude_docs`](--exclude_docs-tools) se révèle particulièrement utile pour pouvoir cibler la ou les pages problématiques et ne rendre que celles-ci.



### Déboguer vos macros { #debug-py-macros }

!!! tip ""
    * Profil du débogueur : "MkDocs".
    * `justMyCode: true`
    * Utiliser [`exclude_docs`](--exclude_docs-tools) pour cibler la ou les pages problématiques.
    * ++f5++




### Déboguer mkdocs, le thème ou d'autres plugins { #debug-py-system }

!!! tip ""
    * Profil du débogueur : "MkDocs".
    * `justMyCode: false`
    * Utiliser [`exclude_docs`](--exclude_docs-tools) pour cibler la ou les pages problématiques.
    * ++f5++


### Tester/déboguer les `.py` de la documentation { #debug-py-docs }

Il s'agit ici de tester les fichiers python utilisés pour les macros `IDE`, `terminal`, ... Deux approches sont possibles :


#### 1. Exécution via Pyodide { #debug-py-pyodide }

Il s'agit plutôt de tester les codes, sans pouvoir mettre à profit des fonctionnalités de débogage à proprement parler :

Lancer un `serve`, aller sur la page en question et utiliser l'IDE lui-même ou [le bouton pour tester la correction](--IDEs-test-corr) depuis le navigateur (en vert, dans l'image ci-dessous) :

![Tester la correction durant un `mkdocs serve`](!!test_corr_png){: loading=lazy .margin-top-h4 .w35 align=right }

* Permet de tester le code dans le même contexte d'exécution que l'utilisateur.
* Lent à mettre en place ([`exclude_docs`](--exclude_docs-tools) peut aider).
* La visibilité ou non des modifications faites au fichier python depuis le navigateur n'est pas toujours évidentes. Elle dépend notamment des points suivants :

    - En mode [`serve --dirty`](--serve-dirty), il faut modifier le fichier python ___et___ enregistrer le [fichier markdown source](--ide-files) pour que les modifications soient intégrées au build.
    - Si la modifications concerne [la section `PYODIDE:code`](--ide-sections), il faut en plus réinitialiser l'IDE pour voir apparaître les changements (sinon, c'est la version précédemment enregistrée dans le localStorage qui est utilisée).


#### 2. Exécuter un fichier python en local (VSC, ...) { #debug-py-local }

L'idée est cette fois de pouvoir travailler plus vite, et éventuellement avec les outils de débogage de votre IDE.

Cependant, l'environnement n'est alors plus le même que celui de l'utilisateur, ce qui génère un nombre certains de contraintes et ne garantit pas que ce qui tourne en local tournera dans pyodide exactement de la même façon (notamment en ce qui concerne l'articulation des codes des différentes [sections](--ide-sections) entre elles).

Les tenants et aboutissants sont discutés à la section suivante, ainsi que les outils permettant de faciliter l'articulation entre les deux types d'environnements, mais voici la configuration du débogueur à utiliser :

!!! tip ""
    * Profil du débogueur : "Current File"
    * `justMyCode: true`
    * Ouvrir le fichier que l'on veut tester dans l'éditeur
    * ++f5++














## Lancer les `.py` de la documentation en local { #py-local }

Le thème propose quelques outils pour faciliter le lancement des fichiers python de la documentation en local, de façon à pouvoir en tester le code en dehors du site construit ou d'un `mkdocs serve`.

Comme évoqué précédemment, il y a cependant un certain nombre de limitations.




!!! danger inline end w50 margin-top-h3 "Argument `-C` du script"

    Avec cet argument, un fichier est créé directement à la racine du projet, en écrasant sans avertissement tout autre fichier du même nom que celui du fichier à copier.

### `toolbox.py` et `pyodide_plot.py` { #py-local-toolbox }

La première chose à faire est "d'installer" ces deux fichiers, fournis par le thème, à la racine du projet.

Le plus simple pour cela est d'utiliser le script PMT :

```
python -m pyodide_mkdocs_theme --toolbox --plot -C
```

Ou en version plus condensée :

```
python -m pyodide_mkdocs_theme -tPC
```

<br>

Ces fichiers permettent de mimer ou réimplanter certaines fonctionnalités utilisées dans l'environnement de Pyodide.
<br>Pour les mettre à profit, il suffit d'ajouter ce code tout en haut du fichier python de la documentation que l'on veut ensuite exécuter localement :

<br>

```python title="Snippet à ajouter en haut des fichiers python de la documentation"
# --- PYODIDE:ignore --- #
from toolbox import *
```

??? help "Comment ça marche ?"

    * Comme décrit dans [la page concernant les IDEs](--ide-sections), les sections `ignore` des fichiers python sont ignorées quand les IDE sont créés pour l'environnement de Pyodide. Donc ce code n'a aucun impact sur les IDEs du site construit.

    * À contrario, le contenu des sections `ignore` est du code python à part entière lorsque un de ces fichiers est exécuté en local.

    * Le fichier `toolbox.py` étant à la racine du projet, il est dans le répertoire de travail (`cwd`) de l'interpréteur python et peut donc être importé depuis un fichier lancé localement.


??? help "`toolbox.py` vs `matplotlib`"

    Le fichier `toolbox.py` importe `pyodide_plot.py`, qui lui-même importe `matplotlib`.

    Si vous n'en avez pas l'utilité et que vous ne souhaitez pas installer `matplotlib`, supprimez ou passez en commentaire la ligne d'import dans `toolbox.py`.


??? help "Contenu du fichier `toolbox.py`"

    ```python
    ---8<--- "toolbox.py"
    ```

??? help "Contenu du fichier `pyodide_plot.py`"

    ```python
    ---8<--- "pyodide_plot.py"
    ```


<br>

Une fois les deux fichiers python placés à la racine du projet et le code ajouté dans le fichier python de la documentation que l'on souhaite tester, cette "boîte à outils" offre les fonctionnalités suivantes :

* Le décorateur `@auto_run`{ .green } est utilisable en local.
<br>La méthode `auto_run.clean` est également présente, mais n'a aucun effet.

* Les codes utilisant `import js`{ .green } (ou toute autre forme d'import de ce package Pyodide) ne lèvent pas d'erreurs, mais leur logique devient inopérante : [l'objet `js` importé est un `Mock`][mocks].
<br>À noter que :

    - Les accès à des propriétés (quelles qu'elles soient et "récursivement") renvoient toujours un objet `Mock`. Les comparaisons avec des valeurs renverront donc toujours `False`, par exemple.
    {{pmt_note("Il est possible d'[augmenter les objets Mock avec d'autres fonctionnalités][mocks], si vous vous en sentez le courage.")}}

    - Les appels de méthodes ne lèvent aucune erreur, mais n'ont plus aucun effet, non plus.
    {{pmt_note("Les méthodes sont également des Mock, qui \"absorbent\" tout bonnement n'importe quel appel de fonction).")}}

* La fonction `mermaid_figure`{ .green }, pour gérer [les graphes Mermaid](--custom/mermaid/),  est également utilisable.
{{ pmt_note("Son seul effet est d'afficher le code mermaid dans la console.") }}

* L'objet `PyodidePlot`{ .green }, utilisé pour [gérer les figures créées avec `matplotlib`](--custom/matplotlib/) est utilisable en local.
{{ pmt_note("Il est fonctionnel et les figures seront effectivement affichées dans une fenêtre isolée, lors des exécutions. Il faut par contre que `matplotlib`{.pmt-note} soit déjà installé dans l'environnement local.") }}

* Les fonctions `pyodide_uploader`{ .green } et `pyodide_downloader`{ .green } sont disponibles sous forme de Mocks et n'ont donc pas d'effet.
{{ pmt_note("Rappel: `pyodide_uploader_async`{.pmt_note} n'est [pas utilisable car asynchrone](--py-local-no-async), et c'est la seule des fonctions d'échanges de fichiers qui renverrait normalement des résultats utilisables pour l'exécution en cours.") }}




### Bibliothèques python { #py-local-py-libs }

- Concernant les bibliothèques externes, qui sont installées automatiquement dans l'environnement Pyodide, il suffit de les installer dans l'environnement local au préalable.

- Pour les [bibliothèques personnalisées](--custom-libs) du projet, si elles ont été créées à la racine du projet, elles sont donc importables pour les exécutions locales sans rien avoir à faire de particulier.




### Les impossibilités { #py-local-no-async }

!!! danger "Section d'environnement utilisant du code async"

    Il est impossible d'exécuter en local un fichier python contenant des sections d'environnement (`env`, `env_term`, `post_term` ou `post`) qui contiennent des appels de fonctions asynchrones :

    ```python title="Appels asynchrones"
    # --- PYODIDE:env --- #
    fichier = "image.jpg"
    await copy_from_server(fichier)     #  <<< HERE !
    ```

    <br>

    ??? note "Raison"

        En python, il est interdit d'utiliser le mot clef `await` en dehors d'une fonction `async`.

        ```python
        async def awaitable(...):
            """ do something async"""
            await other_async_tool(...)     # <-- OK !


        await awaitable(...)                # <-- SyntaxError !
        ```

        Cette restriction s'applique évidemment aux exécutions locales.

        Ces codes fonctionnent par contre dans l'environnement Pyodide car il y a toute une logistique autour qui fait que, quand lancés avec les bons outils (`pyodide.runPythonAsync`), tout se passe comme si le code en question était lancé depuis l'intérieur d'une fonction `async def`, justement.


Ceci implique que tous les exercices impliquant ces types d'outils ne pourront pas être exécutés en local :

* Requêtes http/ftp/... et utilisation de `copy_from_server`
* Interaction asynchrones avec l'utilisateur : `pyodide_uploader_async`

