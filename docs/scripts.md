Le thème vient avec quelques scripts qui peuvent être lancés depuis un terminal :

```
python -m pyodide_mkdocs_theme
```

<br>

```
usage: pyodide_mkdocs_theme [-h]
                            [-v]
                            [--new NEW]
                            [--lang [{de,en,fr}]]
                            [--macros]
                            [--mime]
                            [--py]
                            [--toolbox]
                            [--yml]
                            [--file FILE]
                            [--copy]

optional arguments:
  -h, --help            Show this help message and exit.

  -v, --version         Show current pyodide-mkdocs-theme's version number and exit.

  -n NEW, --new NEW     Starts a new PMT project, creating a directory with the given
                        name in the current folder, then adds some basic contents to
                        the directory (docs with examples, .gitignore, main.py,
                        mkdocs.yml, pyodide_plot.py, requirements.txt, toolbox.py).
                        Works with the --lang argument.

  -m, --mime            Open a page in the browser, to the MDN documentation about
                        MIME types (useful when using pyodide_downloader).

  --lang                Optional. Choices: de, en, fr, with fr being the default.
                        Print the base python code to customize some messages. Can
                        also be used with some other arguments to get the information
                        in languages other than "fr", if relevant.

  -M, --macros          Print the content a `main.py` file, usable to create custom macros
                        for the documentation. The file also contains the code used to
                        modify PMT messages. You can remove it if you don't need it.
                        Works with the --lang argument.

  -P, --plot            Print the content of the PyodidePlot declaration file, helping
                        to run it locally.

  -p, --py              Print an example of python file, for {% raw %}{{ IDE(...) }},
                        {{ terminal(...) }} or {{ py_btn(...) }}{% endraw %} macros.
                        Works with the --lang argument (defaults to english if the
                        language is not available).

  -t, --toolbox         Print the content of the toolbox.py file (helpers to run python
                        files from the documentation locally).

  -y, --yml             Print a base configuration for the mkdocs.yml file.
                        Works with the --lang argument.

  -F FILE, --file FILE  When used in combination with one of --lang, --py or --yml,
                        the information will be written into the given file instead
                        of the stdout (any existing content will be overwritten / use
                        an absolute path or a path relative to the cwd).

  -C, --copy            Acts like --file, but using the original name of the file.
```