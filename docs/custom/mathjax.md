

Tout le nécessaire pour utiliser LaTex dans vos documents est déjà préchargé par le thème.

MathJax est déjà configuré avec le package `cancel`.<br>
Si vous avez besoin de le personnaliser davantage, il est possible de surcharger le fichier javascript du thème.

Cela se déroule en plusieurs temps :


1. Créez ou modifiez un [`custom_dir`](--custom/custom_dir/).

    Si vous en avez déjà un, il suffira de le modifier.

    Dans le cas contraire, ajoutez un dossier à la racine du projet (_PAS_ dans le `docs_dir` !).<br>Le nom typiquement utilisé est `overrides`, mais vous pouvez utiliser un autre nom si vous le souhaitez.

    Modifiez ensuite votre fichier `mkdocs.yml` si vous n'aviez pas encore de `custom_dir`, en l'ajoutant à la section `theme` :

    ```yaml
    theme:
        custom_dir: overrides    # ou tout autre nom que vous aurez choisi
    ```

    <br>

1. Créez un fichier `overrides/js-libs/mathjax-libs.js` dans votre projet et y coller le code donné ci-dessous.

    Vous pouvez modifier les propriétés de l'objet `window.MathJax` à votre convenance, mais il faut impérativement garder le contenu du champ `startup`.<br>
    Laissez également inchangé le code au-dessus de la déclaration de `window.MathJax`.

    !!! note "Code javascript de remplacement pour la configuration de MathJax"

        ```javascript
        {{ md_include_rm_license("pyodide_mkdocs_theme/templates/js-libs/mathjax-libs.js") }}
        ```
