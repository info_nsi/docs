Sont discutés ici :

* Le téléchargement depuis l'environnement Pyodide de fichiers stockés en ligne, sur un serveur.
* La création de fichiers dans l'environnement de Pyodide.
* Les échanges de fichiers Pyodide/utilisateur pour :
    - téléverser (upload) les contenus d'un ou plusieurs fichiers de la machine de l'utilisateur vers l'environnement de Pyodide.
    - télécharger (download) sur la machine de l'utilisateur des données construites lors des exécutions dans Pyodide.

Ne sont donc pas discutés ici les fichiers markdown ou les fichiers pythons utilisés pour les macros `IDE`, `terminal`, ... (les concernant, voir les pages dédiées dans la [section Rédacteurs](--redactors/resume/)).

<br><br>

!!! help "Les codes python pour les échanges de fichiers doivent être dans des sections d'environnement"

    Les échanges de fichiers nécessitent dans leur grande majorité de recourir à des appels asynchrones comme `await fetch(...)`. En python, ce type d'appels ne peut normalement pas être fait en dehors d'une fonction async (`async def func(...)`), alors qu'il est laborieux d'appeler des fonction async depuis du code asynchrone (beaucoup plus qu'en javascript).

    Les [sections d'environnements](--ide-sections) des fichiers pythons utilisés pour les IDE, les terminaux ou les py_btn sont exécutées via un mode `async` de pyodide, qui permet de mettre des appels asynchrones en dehors des fonctions.

    C'est pourquoi les codes concernant des téléchargements et téléversements de fichiers doivent être faits depuis les sections d'environnement.


## Systèmes de fichiers


### Contexte { #files-sys-ctx }

L'environnement de Pyodide dispose de son propre système de fichiers, avec son disque virtuel.

Il est donc possible d'utiliser les fonctionnalités I/O dans Pyodide comme on le ferait sur une machine "normale" (`with open(...)`, `pathlib.Path`, ...).

<br>

!!! warning "Limitations liées au système de fichiers dans Pyodide"

    * {{red("Le système de fichiers est vierge au démarrage de l'environnement")}}.
    <br>Il faut donc créer les fichiers manuellement ou les copier depuis le serveur du site (voir [`copy_from_server` plus bas](--copy_from_server)).

    * Ne pas oublier que l'intégralité de l'environnement, disque virtuel compris, tourne en mémoire vive, par l'intermédiaire des onglets du navigateur. Ceci implique qu'{{ red("il faut éviter de manipuler de gros fichiers via Pyodide, sous peine de faire crasher le navigateur de l'utilisateur") }}.
    <br>Il faut également garder en tête que la quantité de RAM disponible peut varier drastiquement selon la machine, l'OS et/ou le navigateur utilisés.

<br>

!!! tip "Ne pas confondre avec les fichiers sur le serveur"

    Les fichiers placés dans le `docs_dir` (généralement, nommé `"docs"`) sont présents sur le serveur mettant en ligne le site construit via `mkdocs build`, sauf s'ils en sont exclus, par exemple via l'option `exclude_docs` du fichier `mkdocs.yml`.

    {{orange("Ces fichiers sont sur le serveur, et non dans l'environnement Pyodide lui-même.")}}

    {{green("Il est par contre possible d'extraire ces fichiers du serveur, depuis l'environnement Pyodide, pour y utiliser ensuite leur contenu")}} (voir les sections suivantes).


### Extraction de fichiers d'un serveur { #async }

Il est possible de récupérer les données de fichiers ou pages html hébergés en ligne depuis les sections d'environnement (`env`, `env_term`, `post_term` ou `post`) avec ce type de code :

<br>

```python
# --- PYODIDE:env --- #
from js import fetch

url_fichier = "zoo_traduction.csv"
    # Voir ci-dessous concernant la localisation du fichier !

reponse = await fetch(url_fichier)
data    = await reponse.text()
```

`data` peut ensuite être converti par vos soins, pour correspondre à l'utilisation souhaitée.

<br>

Il est également possible de récupérer le contenu brut du fichier en utilisant soit `js.fetch`, soit `pyodide.http.pyfetch` :

=== "js.fetch"

    ```python
    from js import fetch
    reponse = await fetch("zoo_traduction.csv")
    data = (await reponse.arrayBuffer()).to_bytes()
    ```

=== "pyodide.http.pyfetch"

    ```python
    from pyodide.http import pyfetch
    reponse = await pyfetch("zoo_traduction.csv")
    data = await reponse.bytes()
    ```

<br><br>

{{ anchor_redirect(id="fetch-relative-addresses", with_class=False) }}

!!! danger "Le problème des adresses de fichiers : url relatives"

    Si vous utilisez des adresses absolues pour les requêtes, aucun problème.

    Si par contre vous utilisez une adresse relative, pour récupérer un fichier voisin de la page sur laquelle l'utilisateur se trouve actuellement, il faut garder en tête que :

    * L'adresse du fichier doit être donnée par rapport au dossier contenant la page html sur laquelle l'utilisateur se trouve. L'adresse est donc définie {{orange("par rapport au fichier markdown source")}}, et surtout pas par rapport au fichier contenant le code python.

    * Si le thème fonctionne avec l'option mkdocs `use_directory_urls: true`{.orange}, qui est le réglage par défaut pour `mkdocs`, {{orange("le dossier de la page html en cours change selon que le fichier markdown source est nommé `index.md`{.orange} ou autrement")}}.

    <br>

    Par exemple, voici les adresses relatives à utiliser pour accéder aux fichiers `.txt` depuis les fichiers markdown voisins, pour cette hiérarchie sur le dépôt :

    ```
    ├── prog_dyn
    │   ├── index.md
    │   ├── knapsack.txt
    │   └── exo.py
    └── DpR
        ├── closest_points.md
        ├── points.txt
        └── exo.py
    ```

    | `use_directory_urls` | Fichier markdown<br>source, sur le dépôt {{width(15)}} | Répertoire actif sur la page web {{width(13)}} | Chemin relatif `url_fichier` dans le code python |
    |-|-|-|-|
    | `true` | `/prog_dyn/index.md` | `/prog_dyn` | `#!py "knapsack.txt"` |
    | `true`{.orange} | `/DpR/closest_points.md`{.orange} | `/DpR/closest_points` | `"../points.txt"`{.orange} |
    | `false` | `/prog_dyn/index.md` | `/prog_dyn` | `#!py "knapsack.txt"` |
    | `false` | `/DpR/closest_points.md` | `/DpR/closest_points` | `#!py "points.txt"` |



### Création et copie de fichiers

Après extraction d'un fichier sur un serveur, il est possible de recréer le même fichier dans l'environnement Pyodide.
<br>Ceci peut s'avérer particulièrement utile pour travailler ensuite sur des images.

Le thème propose une fonction dédiée à ce type de tâche, `await copy_from_server(...)`, dont le principe de base est de récupérer les données au format bytes, puis d'écrire directement le fichier correspondant sur le disque virtuel :


```python
# --- PYODIDE:env --- #
from pyodide.http import pyfetch

fichier = "image.jpg"
reponse = await pyfetch(url_fichier)
with open(fichier, 'wb') as pyodide_file:
    pyodide_file.write(await reponse.bytes())
```

Un fichier `image.jpg` identique à celui stocké sur le serveur de la documentation existe alors à la racine du disque virtuel dans Pyodide.

{{anchor_redirect(id="copy_from_server", with_class=False)}}

!!! tip "Utiliser la fonction asynchrone `copy_from_server(...)`"

    Le thème propose une fonction asynchrone permettant de récupérer automatiquement un fichier sur un serveur, et de le recréer sur le disque virtuel.

    Le code de l'exemple précédent devient alors simplement:

    ```python
    # --- PYODIDE:env --- #
    fichier = "image.jpg"
    await copy_from_server(fichier)
    ```

    <br>

    Les spécifications complètes de la fonction `copy_from_server` sont les suivantes :

    ```python
    async def copy_from_server(
        src: str,
        dest: str=".",
        name: str="",
    ):
        """
        Récupère le fichier à l'adresse `src` (absolue ou relative au dossier de la
        page en cours), et crée son équivalent sur le disque virtuel de pyodide à
        l'adresse `dest/nom_de_fichier`.
        `nom_de_fichier` est l'argument `name`, ou si celui-ci n'est pas renseigné,
        le nom de fichier à la fin de `src` qui est utilisé à la place.

        Exemple :

            await copy_from_server("../other/img.jpg", "work/black_white")

            => Crée le fichier `work/black_white/img.jpg` sur le disque virtuel.
        """
    ```

    <br>

    | Argument | Rôle |
    |-|-|
    | `src` | Adresse du fichier source.<br>Peut être une adresse relative ou absolue. |
    | `dest` | Dossier de destination du fichier dans pyodide. Par défaut, le répertoire de travail de l'environnement est utilisé. |
    | `name` | Si l'argument `name` est utilisé, ce sera le nom utilisé pour le fichier dans pyodide. Sinon, le nom de fichier sera extrait de `src`. |

    * Si `src` est une adresse relative, [attention au nom du fichier markdown ,`index.md` ou autre, qui peut modifier le chemin relatif sur le site construit](#fetch-relative-addresses).
    * Si `dest` est utilisé, les éventuels répertoires intermédiaires manquant sont créés automatiquement.
    * Si un fichier du même nom existe déjà sur le disque virtuel, il est écrasé automatiquement.




## Échanges de fichiers avec l'utilisateur {{anchor_redirect(id="echanges-de-donnees-avec-lutilisateur")}} { #exchange-files-user }

Moyennant certaines limitations, il est possible de proposer à l'utilisateur de :

- Téléverser son propre contenu dans l'environnement, potentiellement pour l'utiliser ensuite lors des exécutions (uniquement depuis une section __asynchrone__).
- Télécharger des données construites durant l'exécution du code (un graphe mermaid, du texte généré, une image, ...).

Ces opérations sont par essence "asynchrones" car elles utilisent des évènements du DOM, ce qui impacte notamment la façon de réaliser les téléversements de fichiers dans l'environnement.



## Téléverser dans pyodide {{anchor_redirect(id="televerser-dans-pyodide")}} { #pyodide-upload }


!!! tip inline end w35 "But"
    L'utilisateur injecte des données issues d'un de ses fichiers dans l'environnement.

Il est possible de téléverser des contenus au format chaîne de caractères (`#!py str`) en utilisant l'encodage par défaut (utile pour les fichiers `py`, `txt`, `csv`, ...), ou bien au format `#!py bytes` (fichiers `png`, `jpeg`, `zip`, ...).

<br>

Deux fonctions (décrites plus bas) permettent de gérer les téléversements : elles partagent les mêmes arguments, mais n'ont pas exactement les mêmes comportements ni les mêmes types de sortie.


{{ md_include("docs_tools/inclusions/files_encoding.md") }}


??? danger "Bug possible avec les raccourcis clavier"

    {{fake_h3("Contexte")}}

    Les navigateurs mettent en place diverses sécurités pour protéger l'utilisateur contre des sites frauduleux, dont l'une qui peut potentiellement empêcher le bon fonctionnement du téléversement :
    <br>Le téléversement repose sur l'activation d'un évènement `input.click()` via le code lui-même. Or, un navigateur n'autorise pas un programme à faire deux fois de suite ce type d'opération s'il n'y a eu aucune "action utilisateur" dans la page entre les deux, une action utilisateur étant typiquement un clic dans la page.

    {{fake_h3("Le problème")}}

    Les raccourcis clavier ne sont pas considérés comme des "actions utilisateur". Donc le téléversement n'est pas effectué dans la situation suivante :

    1. L'utilisateur déclenche les exécutions d'un IDE avec ++ctrl+s++ ou ++ctrl+enter++.
    1. L'utilisateur annule le téléversement, ou téléverse un fichier.
    1. L'utilisateur réactive un raccourci clavier pour relancer les exécutions sans faire d'autre action dans la page.

    {{fake_h3("Solution partielle")}}

    Le comportement par défaut ne permet pas d'avertir l'utilisateur du problème car aucune erreur n'est levée durant les exécutions, mais une façon alternative de déclencher l'évènement a permis de forcer la levée d'une erreur dans ce cas. Cette approche n'est cependant [pas compatible avec tous les navigateurs][showPicker]{: target=_blank } (en fait, à l'heure actuelle, seul Safari n'est pas compatible).

    * Pour un navigateur {{green("__compatible__")}} :

        L'utilisateur verra un message dans le terminal lui disant de cliquer dans la page avant de réutiliser le raccourci, ou de lancer les exécutions via un bouton plutôt qu'un raccourci.
        <br>{{green("Le terminal restera utilisable normalement")}}, dans ce cas.

    * Pour un navigateur {{red("__NON compatible__")}} :

        Dans ce cas, l'utilisateur peut se retrouver avec une page non fonctionnelle, et aucune indication sur le fait qu'il y a un problème. Il devra obligatoirement {{red("recharger la page")}}.





<br>

### Asynchrone

C'est cette version qu'il faut utiliser en priorité car {{green("__elle permet d'utiliser le contenu du fichier téléversé dans la suite des exécutions__")}}.

Mais comme son exécution est `async`{.red}, cette fonction ne peut être appelée que depuis des sections d'environnement, comme `env`, `env_term`, `post_term` ou `post`.

```python
async def pyodide_uploader_async(
    cbk: Callable[[...], T],
    *,
    read_as:    Literal['txt','img','bytes'] = 'txt',
    with_name:  bool = False,
    multi:      bool = False,
) -> T | Tuple[T] :
    ...
```


### Synchrone

Cette version peut être exécutée depuis n'importe quelle section, mais elle ne sera exécutée qu'après la fin des exécutions en cours. Ceci signifie que {{red("le contenu téléversé ne sera ___PAS___ disponible pour les exécutions en cours")}}, mais seulement pour la fois suivante.

```python
def pyodide_uploader(
    cbk: Callable[[...], None],
    *,
    read_as:    Literal['txt','img','bytes'] = 'txt',
    with_name:  bool = False,
    multi:      bool = False,
) -> None :
    ...
```

??? warning "Préférez la version `async` !"

    La gestion de données provenant de deux exécutions différentes est particulièrement propice aux bugs, en particulier si l'utilisateur peut interagir avec l'environnement entre ces exécutions.

    __N'utilisez la version synchrone que s'il n'existe pas d'autre solution__.


### Entrées/sorties des fonctions

| Argument {{width(9)}} | Type {{width(9)}} | Rôle |
|-|-|-|
| `cbk` | `Callable` | Routine qui reçoit en argument le contenu d'un fichier téléversé au format désiré (`#!py str` ou `#!py bytes`), avec éventuellement le nom du fichier d'origine. Cette fonction a en charge les éventuelles conversions à appliquer au contenu du fichier. Elle peut ou non renvoyer un résultat, selon l'uploader utilisé (voir ci-dessous). |
| `read_as` | `#!py str='txt'` | Permet de choisir sous quelle forme le contenu du fichier va être lu et passé à `cbk`:<br>{{ul_li([
    "`'txt'` : renvoie le contenu au format str (lu avec l'encodage par défaut)",
    "`'img'` : renvoie le contenu sous forme de DataURL, qui peut être insérée directement dans une balise `<img>`",
    "`'bytes'` : renvoie le contenu au format `#!py bytes`"
])}} |
| `with_name` | `#!py bool=False` | Si vrai, la routine `cbk` recevra un second argument, `filename`, indiquant le nom du fichier source. |
| `multi` | `#!py bool=False` | Si vrai, l'utilisateur aura le droit de sélectionner plusieurs fichiers à la fois. La routine `cbk` sera appelée une fois par fichier. |

<br>

Signatures pour `cbk`, selon l'appel utilisé pour l'uploader :

| Uploader  | `#!py with_name` | Signature de `cbk` |
|-|-|-|
| async  | `#!py False` | `#!py cbk(data) -> T` |
| async  | `#!py True`  | `#!py cbk(data, filename) -> T` |
| sync   | `#!py False` | `#!py cbk(data) -> None` |
| sync   | `#!py True`  | `#!py cbk(data, filename) -> None` |

Le type des données reçues via l'argument `data` dépend de l'argument `read_as` passé à l'`uploader` :

| `uploader(..., read_as=...)` | Type de `data` |
|-|-|
| `#!py 'txt'`   | `#!py str` |
| `#!py 'img'`   | `#!py str` |
| `#!py 'bytes'` | `#!py bytes` |

Les types de sortie de la routine `cbk` diffèrent également selon l'uploader utilisé, et éventuellement de l'argument `multi` :

| Fonction | `multi` | Type renvoyé par `cbk` |
|-|-|-|
| `pyodide_uploader_async` | `#!py False`   | `#!py T` (au choix du rédacteur) |
| `pyodide_uploader_async` | `#!py True`   | `#!py tuple[T,...]` |
| `pyodide_uploader` | `#!py True|False` | `#!py None` |

* Avec `pyodide_uploader_async` : `cbk` peut renvoyer le résultat du traitement des données provenant du fichier téléversé par l'utilisateur. Ces résultats seront ensuite renvoyés par l'appel initial à `pyodide_uploader_async`, les rendant disponibles dans l'environnement python pour la suite des exécutions.
* Avec `pyodide_uploader` : `cbk` ne renvoie pas de résultat et la routine devra muter l'environnement global pour que les données restent disponibles pour l'exécution suivante.



### Exemples

Voici des exemples fonctionnels de chaque version, pour se rendre compte de leur mode de fonctionnement :


=== "async/txt"

    Téléversement du contenu d'un fichier au format chaîne de caractères :

    {{ IDE_py("exemples/upload_async_txt", MAX_SIZE=4, before="!!!", py_title="Contenu du fichier python", admo_kls='note') }}


=== "async/img"

    Téléversement d'une image au format bytes et insertion dans la page :

    {{ IDE_py("exemples/upload_async_img", MAX_SIZE=4, before="!!!", py_title="Contenu du fichier python", admo_kls='note') }}

    Image téléversée insérée ci-dessous :

    <div style="border:solid gray;width:100%;min-height:10px;display:flex;justify-content:center"><img id="img-target"/></div>

    Le code html initial de la balise ci-dessus est le suivant :

    ```html
    <div style="border:solid gray;width:100%;min-height:10px;display:flex;justify-content:center">
        <img id="img-target" />
    </div>
    ```


=== "async/multi/encoding"

    Ouverture d'un fichier texte avec un encodage spécifique :

    {{ IDE_py("exemples/upload_async_encoding", MAX_SIZE=4, before="!!!", py_title="Contenu du fichier python", admo_kls='note') }}


=== "Chargement sync"

    {{ IDE_py("exemples/upload_sync", MAX_SIZE=4, before="!!!", py_title="Contenu du fichier python", admo_kls='note') }}







## Télécharger depuis pyodide {{anchor_redirect(id="telecharger-depuis-pyodide")}} { #pyodide-download }

!!! tip inline end w35 "But"
    L'utilisateur récupère des données issues de l'environnement en récupérant un fichier dans son répertoire de téléchargement.

La fonction de téléchargement est synchrone et peut être utilisée depuis n'importe quelle section.

De la même façon que pour le téléchargement des contenus des éditeurs, le navigateur impose que les fichiers soient enregistrés dans le répertoire de téléchargement, et seul le nom de fichier peut être choisi.

<br>

La signature de la fonction est la suivante :

```python
pyodide_downloader(
    content:    str|bytes|list[int]|bytearray,
    filename:   str,
    type_mime:  str="text/plain"
)
```

| Argument {{width(9)}} | Rôle |
|-|-|
| `content` | Le contenu du fichier. Si `content` est une liste d'entiers, elle sera automatiquement convertie en `#!py bytes`.<br>Le type de contenu doit être cohérent avec la valeur utilisée pour l'argument `type_mime`. |
| `filename` | Nom du fichier (le navigateur peut y ajouter des numéros de version, si un fichier du même nom existe déjà dans le répertoire de téléchargement). |
| `type_mime` | Définit le type de fichier créé. Ce type doit impérativement être cohérent avec le contenu fourni en premier argument pour que le fichier téléchargé puisse ensuite être ouvert par l'utilisateur.<br>La liste des types MIME possible est consultable [ici][typesMimes]{: target=_blank }. Un des scripts du thème permet d'ouvrir la page en question directement dans le navigateur : `python -m pyodide_mkdocs_theme --mime`. |

{{ md_include("docs_tools/inclusions/files_encoding.md") }}

Exemple :

{{ IDE('exemples/download') }}
