!!! warning "Fonctionnalités avancées"

    Utiliser le `custom_dir` permet de modifier le comportement du thème en profondeur ou d'en réutiliser certaines fonctionnalités.

    Pour bien mettre à profit le `custom_dir`, il faut être familier avec la [personnalisation d'un thème][material-custom]{: target=_blank }.

    <br>_"With great powers..."_




## Définir un `custom_dir`

Il suffit d'ajouter un dossier à la racine du projet, et d'ajouter son nom dans la section `theme` du fichier `mkdocs.yml`.
<br>Typiquement, le nom de dossier est `overrides`, mais il est possible d'utiliser un autre nom aussi.

```yaml
theme:
    custom_dir: overrides
```




## Utilisation

Il est possible d'ajouter autant de fichiers que nécessaire dans un `custom_dir`, du moment qu'ils n'ont pas les mêmes noms que ceux déclarés par les thèmes parents.

Si par contre il s'agit de modifier l'un des fichiers définis par un des thèmes parents, il faut identifier le thème qui définit le fichier concerné, récupérer le contenu du fichier à modifier, puis créer un fichier du même nom et chemin relatif par rapport au `custom_dir` que dans le dossier `templates` d'origine, et y faire les modifications souhaitées.

Il est possible de trouver les fichiers définis par les thèmes aux endroits suivants :

 * Dossier `templates` [du thème (PMT)][pmt-templates]{: target=_blank }
 * Dossier `templates` de [material][material-templates]{: target=_blank }.
 * Il est aussi possible de retrouver ces dossiers sur une installation locale, dans les répertoires d'installation des thèmes.

<br>{{ anchor_redirect(id="override-main", with_class=False) }}

!!! danger "Override de `main.html` ?"

    Une personne ayant déjà étendu les fonctionnalités de `material` en travaillant avec un fichier `main.html` défini dans le `custom_dir` pourrait être tentée de faire de même pour étendre le thème, mais {{red("cela n'est pas possible directement")}}.

    En effet, `pyodide-mkdocs-theme` utilise déjà un fichier `main.html`, et travailler avec un fichier `main.html` imposerait donc de récupérer la version de Pyodide-MkDocs-Theme pour ensuite la modifier.

    Pour la même raison, il n'est pas non plus possible d'utiliser `{% raw %}{% extends "main.html" %}{% endraw %}` (ceci conduirait à une boucle infinie).

    Plutôt que de faire un override du fichier `main.html` du thème (qui est de volume conséquent et pourrait être modifié d'une version à une autre), préférer l'utilisation des fichiers "crochets" décrits ci-dessous. Ceux-ci permettent d'insérer facilement de la logique à différents endroits du fichier `main.html` du thème.




## Crochets html {{anchor_redirect(id="crochets-html ")}} { #main-html-hooks }

Afin de facilité la personnalisation du thème, sans avoir besoin de redéclarer des fichiers entiers, le thème met à disposition quatres crochets qui permettent d'insérer de la logique à différents moments, dans le code html de chaque page.

<br>

Pour utiliser ces crochets, il suffit d'ajouter au `custom_dir` un dossier `hooks` contenant le ou les fichiers html appropriés à votre usage :

| Crochet | Insère de la logique dans le fichier `main.html`... |
|:-|:-|
| `hooks/libsBefore.html`    | ...au début du bloc `libs` |
| `hooks/libsAfter.html`     | ...à la fin du bloc `libs` |
| `hooks/scriptsBefore.html` | ...au début du bloc `scripts` |
| `hooks/scriptsAfter.html`  | ...à la fin du bloc `scripts` |

<br>

Ces fichiers sont inclus via la commande Jinja `{% raw %}{% include "..." ignore missing %}{% endraw %}`, ce qui permet d'y utiliser également des fonctionnalités Jinja, ou encore du css ou du js en y écrivant les balises appropriées.
<br>C'est notamment la façon idoine de réintroduire des balises `<scripts>` ou `<style>` chargeant des fichiers issues de CDNs dans le fichier `main.html`, si leur insertion via [`mkdocs.yml:extra_javascript`](https://www.mkdocs.org/user-guide/configuration/#extra_javascript){: target=_blank } a échoué (1).
{ .annotate }


1. Les scripts introduits via `extra_javascript` sont ajoutés en bas de la page html, juste avant le contenu du footer. Une erreur peut alors être levée si du code JS essaie d'utiliser ce contenu plus haut dans la page, alors que le cdn n'est pas encore chargé (_sauf astuces assez moches..._ :shhhh: ). Dans ce genre de cas, insérer la balise depuis un des crochets `libs` devrait permettre de résoudre le problème.