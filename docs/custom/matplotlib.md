Le thème propose des fonctionnalités pour faciliter l'intégration de graphiques réalisés avec [`matplotlib`][matplotlib]{: target=_blank } dans la documentation, créés à partir de données/codes dans pyodide.

Ces outils sont une évolution d'un travail proposé initialement par Nicolas Révéret.





## Principe général { #matplotlib-generalities }

L'idée générale pour que cela fonctionne est la suivante :

1. Insérer dans le fichier markdown un élément html (une `<div>`) qui accueillera la figure une fois qu'elle sera créée.
1. Importer [`matplotlib`][matplotlib]{: target=_blank } depuis pyodide.
1. Configurer `matplotlib` pour interagir avec le DOM.
1. Construire les données et afficher les courbes souhaitées dans la `div` créée au point 1.

<br>

* L'étape 1 est facilitée par l'utilisation de la macro [`{% raw %}{{ figure(...) }}{% endraw %}`](--redactors/figures/).
* L'étape 2 est faite par l'utilisateur, normalement depuis la [section `env`](--ide-sections) du fichier python associé à une macro `IDE`, `terminal` ou `py_btn`.
* L'étape 3 est gérée automatiquement par le thème, lorsqu'un import de `matplotlib` est détecté (étape 2).
* L'étape 4 est facilitée par l'utilisation d'objets de [la classe `PyodidePlot`](--pyodide-plot) (déclarée automatiquement par le thème, lors de l'étape 2), qui permettent de tracer les figures directement au bon emplacement dans le DOM (la `div` du point 1, donc).






## Exemple simple

Concrètement, pour la situation la plus simple où ___une seule figure___ doit être affichée dans la page, on peut procéder comme suit :

!!! note "Fichier markdown"

    Dans la page markdown, on insère l'[`IDE`](--redactors/IDE-details/), le [`terminal`](--redactors/terminaux/) ou le [`py_btn`](--redactors/py_btns/) qui générera la figure voulue, ainsi que l'élément qui accueillera la figure :

    ```markdown
    {% raw %}{{ IDE('pyodide_plot_1') }}

    {{ figure() }}{% endraw %}
    ```

!!! note "Fichier python"

    {{ py('exemples/pyodide_plot_1') }}

    Les objets `PyodidePlot` se comportent comme le module `matplotlib.pyplot` mais disposent également d'une méthode `plot_func`, qui permet de simplifier le tracé d'une courbe unique.

    _[Documentation détaillée de la classe `PyodidePlot`](--pyodide-plot) plus bas dans cette page_.


!!! note "Résultat"

    {{ IDE('exemples/pyodide_plot_1', TERM_H=3) }}

    Nota : L'installation de matplotlib peut être _très_ longue...

    {{ figure() }}




## La classe `PyodidePlot` { #pyodide-plot }

### Généralités

Cette classe est déclarée automatiquement au premier import de `matplotlib` dans pyodide.

* Son constructeur prend un seul argument, optionnel, qui est l'identifiant html (`id`) de la balise `div` qui devra accueillir la figure.
* Si cet argument n'est pas fourni, c'est la valeur par défaut pour [l'argument `div_id` de la macro `figure`](--redactors/figures/) qui est utilisé.
* Ceci est compatible avec la configuration via les [fichiers {{meta()}} et les entêtes des fichiers markdown](--custom/metadata/).

L'objet `PyodidePlot` ainsi créé peut ensuite être utilisé en remplacement de `matplotlib.pyplot` : il relaie automatiquement tous les appels de méthodes à la fonction correspondant du module `pyplot`. Deux exceptions à cela :

* La méthode __`PyodidePlot.plot`{.green}__ met en place le tracé de la figure dans la `div` voulue, avant de relayer l'appel à `pyplot.plot`. Il faut donc _{{red("ne surtout pas utiliser")}} `pyplot.plot`{.red}_

* La méthode `PyodidePlot.plot_func` n'existe pas dans le module `pyplot`. C'est un ajout du thème, qui permet d'obtenir en un seul appel le tracé d'une fonction sur un domaine donné, en prenant en charge la construction des ordonnées, le titre éventuel de la figure et l'appel final à `pyplot.show()`.

<br>

!!! warning "Disponibilité de la classe `PyodidePlot`"

    La classe `PyodidePlot` n'est déclarée dans l'environnement pyodide qu'_après_ que `matplotlib` ait été installé.
    <br>Cela signifie que la classe ne peut être utilisée dans une [section](--ide-sections) que si `matplotlib` y est également importé, ou s'il a déjà été importé précédemment.


??? tip "Accéder au code de la classe"

    Il est possible d'utiliser l'un des scripts du thème pour voir le code complet de la classe :

    ```
    python -m pyodide_mkdocs_theme --plot
    ```

    La classe utilisée dans le thème est rigoureusement identique à celle présente dans ce fichier.
    <br>Certaines instructions au tout début du fichier ne correspondent par contre pas au code utilisé dans le thème, car le but de ce fichier est de pouvoir exécuter les fichiers pythons de la documentation utilisant `matplotlib` en local (voir l'admonition suivante).


??? tip "Exécution locale de fichier python utilisant la classe `PyodidePlot`"

    Si vous souhaitez exécuter des fichiers python utilisant `matplotlib` en local, il faut "ruser" un peu, mais le thème fournit les outils nécessaires :

    1. Installer `matplotlib` sur la machine/dans l'environnement, si ce n'est pas déjà fait.

        !!! note "{{orange("Ne pas ajouter `matplotlib`{.orange} au fichier `requirements.txt`{.orange}")}}"
            La bibliothèque `matplotlib` est ici une _dépendance de développement_, et non une dépendance de la documentation.

    1. Utiliser la commande ci-dessous, pour récupérer le fichier `pyodide_plot.py` du thème et le placer à la racine du projet :

            python -m pyodide_mkdocs_theme --plot -F pyodide_plot.py

    1. Ajouter ceci au tout début du fichier python __de la documentation__ que vous voulez exécuter en local :

        ```python
        # --- PYODIDE:ignore --- #
        from pyodide_plot import PyodidePlot
        ```

        Ce code ne sera exécuté qu'en local, les sections `ignore` des fichiers n'étant pas récupérées pour le site construit avec mkdocs.


???+ help "Documentation complète de la classe `PyodidePlot`"

    ::: pyodide_plot.PyodidePlot
        options:
            members_order: "source"






## Exemples & astuces

??? help "Plusieurs figures dans la même page"

    {{ figure_macro_md( 'exemples/pyodide_plot_2', "
{{ IDE('exemples/pyodide_plot_2') }}

!!! help ""
    {{ figure('cible_1') }}

    {{ figure('cible_2') }}
") }}


??? help "Plusieurs courbes sur la même figure"

    {{ figure_macro_md( 'exemples/pyodide_plot_double', "
{{ IDE('exemples/pyodide_plot_double') }}

{{ figure('div_double') }}
") }}


??? help "Modifier la figure tracée avec `plot_func`"

    En passant l'argument `show=False` à la méthode `plot_func`, il est possible de ne pas finaliser l'affichage de la figure, pour pouvoir la modifier.

    {{ figure_macro_md( 'exemples/pyodide_plot_show', "
{{ IDE('exemples/pyodide_plot_show') }}

{{ figure('div_show') }}
") }}

??? help "Garder l'auto-complétion avec `pyplot` durant le développement"

    Les objets `PyodidePlot` évitent certes d'avoir à importer `pyplot`, mais l'auto-complétion n'est plus disponible, ce qui n'est pas toujours très pratique, si le travail de développement est fait en local.

    Mais comme ces objets ne servent que de relais (à l'exception des méthodes `plot` et `plot_func`), il est possible de faire toutes les autres étapes avec le module d'origine :

    {{ figure_macro_md( 'exemples/pyodide_plot_pyplot', "
{{ IDE('exemples/pyodide_plot_pyplot') }}

{{ figure('div_pyplot') }}
") }}
