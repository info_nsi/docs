#-PYODIDE:env-#
to_mermaid = mermaid_figure('term-fig-id')

def generate():
    to_mermaid("""
graph TB
    Graph --- with
    Graph --- btn
    btn --- from
    btn --- terminal
""")
print('Utiliser: generate()')