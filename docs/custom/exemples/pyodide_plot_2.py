# --- PYODIDE:env --- #

import matplotlib       # Indispensable (déclenche la déclaration de PyodidePlot)

plt1 = PyodidePlot('cible_1')
plt2 = PyodidePlot('cible_2')

# --- PYODIDE:code --- #
# plt1, plt2: PyodidePlot

# Tracer directement une fonction
plt1.plot_func(lambda x:x**2, range(1,10), 'r-', title="La fonction carré")

# Les objets PyodidePlot peuvent aussi être utilisés exactement comme matplotlib.pyplot:
xs = [ -2 + n*.1 for n in range(41) ]        # No range floats in 3.8
ys = [ x**3 for x in xs ]

plt2.plot(xs, ys, 'r-')
plt2.grid()     # Optionnel : pour voir le quadrillage
plt2.axhline()  # Optionnel : pour voir l'axe des abscisses
plt2.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt2.title("La fonction cube")
plt2.show()
