# --- PYODIDE:env --- #

import matplotlib       # Indispensable (déclenche la déclaration de PyodidePlot)

fig = PyodidePlot('div_pyplot')

# --- PYODIDE:code --- #
# fig: PyodidePlot

import matplotlib.pyplot as plt

xs = [ -5 + n*.25 for n in range(41) ]        # No range floats in 3.8
ys = [ x**3 for x in xs ]
fig.plot(xs, ys, 'r-')

# En local, on aurait ci-dessous à l'auto-complétion et les suggestions dans l'IDE:
plt.grid()
plt.axhline()
plt.axvline()
plt.title("La fonction cube")
plt.show()
