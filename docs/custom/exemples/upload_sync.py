
# --- PYODIDE:env --- #
print("env!")

if 'data_sync' not in globals():     # define once only
    data_sync = None

def upload(content:str):
    global data_sync
    data_sync = content[:30]+' [...]'  # available on next run
    print("This is run from an event and will show up in the browser's console only! (F12)")

pyodide_uploader(upload)  # This is synch, but upload is run later from an async context!
print('Not waiting...')

# --- PYODIDE:code --- #
print("data:", data_sync or "NOTHING YET!")      # you see content of previous run!

# --- PYODIDE:post --- #
print("post!")
print("Regarder dans la console! (F12)")
