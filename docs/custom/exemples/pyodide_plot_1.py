# --- PYODIDE:env --- #

import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
pmt_plot = PyodidePlot()  # Disponible après l'import de matplotlib

# --- PYODIDE:code --- #

# Tracer directement une fonction:
# pmt_plot: PyodidePlot
pmt_plot.plot_func(lambda x:x**2, range(11), 'r-', title="La fonction carré")
