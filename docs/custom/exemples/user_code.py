# --- PYODIDE:env --- #
def env_test():
    triche = "joujous bijous caillous chous pous genous".split()

    for x in triche:
        assert str(x) not in __USER_CODE__, "~~Code~~ Français non conforme"

try:
    __USER_CODE__
except:
    pass            # exécution en dehors de pyodide, la variable n'existe pas
else:
    env_test()      # c'est l'utilisateur qui joue avec l'IDE
finally:
    del env_test    # suppression de l'environnement si vous l'estimez nécessaire
                    # (pourrait aussi être dans la branche `else`)

# --- PYODIDE:code --- #
# joujous ?