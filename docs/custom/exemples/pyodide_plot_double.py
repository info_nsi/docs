# --- PYODIDE:env --- #

import matplotlib       # Indispensable (déclenche la déclaration de PyodidePlot)

fig = PyodidePlot('div_double')

# --- PYODIDE:code --- #
# fig: PyodidePlot

xs  = [ -5 + n*.25 for n in range(41) ]
ys  = [ x**3 for x in xs ]
xs2 = range(-10,11)
ys2 = [*map( lambda x:x**2, xs2)]

fig.plot(xs, ys, 'r-', xs2, ys2, 'b+')

fig.axhline()
fig.axvline()
fig.title("Les fonctions carré et cube")
fig.show()