# --- PYODIDE:env --- #
print("env!") ; DataURL = str

def upload(img:DataURL):
    import js
    js.document.getElementById("img-target").src = img
    return img

data = await pyodide_uploader_async(upload, read_as='img')
# `data` available NOW!

# --- PYODIDE:code --- #
print("From `PYODIDE:code`, data is now:")
print(data[150:])

# --- PYODIDE:post --- #
print("post!")
