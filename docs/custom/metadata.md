# Gestion de la configuration et des arguments par défauts des macros


Il y a différentes façons de modifier les [options de configuration du plugin `pyodide_macros`](--custom/config/), ce qui inclut la quasi intégralité des arguments par défaut des macros du thème.

<br>


<div class="inline end w35 meta-spacer" markdown>

!!! note margin-top-meta-mkdocs "`mkdocs.yml`"
    ```yaml
    plugins:
        - pyodide_macros:
            # Vos réglages ici
    ```

!!! note margin-top-meta-file "{{meta()}}"
    ```yaml
    # Vos réglages du plugin au
    # niveau de base du fichier
    ```

!!! note margin-top-meta-header "`votre_page.md`"
    ```yaml
    ---
    # Vos réglages en entête,
    # entre les `---`
    ---
    ```

!!! note margin-top-meta-macro "`votre_page.md`"
    ```markdown
    Dans l'appel lui-même :
    {% raw %}{{ macro(...) }}{% endraw %}
    ```

</div>

1. Le point d'entrée évident est bien sûr le fichier `mkdocs.yml`.
<br>Les réglages sont à y faire dans la section `plugins.pyodide_macros`, comme attendu.<br><br>

    {{sep()}}

1. Il est également possible d'utiliser des fichiers de configuration {{meta()}}, placés directement dans la hiérarchie de la documentation, pour modifier une partie de la configuration.
<br>Ces fichiers fonctionnent de manière similaire aux [fichiers `.meta.yml` utilisés par `material`][macros-meta]{: target=_blank } : à chaque fois que l'un d'eux est rencontré sur le disque, les options du plugin qui y sont définies prennent le pas sur les valeurs utilisées jusqu'à ce point là de la documentation.

    {{sep()}}

1. On peut définir les mêmes options pour une page complète, en les définissant en tant que métadonnées dans l'entête d'un fichier markdown.
<br>Les options configurées via les métadonnées ont précédence sur le contenu des fichiers {{meta()}}.

    {{sep()}}

1. Enfin, concernant les arguments des macros, les valeurs passées directement en arguments des macros ont précédence sur toutes les autres méthodes.

    {{sep()}}

<br>


!!! tip "Précédence des sources"

    <div class="img center" markdown>$Plus~petit~=~moins~prioritaire$</div>
    <div class="img center" markdown>$mkdocs.yml~\lt~{{meta(0)}}~\lt~entêtes.md~\lt~{% raw %}{{ macro(...) }}{% endraw %}$</div>

<br>

??? tip "Hiérarchie des options de configuration du plugin..."

    Pour rappel, voici l'intégralité des options de configuration du plugin `pyodide_macros` :

    --8<-- "docs_tools/inclusions/config_plugin.md"





## Les fichiers {{meta()}} {{anchor_redirect(id="les-fichiers-metapmtyml")}} { #meta-pmt-yml-files }

Chaque fois qu'un de ces fichiers est trouvé dans la hiérarchie de la documentation, son contenu est extrait et toutes les données correspondant à des options de configuration du plugin `pyodide_macros` remplacent celles de la configuration utilisée jusque ici, pour tous les fichiers markdown présents dans le même dossier ou n'importe quel sous-dossier (quelle que soit la profondeur).

{{div_svg("docs/assets/meta-yml.svg", kls="runtime inline end w45")}}

Dans le cas exposé ci-contre :

* Le contenu de `mkdocs.yml`{ .green .bgd } s'applique aux fichiers/dossiers en vert.

* Une fois arrivé dans le dossier contenant le fichier {{meta()}}{ .blue .bgd }, les fichiers `.md`{ .cyan .bgd } dans ce dossier ou plus bas dans la hiérarchie verront la fusion du contenu de `mkdocs.yml`{ .green .bgd } avec celui de {{meta()}}{ .blue .bgd }.

* De manière similaire, du côté du fichier {{meta()}}{ .red .bgd } , les fichiers dans ce dossier ou plus bas (en `jaune`{ .yellow .bgd }, donc) verront la configuration du `mkdocs.yml`{ .green .bgd } mise à jour avec avec celle du fichier {{meta()}}{ .red .bgd }.

* Si maintenant le fichier `.md`{ .yellow .bgd } possède des données de configuration en entête, elles prendront le pas sur les réglages provenant des fichiers `mkdocs.yml`{ .green .bgd } et {{meta()}}{ .red .bgd }.

{{sep()}}

Exemples :


=== "Changer les `python_libs`"

    Il est possible d'interdire l'utilisation de certaines [bibliothèques python personnalisées](--custom-libs) en modifiant l'option selon le dossier où l'on se trouve (voir [la page dédiée](--libs-vs-meta) pour des détails techniques à ce sujet).

    <br>

    {{md_include("docs_tools/inclusions/meta_python_libs.md")}}


=== "Conditions examens"

    Voici par exemple un fichier qui ferait en sorte que tous les IDEs dans le dossier `exams` ou ses sous-dossiers :

    - Ne révèleraient pas le code des assertions qui n'ont pas de messages d'erreurs, lors des validations, si le test est échoué.
    - Aient un nombre d'essais infini.
    - SL'erreur normalement levée lorsqu'un IDE a une correction ou des remarques avec un nombre d'essais infini serait supprimée.
    - Il serait également possible d'avoir des tests secrets sans correction ou remarques à révéler (cas qui lève également une erreur, normalement).

    <br>

    !!! help inline end w60 "Contenu du fichier `yml`"

        ```yaml
        args:
            IDE:
                SANS: 'eval exec'
                MAX: 1000
                LOGS: false
        ides:
            forbid_corr_and_REMs_with_infinite_attempts: false
            forbid_hidden_corr_and_REMs_without_secrets: false
            forbid_secrets_without_corr_or_REMs: false
        ```

    !!! help no-margin-top "Organisation des fichiers"

        ```
        ...
        ├── exams
        │   ├── {{meta(0)}}
        │   ├── sujet1
        │   │   ├── exo1.md
        │   │   └── exo2.md
        │   ├── sujet2
        │   │   └── exo.md
        │   ...
        ```

    <br>

    !!! warning "Ceci n'est qu'un exemple..."

        En effet, le thème n'est pas conçu pour ce genre des contextes "examens" (voir les informations concernant [les restrictions](--restrictions))






## Métadonnées dans les entêtes de pages {{anchor_redirect(id="meta-donnees")}} { #pages-meta }

Il est possible d'ajouter les mêmes informations dans l'entête d'un fichier markdown:

!!! help no-margin-top "Contenu du `mkdocs.yml`"

    ```yaml
    ---
    author:
        - Frédéric Zinelli
    hide:
        - navigation
        - toc
    title: 42
    tags:
        - PMT
    ides:
        show_only_assertion_errors_for_secrets: false
        deactivate_stdout_for_secrets: true
        decrease_attempts_on_user_code_failure: 'editor'
    args:
        IDE:
            LOGS: false
    ---

    [ Contenu markdown de la page... ]
    ```


<br>

Noter que :

* Seules les options correspondant au plugin `pyodide_macros` seront récupérées dans les métadonnées et fusionnéeS à la configuration globale.
* Pour que les métadonnées soient valides, elles ne doivent pas contenir de lignes vides.
* Les comportements normaux des métadonnées restent valables. Il est notamment toujours possible d'exploiter n'importe quelle données définie dans les métadonnées durant les appels de macros, comme c'est habituellement le cas avec le plugin [`mkdocs-macros`][macros-meta]{: target=_blank }.
