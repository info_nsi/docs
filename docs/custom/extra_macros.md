

Le plugin `pyodide_macros` hérite de la classe `MacrosPlugin` du plugin [`mkdocs-macros-plugin`][mkdocs-macros]{: target=_blank }, et les différentes façons de déclarer les macros pour celui-ci restent donc toutes applicables avec le thème.

Il est donc toujours possible d'ajouter vos propres macros en plus de celles définies par le thème (voir par exemple le fichier [`main.py` à la racine du dépôt][pmt-repo] de pyodide-mkdocs-theme).


!!! help "Contraintes sur les noms des macros personnalisées"

    Les macros personnalisées ne doivent pas avoir le même nom que l'une de celles définies par le thème ([voir ici](--macros_table)).


## Fichier `main.py`

C'est la méthode la plus simple :

--8<-- "docs_tools/inclusions/macros_main.md"


## Module de macros

Si vous avez beaucoup de code/macros personnalisées, il peut être intéressant d'utiliser un package python plutôt qu'un simple fichier `main.py`, pour obtenir une meilleure organisation du projet :

--8<-- "docs_tools/inclusions/macros_package.md"




## Configuration

Toutes les options du plugin original des macros sont disponibles directement sur le plugin `pyodide_macros`.<br>Pour plus de détails, voir :

* La [page de configuration](--pyodide_macros) du plugin `pyodide_macros` du thème.
* La [documentation du plugin original][mkdocs-macros]{: target=_blank } de `mkdocs-macros-plugin`.



## Indentation et macros multilignes {{anchor_redirect(id="indentation-et-macros-multi-lignes")}} { #multiline-macros-vs-indentations }

Pour les macros qui doivent insérer du contenu multilignes, il est important de pouvoir savoir à quel niveau d'indentation l'appel de la macro en cours a été fait.

Par exemple, pour une macro `#!py admo(lst:List[str])` qui générerait une liste de puces dans une admonition, on souhaite obtenir le résultat suivant :

!!! note inline end w45 "Rendu markdown"

    ```markdown
    !!! tip ""
        * chat
        * chien
        * chaud

    ??? tip "indenté"
        !!! tip ""
            * chat
            * chien
            * chaud
    ```

!!! note no-margin-top "Fichier markdown source"

    ```markdown
    {% raw %}{{ admo(["chat", "chien", "chaud"]) }} {% endraw %}

    ??? tip "indenté"
        {% raw %}{{ admo(["chat", "chien", "chaud"]) }} {% endraw %}

    {{insecable()}}
    {{insecable()}}
    {{insecable()}}
    {{insecable()}}
    {{insecable()}}
    ```

Pour réaliser cela, il faut que la macro sache combien d'espaces elle doit ajouter au début de chaque ligne.

<br>

__`Pyodide-mkdocs-theme` propose une fonctionnalité permettant d'indenter automatiquement le contenu généré par votre macro avec le bon niveau d'indentation : __ `env.indent_macro( contenu )`.

Cette fonctionnalité peut-être étendue aux macros personnalisées, en procédant de la façon suivante :

1. Ajouter le nom de la macro dans la configuration du plugin du thème, dans [`build.macros_with_indents`](--pyodide_macros_build_macros_with_indents) :

    ```yaml
    plugins:
        - pyodide_macros:
            build:
                {{ config_validator("build.macros_with_indents",1)}}:
                    - admo
    ```

    <br>

1. Implanter la macro dans votre fichier `main.py` ou votre module de macros personnalisées (voir en haut de cette page), en omettant la logique d'indentation dans un premier temps :

    ```python
    from typing import List
    from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

    def define_env(env:PyodideMacrosPlugin):

        @env.macro
        def admo(lst:List[str]):
            list_items = '!!! tip ""' + ''.join( f'\n    * { item }' for item in lst )
            return list_items
    ```

    <br>

1. Intégrer au code de la macro l'appel à la méthode `#!py env.indent_macro(content:str)`, qui prend en argument le contenu markdown non indenté, et renvoie le contenu indenté de manière appropriée. :

    ```python
        @env.macro
        def admo(lst:List[str]):
            list_items = '!!! tip ""' + ''.join( f'\n    * { item }' for item in lst )
            indented   = env.indent_macro(list_items)
            return indented
    ```

    <br>

Les appels de macro dans l'exemple [ci-dessus](--multiline-macros-vs-indentations) générerait en l'occurrence les chaînes suivantes :

* Appel 1 :
    ```
    '!!! tip ""\n    * chat\n    * chien\n    * chaud'
    ```

* Appel 2 :
    ```
    '!!! tip ""\n        * chat\n        * chien\n        * chaud'
    ```

Notez que dans la sortie indentée, la première ligne ne doit contenir aucune indentation, puisque l'appel de macro lui-même est déjà indenté dans le fichier markdown source.


!!! warning "Utilisation des macros référencées dans [`build.macros_with_indents`](--pyodide_macros_build_macros_with_indents)"

    * Il ne doit pas y avoir d'autres caractères que des espaces à gauche de ces appels de macros, dans les fichiers markdown.

    * `#!py PyodideMacrosTabulationError` est levée si des caractères de tabulation sont trouvés dans les indentations à gauche de ces appels de macros.

        Il est possible de contourner cette erreur en utilisant l'option {{ config_link('build.tab_to_spaces') }} du plugin, pour remplacer automatiquement les caractères de tabulation.
        <br>{{orange("Garder en tête que la validité des indentations obtenues n'est alors plus garantie")}}.


??? note "Contrôle "plus fin" (?) de l'ajout des indentations"

    S'il s'avérait nécessaire d'exercer un contrôle plus poussé sur l'insertion des indentations, il est également possible d'utiliser la méthode `env.get_macro_indent()`, qui renvoie la chaîne de caractères avec le bon nombre d'espaces qu'il faudrait ajouter au début de chaque ligne, _sauf la première_.

    La macro ci-dessus pourrait alors s'écrire, par exemple :

    ```python
        def admo(lst:List[str]):
            list_items = '!!! tip ""' + ''.join( f'\n    * { item }' for item in lst )
            indent     = env.get_macro_indent()
            return list_items.replace('\n', '\n'+indent)
    ```