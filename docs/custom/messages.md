# Modifier la langue et les messages


Il est possible de personnaliser tous les textes générés par le thème :

- la chaîne de caractères utilisée pour séparer le code utilisateur des tests publics dans les éditeurs de code (voir [`TestsToken`](--messages-details) pour les contraintes spécifiques à ce cas),
- les info-bulles des différents boutons liés au thème,
- les messages affichés dans les terminaux,
- les noms des admonitions (pour les solutions et remarques des macros `{% raw %}{{ IDE(...) }}{% endraw %}`, ou les macros `{% raw %}{{ figures() }}{% endraw %}`),
- ...

Les changements peuvent être ponctuels, mais cela peut aller jusqu'à définir une autre langue !













## Langue utilisée

Le thème propose tous ses messages en différentes langues : {{ get_langs_list() }}.
<br>La langue par défaut est `fr`.

Comme pour [mkdocs-material][material-custom]{: target=_blank } la langue du thème est modifiée dans le fichier `mkdocs.yml` :


<div style="display:grid; width:100%;">
    <div class="row-of-imgs w100">
        <div class="c1 w90">
            <div markdown>
            ```yaml
            theme:
                language: fr
            ```
            </div>
            <img src="!!Lang-FR_png" alt="fr" loading="lazy">
        </div>
        <div class="c2 w90">
            <div markdown>
            ```yaml
            theme:
                language: en
            ```
            </div>
            <img src="!!Lang-EN_png" alt="en" loading="lazy">
        </div>
    </div>
</div>

<br>

??? tip "Add a new language to the theme?"

    If you want another language to be added to the theme, the process would be the following:

    1. Create a fork of [the project][pmt-repo]{: target=_blank }.
    1. Create a `pyodide_mkdocs_theme/pyodide_macros/messages/xx_lang.py` file by copying `en_lang.py` in the same directory, then:

        - Change the class name to `LangXx`
        - Translate all the string arguments of each property of the class
        - Translate also their docstrings

    1. Create a copy of the `pyodide_mkdocs_theme/scripts/en` directory at `pyodide_mkdocs_theme/scripts/xx` then translate the files in `pyodide_mkdocs_theme/scripts/xx/docs`:

        - For `xx/docs/index.md`: no need to translate both `multi_qcm` calls, one will be enough.
        - For `xx/docs/exo.py`: don't forget to translate the function and variable names.

    1. Create an [issue][pmt-issues]{: target=_blank } on the PMT's repository explaining your request and giving the link to the forked repository.
    <br>Alternatively, the individual files could be provided directly in the issue, if you don't want to fork the repository itself.


## Personnalisations


### Guide rapide

Il est possible de modifier tous ces messages en mettant à profit la définition de macros personnalisées.
<br>L'idée générale est la suivante :

1. Récupérez le code python de personnalisation, via [la commande appropriée](--scripts-lang) des [scripts du thème](--scripts/) ou tout en bas de cette page.

1. Placez ce code dans un fichier de macros personnalisées (voir comment faire plus bas, selon qu'il y a déjà des macros personnalisées dans votre projet ou non).

1. Modifiez les messages qui vous concernent. Vous pouvez supprimer les entrées qui ne nécessitent pas de modification.

1. Pour les infos-bulles (objets `Tip`), pensez à adapter leur largeur (en `em`) via le premier argument.

1. _"Et voilà !"_





### Récupérer le code { #scripts-lang }

Les [commandes](--scripts/) ci-dessous permettent d'afficher dans la console le contenu du code python nécessaire pour modifier tous les messages utilisés automatiquement par le thème.

Par exemple:

* En français, par défaut :

    ```code
    python -m pyodide_mkdocs_theme --lang
    ```

<br>

* Dans une langue spécifique, en utilisant l'abréviation appropriée ( {{ get_langs_list('ou') }}) :

    ```code
    python -m pyodide_mkdocs_theme --lang en
    ```

<br>

* Il est aussi possible de créer directement un fichier contenant le code complet, plutôt que de l'afficher dans la console :

    ```code
    python -m pyodide_mkdocs_theme --lang --file "main.py"
    ```

    !!! danger "Argument `--file`"
        Si l'argument `--file` pointe vers un fichier qui existe déjà, ce ficher sera remplacé sans avertissement.




### Intégrer le code de personnalisation au projet

La méthode d'intégration dépend de la préexistence ou non de macros personnalisées dans votre projet.

<br>

* ___Projet sans macros personnalisées___

    Dans ce cas, il suffit d'enregistrer le code de personnalisation dans un fichier `main.py` à la racine du projet.
    <br>Le plus simple est alors d'utiliser la [commande appropriée](--scripts-lang) des scripts du thème en utilisant l'argument `--file`.

<br>

* ___Projet avec un fichier `main.py` existant___

    Le plus simple est alors d'ajouter les imports ainsi que le contenu de la fonction `define_env` du [code de personnalisation](--customization-code) dans votre fichier `main.py`.

<br>

* ___Projet avec un package de macros personnalisées___

    Si vous disposez d'un package de macros, vous saurez sans doute adapter votre package...

    ??? help "Remplacer le fichier main.py par une bibliothèque"

        Si vous souhaitez passer d'un fichier `main.py` à un package, les grandes lignes de l'opération sont les suivantes :<br><br>

        --8<-- "docs_tools/inclusions/macros_package.md"














## Détail des messages {{anchor_redirect(id="detail-des-messages")}} { #messages-details }

=== "Les éditeurs"

    ![Editor infos](!!simple_IDE_png){: loading=lazy .w30 align=right }

    <br>

    Gestion du séparateur entre le code utilisateur et les tests publics, ainsi que de l'info-bulle du bouton pour activer/désactiver ces tests.

    <br>

    ---

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members:
                - tests
                - comments

=== "Exécutions"

    ![Terminal messages](!!terminal_messages_png){: loading=lazy .w30 align=right }

    <br>

    Boutons du terminal et messages usuels vus par l'utilisateur dans les terminaux, lorsque du code est exécuté.

    <br>

    ---

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: alphabetical
            members:
                - feedback
                - wrap_term
                - run_script
                - install_start
                - install_done


=== "Tests: étapes"

    ![Terminal messages](!!terminal_messages_png){: loading=lazy .w30 align=right }

    <br>

    Construction des messages lorsqu'une étape des tests est passée, que ce soit les tests publics ou les validations.

    <br>

    ---

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - secret_tests
                - public_tests
                - validation
                - editor_code
                - success_msg
                - success_msg_no_tests
                - unforgettable


=== "Validations: messages finaux"

    ![Terminal messages](!!terminal_success_failure_png){: loading=lazy .w35 align=right }

    Ces chaînes de caractères servent à construire les messages dans les terminaux lorsque le code de l'utilisateur passe les tests ou bien a consommé tous les essais disponibles.

    | Évènement | Canevas du message |
    |:-:|:-|
    | Succès { .green } | `{success_head} emoji - {success_head_extra}`<br>`{success_tail} {reveal}` |
    | Échec { .orange }  | `{fail_head} - {reveal} {fail_tail}` |

    Dans les deux cas, la chaîne `reveal` est construite en fonction de la présence ou l'absence de correction et remarques :

    | Correction | Remarques | `reveal` |
    |-|-|:-|
    | Oui { .green } | Non { .orange } | `{reveal_corr}` |
    | Oui { .green } | Oui { .green } | `{reveal_corr} {reveal_join} {reveal_rem}` |
    | Non { .orange } | Oui { .green } | `{reveal_rem}` |

    <br>

    ---

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - success_head
                - success_tail
                - fail_head
                - reveal_corr
                - reveal_join
                - reveal_rem
                - success_head_extra
                - fail_tail

=== "Corr & Rem"

    ![Admonition example](!!sol_and_rems_png){: loading=lazy .w35 align=right }

    <br>

    Chaînes de caractères utilisées pour construire l'admonition contenant la solution et les remarques (si elles existent. Ne concerne que les remarques "non visibles", donc pas les contenus des fichiers `{exo}_VIS_REM.md`).

    <br>

    ---

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - title_corr
                - title_rem
                - corr
                - rem

=== "Boutons - IDEs"

    ![Editor infos](!!simple_IDE_png){: loading=lazy .w30 align=right }

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - py_btn
                - play
                - check
                - download
                - upload
                - restart
                - save
                - corr_btn
                - show
                - attempts_left

=== "Qcms"


    ![qcm example](!!qcm_example_png){: loading=lazy .w35 align=right }

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - qcm_title
                - qcm_mask_tip
                - qcm_check_tip
                - qcm_redo_tip

=== "Figures"

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - figure_text
                - figure_admo_title

=== "Divers"

    ::: pyodide_mkdocs_theme.pyodide_macros.messages.Lang
        options:
            show_bases: false
            heading_level: 4
            show_root_heading: false
            members_order: source
            members:
                - tip_trash
                - picker_failure













## Les classes utilisées


### ::: pyodide_mkdocs_theme.pyodide_macros.messages.Msg
    options:
        show_bases: false
        show_root_heading: true


### ::: pyodide_mkdocs_theme.pyodide_macros.messages.MsgPlural
    options:
        show_bases: false
        show_root_heading: true

<br>

!!! note "About messages formatting"

    Here are the available formatting fonctions.


    | Name | Formatting |
    |-|-|
    | `#!py "error"`   | {{red("__Red + bold__")}} |
    | `#!py "info"`    | {{gray("_Grey + italic_")}} |
    | `#!py "italic"`  | {{primary("_Italic_")}} |
    | `#!py "none"`    | {{primary("None")}} (used to override a default formatting if needed) |
    | `#!py "stress"`  | {{primary("__Bold__")}} |
    | `#!py "success"` | {{green("___Green + bold + italic___")}} |
    | `#!py "warning"` | {{orange("___Orange + bold + italic___")}} |

    Notes:

    - In terminals, the default color is defined by the color palette (here, a dark blue).
    - The colors used here are not exactly the same as in terminals.

<br>

### ::: pyodide_mkdocs_theme.pyodide_macros.messages.Tip
    options:
        show_bases: false
        show_root_heading: true

### ::: pyodide_mkdocs_theme.pyodide_macros.messages.TestsToken
    options:
        show_bases: false
        show_root_heading: true
        member: msg













## Code de personnalisation {: #customization-code }

```python

from pyodide_mkdocs_theme.pyodide_macros import (
    PyodideMacrosPlugin,
    Msg, MsgPlural, TestsToken, Tip,
)


def define_env(env:PyodideMacrosPlugin):
    """ The customization has to be done at macro definition time.
        You could paste the code inside this function into your own main.py (or the
        equivalent package if you use a package instead of a single file). If you don't
        use personal macros so far, copy the full code into a `main.py` file at the root
        of your project (note: NOT in the docs_dir!).

        NOTE: you can also completely remove this file if you don't want to use personal
              macros or customize the messages in the built documentation.

        * Change whatever string you want.
        * Remove the entries you don't want to modify
        * Do not change the keyboard shortcuts for the Tip objects: the values are for
          informational purpose only.
        * See the documentation for more details about which string is used for what
          purpose, and any constraints on the arguments:
          https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/messages/#messages-details

        ---

        The signatures for the various objects defined below are the following:

        ```python
        Msg(msg:str)

        MsgPlural(msg:str, plural:str="")

        Tip(width_in_em:int, msg:str, kbd:str=None)

        TestsToken(token_str:str)
        ```
    """

    env.lang.overload({

    # Editors:
        "tests":      TestsToken("\n# Tests\n"),
        "comments":   Tip(17, "(Dés-)Active le code après la ligne <code>{tests}</code> "
                             "(insensible à la casse)", "Ctrl+I"),


    # Terminals
        "feedback":      Tip(19, "Tronquer ou non le feedback dans les terminaux (sortie standard"
                                " & stacktrace / relancer le code pour appliquer)"),
        "wrap_term":     Tip(19, "Si activé, le texte copié dans le terminal est joint sur une "
                                "seule ligne avant d'être copié dans le presse-papier"),


    # Runtime feedback
        "run_script":    Msg("Script lancé...", format='info'),
        "install_start": Msg("Installation de paquets python. Ceci peut prendre un certain temps...", format='info'),
        "install_done":  Msg("Installations terminées !", format='info'),

        "validation":    Msg("Validation - ", format='info'),
        "editor_code":   Msg("Éditeur", format='info'),
        "public_tests":  Msg("Tests publics", format='info'),
        "secret_tests":  Msg("Tests secrets", format='info'),
        "success_msg":   Msg("OK", format='success'),
        "success_msg_no_tests": Msg("Terminé sans erreur.", format='info'),
        "unforgettable": Msg("N'oubliez pas de faire une validation !", format='warning'),


    # Terminals: validation success/failure messages
        "success_head":  Msg("Bravo !", format='success'),
        "success_tail":  Msg("Pensez à lire"),
        "fail_head":     Msg("Dommage !", format='warning'),
        "reveal_corr":   Msg("le corrigé"),
        "reveal_join":   Msg("et"),
        "reveal_rem":    Msg("les commentaires"),
        "success_head_extra":  Msg("Vous avez réussi tous les tests !"),
        "fail_tail":     MsgPlural("est maintenant disponible", "sont maintenant disponibles"),


    # Corr  rems admonition:
        "title_corr": Msg('Solution'),
        "title_rem":  Msg('Remarques'),
        "corr":       Msg('🐍 Proposition de correction'),
        "rem":        Msg('Remarques'),


    # Buttons, IDEs buttons & counter:
        "py_btn":        Tip(9, "Exécuter le code"),
        "play":          Tip(9, "Exécuter le code", "Ctrl+S"),
        "check":         Tip(9, "Valider", "Ctrl+Enter"),
        "download":      Tip(0, "Télécharger"),
        "upload":        Tip(0, "Téléverser"),
        "restart":       Tip(0, "Réinitialiser l'éditeur"),
        "save":          Tip(0, "Sauvegarder dans le navigateur"),
        "corr_btn":      Tip(0, "Tester la correction (serve)"),
        "show":          Tip(0, "Afficher corr & REMs"),
        "attempts_left": Msg("Évaluations restantes"),


    # QCMS
        "qcm_title":     MsgPlural("Question"),
        "qcm_mask_tip":  Tip(11, "Les réponses resteront cachées..."),
        "qcm_check_tip": Tip(11, "Vérifier les réponses"),
        "qcm_redo_tip":  Tip(9,  "Recommencer"),


    # Others
        "tip_trash": Tip(15, "Supprimer du navigateur les codes enregistrés pour {site_name}"),


        "figure_admo_title": Msg("Votre figure"),
        "figure_text":       Msg("Votre tracé sera ici"),


        "picker_failure": Msg(
            "Veuillez cliquer sur la page entre deux utilisations des raccourcis clavier ou "
            "utiliser un bouton, afin de pouvoir téléverser un fichier."
        )
    })

```
