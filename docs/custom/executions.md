# Exécutions / Runtime



Cette page contient des informations supplémentaires concernant la gestion des IDEs ou des terminaux durant le "runtime", ainsi que ce qui touche aux restrictions (de près ou de loin).

La structure et l'agencement des fichiers pythons {{annexes()}} eux-mêmes sont discutés [ici pour les IDEs](--redactors/IDE-details/) et [là pour les terminaux](--macro-terminal).

Pour des gestions de fichiers plus complexes (bibliothèques, extraction de fichiers distants, ...) voir la [page dédiée](--custom/files/).




## Exécutions des codes python { #runtime }


{{ md_include("docs_tools/inclusions/runtime.md") }}



## Extraction codes et commandes utilisateur { #user_code }

Il est possible d'accéder depuis le code python au code de l'utilisateur au moment où les exécutions sont lancées (contenu de l'éditeur quand il existe, commande exécutée depuis un terminal).

* Le code de l'éditeur est contenu dans une variable cachée nommée `__USER_CODE__` et est accessible depuis n'importe quelle étape des exécutions. S'il n'y a pas d'éditeur associé (ex: terminal isolé), la variable contient une chaîne vide.
* Si les exécutions sont lancées depuis un terminal, la commande utilisée est stockée dans une variable cachée nommée `__USER_CMD__`. Lorsque le terminal n'est pas utilisé, la variable contient une chaîne vide.

Ces variables peuvent être utilisées pour faire des vérifications additionnelles, en plus des fonctionnalités proposées avec les restrictions classiques (voir [argument `{% raw %}{{ IDE(... SANS=...) }}{% endraw %}`](--IDE-SANS)).

<br>

=== "Exemple terminal"

    À titre d'exemple, voici une utilisation de `__USER_CMD__`, pour empêcher l'utilisateur d'exécuter des commandes dans le terminal :

    {{ IDE_py('exemples/user_cmd', MAX_SIZE=3, TERM_H=8, before='!!!') }}

=== "Exemple éditeur"

    Le but est cette fois d'avoir un fichier python qui puisse tourner aussi en local, en dehors de l'environnement de pyodide (à supposer que le reste du fichier le permette également).

    {{ IDE_py('exemples/user_code', MAX_SIZE=3, TERM_H=8, before='!!!') }}

<br>

???+ danger "Important"

    * L'utilisateur a aussi accès à ces variables, s'il en connaît l'existence, donc un rédacteur devrait y faire appel uniquement depuis la [section `env`](--ide-sections) pour `__USER_CODE__` et depuis la [section `env_term`](--ide-sections) pour `__USER_CMD__`, c'est-à-dire avant que le code de l'utilisateur ne puisse lui-même y accéder (et donc potentiellement les modifier).

    * Si le but est de connaître le nombre de caractères du code de l'utilisateur, ne pas oublier que les [tests publics](--ide-sections) _font partie du contenu de l'éditeur_.

    * Si des restrictions sont appliquées sur le code de l'éditeur sans utiliser les [arguments `SANS`](--IDE-SANS) des IDEs et terminaux, garder en tête que si les commandes du terminal ne sont pas également analysées, l'utilisateur pourrait mettre ce dernier à profit pour y définir différentes choses, et ainsi rendre des données disponibles dans l'environnement de l'éditeur sans taper de code dans l'éditeur lui-même (voir aussi la fonction `clear_scope`, dans l'admonition ci-dessous).

    * {{orange("_Il est très difficile d'écrire de bonnes restrictions !_")}} (surtout en python...)

        De bonnes restrictions devraient:

        1. Être efficaces (au sens de _contraignantes_).
        1. Ne pas provoquer de faux positifs (ou alors le moins possible).
        1. Ne pas rendre l'expérience de l'utilisateur horrible, de par leur fonctionnement (mauvais feedback, erreurs incompréhensibles, comportements inattendus d'objets ou fonctions classiques sans avertissement ou feedback, ...).

        <br>D'expérience, si vous vous lancez dans ce genre de choses, il y a fort à parier que vous n'arriviez pas à un résultat satisfaisant les trois points à la fois.
        <br>Les restrictions déjà proposées via l'argument [`SANS`](--IDE-SANS) des macros sont ce qui s'en rapproche le plus et je déconseille fortement d'en mettre d'autres en place. Par ailleurs, il est probable que vous ne puissiez pas mélanger facilement votre propre logique à celle issue de l'utilisation de l'argument `SANS`, sauf à explorer le code du thème pour voir comment vous y adapter...


??? danger "`clear_scope(skip=())`"

    _À but de complétude uniquement_, mais encore une fois, ___l'utilisation de tout ceci est déconseillée___.

    Une fonction `clear_scope`, masquée mais accessible depuis n'importe où, permet de supprimer tout ce qui a été défini dans l'environnement de l'utilisateur depuis le démarrage de Pyodide.<br>
    Elle peut permettre de s'assurer que l'utilisateur ne définit pas des choses qu'il n'est pas sensé définir via un terminal, pour contourner certaines restrictions. Il est également possible de passer un itérable en argument, contenant les noms d'objets ou de variables à ne pas effacer de l'environnement.


    Si cette fonction est utilisée, elle devrait l'être depuis la section `env`, pour les mêmes raisons que précédemment (et également pour que cela n'affecte pas les comportements liées aux restrictions de codes).





## Fonctionnement des restrictions { #restrictions }

Voici quelques informations supplémentaires sur la façon dont les restrictions de code fonctionnent ([argument `SANS`](--IDE-SANS) des macros `IDE`).


### Philosophie { #restrictions-core-ideas }

!!! warning "But recherché avec les restrictions"

    {{ md_include("docs_tools/inclusions/restrictions_goals.md") }}

!!! warning "Efficacité des interdictions"

    {{ md_include("docs_tools/inclusions/restrictions_efficiency.md") }}



### Codes concernés { #restrictions-what-codes }

Les restrictions ne sont appliquées que dans certaines sections/situations :

{{ md_include("docs_tools/inclusions/restrictions_where.md") }}





### Méthodes {{anchor_redirect(id="interdictions-de-methodes")}} { #restrictions-methods }

!!! tip "Interdictions de méthodes"

    * `SANS=".method1 .method2"` (point + identifiant)
    * Recherche textuelle des méthodes dans le code

Lors des [exécutions](--runtime), avant de lancer le code actuellement dans l'éditeur, toutes les chaînes commençant par un point sont recherchées dans le contenu de l'éditeur. Si l'une d'elle est trouvée, une erreur est levée et les exécutions sont stoppées.

!!! danger "Risques de faux positifs"

    Cette approche fait que le contenu des commentaires dans l'éditeur peut créer des faux positifs :

    ```python title="SANS='.count'"
    def user():
        # Ne pas utiliser la méthode .count !   <<< Ceci suffit pour lever une erreur !
        pass
    ```



### Fonctions {{anchor_redirect(id="autres-interdictions")}} { #restrictions-functions }

!!! tip "Interdictions de méthodes"

    * `SANS="min sorted"` (identifiant)
    * Les appels de fonctions lèvent une erreur.

Les interdictions de fonctions sont mises en place en remplaçant les fonctions d'origine dans l'environnement global par des versions qui lèvent une erreur lorsqu'elles sont appelées.

Ceci présente des avantages et des inconvénients :

* Point positif: les éléments interdits mis dans des commentaires ne vont pas déclencher de faux positifs.
* Contrainte : ces interdictions concernent tous les codes susceptibles d'utiliser la fonction de l'utilisateur, donc elles s'appliquent aux codes des [sections](--ide-sections) `code`, `tests` et `secrets`. Il n'est donc "pas possible" d'utiliser les functions ou modules interdits depuis les tests pour valider les réponses du l'utilisateur...

Exemple :

```python title="SANS='sorted'"
# sorted(arr)               # ce commentaire ne lève pas d'erreur

def func1(arr:list):
    return sorted(arr)      # lève une erreur si func1 est appelée

f = sorted                  # pas d'erreur car pas d'appel !

def func2(arr:list):
    return f(arr)           # lève une erreur si func2 est appelée !
```


### Modules { #restrictions-packages }

!!! tip "Interdictions de modules / bibliothèques"

    * `SANS="numpy heapq"` (identifiant ne correspondant ___pas___ à une fonction définie dans l'environnement)
    * Les imports lèvent une erreur.

Les interdictions de modules/bibliothèques fonctionnent de manière similaire aux interdictions de fonctions : la fonction d'import originale est remplacée par une autre, qui se comporte normalement pour les modules autorisés, mais qui lève une erreur si l'utilisateur tente d'importer un module interdit.

Exemple :

```python title="SANS='numpy'"
# Chacune de ces instructions lèverait une erreur :
import numpy
import numpy as np
from numpy import array
```

!!! warning "Ne pas importer de module interdits directement dans l'environnement depuis les sections `env`, `env_term`, ... !"

    Les restrictions ne sont pas appliquées aux [sections](--ide-sections) dites "d'environnement" (celles exécutées [`async`](--async)), donc il est possible d'y importer des modules interdits à l'utilisateur. Il ne faut cependant pas oublier qu'une fois un import effectué, le module est ensuite disponible dans le scope dans lequel il a été importé.

    Il faut donc importer les modules interdit depuis l'intérieur d'une fonction pour éviter que l'utilisateur ne puisse accéder au module directement : c'est en effet l'utilisation de la fonction d'importation qui impose la restriction.




### Levée des restrictions {{anchor_redirect(id="verifications-en-fin-dexecutions")}} { #restrictions-remove }

Une fois les codes utilisateur/tests/la commande exécutés, les restrictions sont levées, mais le code faisant cela va d'abord vérifier que les fonctions restreintes n'ont pas été modifiées ou redéfinies par l'utilisateur. Si c'est le cas, une erreur est levée.



### Work around... { data-search-exclude }

Si vous souhaitez tout de même utiliser des fonctions interdites depuis les tests secrets, il est en fait possible de récupérer les fonctions originales et de les utiliser. Notez cependant qu'il est alors indispensable d'exécuter les tests `secrets` dans une fonction afin de ne pas définir la fonction d'origine dans la scope de l'utilisateur, ce qui, outre le fait de la rendre disponible aussi pour lui, lèverait une erreur lors de la vérification finale des restrictions (voir point précédent).

```python title="Extraction des builtins d'origine"
def test_anti_leak_function():
    sorted = __move_forward__('sorted')
    arr = [1,3,7,1,25,8,5,2]
    exp = sorted(arr)
    assert func(arr) == exp, "some message"

def test_anti_leak_import():
    __import__ = __move_forward__('__import__')
    module = __import__("forbidden_module")

    assert ...


test_anti_leak_function() ; del test_anti_leak_function
test_anti_leak_import() ; del test_anti_leak_import
```

Le [décorateur `auto_run`](--tests-auto_run) peut aider à simplifier l'écriture des tests, en évitant d'avoir à appeler les fonctions à la main.