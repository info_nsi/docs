

## Architecture globale { #global-architecture }

Le plugin `PyodideMacrosPlugin` peut être configuré via le fichier `mkdocs.yml`, les options disponibles étant les suivantes :

<br>

--8<-- "docs_tools/inclusions/config_plugin.md"




## Détails

<br INSERTION_TOKEN>


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.ArgsConfig') }}`#!py ArgsConfig` { #pyodide_macros.args .doc .doc-heading  }
Bases: `#!py CopyableConfig`

Réglages des arguments par défaut accessibles pour les différentes macros du thème.

Explications détaillées dans la page [Aide rédacteurs/Résumé](--redactors/resume/).


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig') }}`#!py BuildConfig` { #pyodide_macros.build .doc .doc-heading  }
Bases: `#!py CopyableConfig`

Réglages concernant la construction de la documentation ou qui impactent la façon dont le contenu des pages est construit.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.deprecation_level') }}`#!py deprecation_level = C.Choice(('error', 'warn'), default='error')` { #pyodide_macros.build.deprecation_level .doc .doc-heading .fake-h4 }
Comportement utilisé lors d'un build/serve lorsqu'une option obsolète est utilisée.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.encrypted_js_data') }}`#!py encrypted_js_data = C.Type(bool, default=True)` { #pyodide_macros.build.encrypted_js_data .doc .doc-heading .fake-h4 }
Si `True`, les données de configuration des IDEs, terminaux et py_btns sont encodées. Si des problèmes de décompression des données sont rencontrés, cette option peut être désactivée.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.forbid_macros_override') }}`#!py forbid_macros_override = C.Type(bool, default=True)` { #pyodide_macros.build.forbid_macros_override .doc .doc-heading .fake-h4 }
Si `True`, `PyodideMacrosError` est levée lorsque deux macros du même nom sont enregistrées par le plugin.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.ignore_macros_plugin_diffs') }}`#!py ignore_macros_plugin_diffs = C.Type(bool, default=False)` { #pyodide_macros.build.ignore_macros_plugin_diffs .doc .doc-heading .fake-h4 }
Passer à `#!py False` pour éviter la vérification de compatibilité de la configuration du plugin `PyodideMacroPlugin` avec celle du plugin original des macros, `MacrosPlugin`.

??? note "Raisons de cette vérification"

    Le plugin du thème hérite de celui de la bibliothèque `mkdocs-macros-plugin`, `PyodideMacros`. Or, la configuration du plugin `MacrosPlugin` est faite "à l'ancienne", avec `config_scheme`, alors que celle de `PyodideMacroPlugin` utilise les classes `Config` disponibles à partir de mkdocs 1.5+. Les deux étant incompatibles, cela à imposé de reporter en dur la configuration du plugin d'origine dans celui du thème.

    Ceci fait qu'une modification de la configuration du plugin d'origine pourrait rendre celui du thème inopérant sans préavis. Si une différence est constatée entre les deux configurations, le build est donc avorté car il n'y a aucune garantie que le site construit puisse encore être correct. Cette vérification permet donc d'assurer que le comportement des objets `MacrosPlugin` sera celui attendu.

    Si les modifications de `MacrosPlugin` sont mineures, il est possible qu'un build puisse tout de même fonctionner, et passer cette option à `#!py False` permettra donc de faire l'essai. <br>À utiliser à vos risques et périls, dans ce cas...



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.load_yaml_encoding') }}`#!py load_yaml_encoding = C.Type(str, default='utf-8')` { #pyodide_macros.build.load_yaml_encoding .doc .doc-heading .fake-h4 }
Encodage à utiliser lors du chargement de données YAML avec les fonctionnalités originales de MacrosPlugin :

La méthode d'origine n'utilise aucun argument d'encodage, ce qui peut entraîner des comportements différents entre Windows et Linux (typiquement : lors de l'exécution d'une pipeline par rapport au travail local sous Windows).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.macros_with_indents') }}`#!py macros_with_indents = C.ListOfItems(C.Type(str), default=[])` { #pyodide_macros.build.macros_with_indents .doc .doc-heading .fake-h4 }
Permet d'enregistrer des macros personnalisées (liste de chaînes de caractères), qui insèrent du contenu markdown multilignes, pour pouvoir indenté  correctement le contenu dans la page :

Une fois qu'une macro est enregistrée dans cette liste, elle peut appeler la méthode `env.indent_macro(markdown)` durant son exécution pour que le contenu généré soit indenté correctement par le plugin.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.meta_yaml_encoding') }}`#!py meta_yaml_encoding = C.Type(str, default='utf-8')` { #pyodide_macros.build.meta_yaml_encoding .doc .doc-heading .fake-h4 }
Encodage utilisé pour charger les [fichiers `.meta.pmt.yml`](--custom/metadata/).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.python_libs') }}`#!py python_libs = C.ListOfItems(C.Type(str), default=['py_libs'])` { #pyodide_macros.build.python_libs .doc .doc-heading .fake-h4 }
Liste de répertoires de [bibliothèques python](--custom-libs) qui doivent être importables dans Pyodide.

Une erreur est levée si :

* Le nom donné ne correspond pas à un répertoire existant (sauf s'il s'agit de la valeur par défaut, `#!py "py_libs"`).
* Le répertoire n'est pas situé à la racine du projet.
* Le répertoire n'est pas un paquet Python (c'est-à-dire qu'il ne contient pas de fichier `__init__.py`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.skip_py_md_paths_names_validation') }}`#!py skip_py_md_paths_names_validation = C.Type(bool, default=False)` { #pyodide_macros.build.skip_py_md_paths_names_validation .doc .doc-heading .fake-h4 }
Par défaut, les noms de chemin de tous les fichiers `.py` et `.md` présents dans le `docs_dir` sont vérifiés pour s'assurer qu'ils ne contiennent aucun caractère autre que des lettres, des chiffres, des points ou des tirets. Cela garantit le bon fonctionnement des macros liées aux IDEs.

Si des caractères indésirables sont détectés, une erreur de type `BuildError` est levée. Cependant, cette vérification peut être désactivée en assignant `True` à ce paramètre. ... À Utiliser  à vos risques et périls.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.tab_to_spaces') }}`#!py tab_to_spaces = C.Type(int, default=-1)` { #pyodide_macros.build.tab_to_spaces .doc .doc-heading .fake-h4 }
Si cette option est définie avec une valeur positive (ou nulle), les tabulations trouvées avant un appel à une macro multiligne seront automatiquement converties en utilisant ce nombre d'espaces (voir l'option [`macros_with_indent`](--pyodide_macros_build_macros_with_indents)). <br>_Aucune garantie n'est alors donnée quant à la correction du résultat_.

Si une conversion est effectuée, un avertissement sera affiché dans la console pour faciliter la localisation et la modification des appels de macros responsables du warning.

!!! warning "Éviter les caractères de tabulation dans la documentation"

    Régler votre éditeur de code de manière à ce qu'il remplace automatiquement les tabulations par des espaces.

    Les caractères de tabulation ne sont pas toujours interprétés de la même façon selon le contexte d'utilisation du fichier, tandis que les fichiers markdown reposent en bonne partie sur les indentations pour définir la mise en page des rendus. <br>Les tabulations sont donc à proscrire.


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig') }}`#!py IdesConfig` { #pyodide_macros.ides .doc .doc-heading  }
Bases: `#!py CopyableConfig`

Réglages spécifiques aux IDEs (comportements concernant l'utilisateur et les exécutions).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.deactivate_stdout_for_secrets') }}`#!py deactivate_stdout_for_secrets = C.Type(bool, default=True)` { #pyodide_macros.ides.deactivate_stdout_for_secrets .doc .doc-heading .fake-h4 }
Détermine si la sortie standard (stdout) sera visible dans les terminaux lors des tests secrets ou non.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.decrease_attempts_on_user_code_failure') }}`#!py decrease_attempts_on_user_code_failure = C.Choice(('editor', 'public', 'secrets', True, False), default='editor')` { #pyodide_macros.ides.decrease_attempts_on_user_code_failure .doc .doc-heading .fake-h4 }
En dehors des étapes d'environnement, les validations sont constituées de 3 étapes:

1. `#!py "editor"` : code dans l'éditeur de l'IDE (y compris l'état actuel des tests publics).
1. `#!py "public"` : version __originale__ des tests publics (section `tests` dans le fichier python).
1. `#!py "secrets"` : section `secrets` du fichier python.

Les exécutions étant stoppées à la première erreur rencontrée, cette option définit à partir de quelle étape une erreur doit consommer un essai.

--8<-- "docs_tools/inclusions/decrease_attempts_on_user_code_failure.md"

??? warning "Options booléennes"

    Les valeurs booléennes sont là uniquement pour la rétrocompatibilité et un warning apparaîtra dans la console si elles sont utilisées.

    * `True` correspond à `#!py "editor"`
    * `False` correspond à `#!py "secrets"`



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.encrypt_alpha_mode') }}`#!py encrypt_alpha_mode = C.Choice(('direct', 'shuffle', 'sort'), default='direct')` { #pyodide_macros.ides.encrypt_alpha_mode .doc .doc-heading .fake-h4 }
Les contenus (codes, corrections & remarques) sont transmis de mkdocs au environnement JS en utilisant des données compressées. L'encodage est réalisé avec l'algorithme LZW, et cette option contrôle la manière dont l'alphabet/la table initiale est construits à partir du contenu à encoder :

- `#!py "direct"` : l'alphabet utilise les symboles dans l'ordre où ils sont trouvés dans le contenu à compresser.
- `#!py "shuffle"` : l'alphabet est mélangé aléatoirement.
- `#!py "sort"` : les symboles sont triés dans l'ordre naturel.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.encrypt_corrections_and_rems') }}`#!py encrypt_corrections_and_rems = C.Type(bool, default=True)` { #pyodide_macros.ides.encrypt_corrections_and_rems .doc .doc-heading .fake-h4 }
Si activé, le contenu de la div HTML de la correction et des remarques, sous les IDEs, sera compressé lors de la construction du site.

Désactiver ceci peut être utile durant le développement, mais {{ red("cette option doit toujours être activée pour le site déployé") }}, sans quoi la barre de recherche pourraient suggérer le contenu des corrections et des remarques à l'utilisateur.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.forbid_corr_and_REMs_with_infinite_attempts') }}`#!py forbid_corr_and_REMs_with_infinite_attempts = C.Type(bool, default=True)` { #pyodide_macros.ides.forbid_corr_and_REMs_with_infinite_attempts .doc .doc-heading .fake-h4 }
Lors de la construction des IDEs, si une section `corr`, un fichier `REM` ou ` VIS_REM` existent et que le nombre de tentatives est illimité, ce contenu ne sera jamais accessible à l'utilisateur, sauf s'il réussit les tests.

Par défaut, cette situation est considérée comme invalide et une `BuildError` sera levée. Si ce comportement est souhaité, passer cette option à `False`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.forbid_hidden_corr_and_REMs_without_secrets') }}`#!py forbid_hidden_corr_and_REMs_without_secrets = C.Type(bool, default=True)` { #pyodide_macros.ides.forbid_hidden_corr_and_REMs_without_secrets .doc .doc-heading .fake-h4 }
Lors de la construction des IDEs, le bouton de validation n'apparaît que si une section `secrets` existe. <br>Si des sections `corr` ou des fichiers `REM` existent alors qu'aucune section `secrets` n'est présente, leur contenu ne sera jamais disponible pour l'utilisateur en raison de l'absence de bouton de validation dans l'interface.

Par défaut, cette situation est considérée comme invalide et une `BuildError` sera levée. Si ce comportement est souhaité, passer cette option à `False`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.forbid_secrets_without_corr_or_REMs') }}`#!py forbid_secrets_without_corr_or_REMs = C.Type(bool, default=True)` { #pyodide_macros.ides.forbid_secrets_without_corr_or_REMs .doc .doc-heading .fake-h4 }
Par défaut, cette situation est considérée comme invalide et une `BuildError` sera levée. Si ce comportement est souhaité, passer cette option à `False`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.show_only_assertion_errors_for_secrets') }}`#!py show_only_assertion_errors_for_secrets = C.Type(bool, default=False)` { #pyodide_macros.ides.show_only_assertion_errors_for_secrets .doc .doc-heading .fake-h4 }
Si activé (`True`), la stacktrace des messages d'erreur sera supprimée et seuls les messages des assertions resteront inchangées lorsqu'une erreur sera levée pendant les tests secrets.

| `AssertionError` | Pour les autres erreurs |
|:-:|:-:|
| {{ pmt_note("Option à `false`",0) }} ![AssertionError: message normal](!!show_assertions_msg_only__assert_full_png) | {{ pmt_note("Option à `false`",0) }} ![Autres erreurs: message normal](!!show_assertions_msg_only__error_full_png) |
| ![AssertionError: sans stacktrace](!!show_assertions_msg_only_assert_no_stack_png){{ pmt_note("Option à `true`") }} | ![Autres erreurs sans stacktrace ni message](!!show_assertions_msg_only_error_no_stack_png){{ pmt_note("Option à `true`") }} |


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.QcmsConfig') }}`#!py QcmsConfig` { #pyodide_macros.qcms .doc .doc-heading  }
Bases: `#!py CopyableConfig`

Réglages spécifiques aux QCMs.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.QcmsConfig.forbid_no_correct_answers_with_multi') }}`#!py forbid_no_correct_answers_with_multi = C.Type(bool, default=True)` { #pyodide_macros.qcms.forbid_no_correct_answers_with_multi .doc .doc-heading .fake-h4 }
Si désactivé (`False`), une question sans réponse correcte fournie, mais marquée comme `multi=True`, est considérée comme valide. Si cette option est réglée à `True`, cette situation lèvera une erreur.


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TermsConfig') }}`#!py TermsConfig` { #pyodide_macros.terms .doc .doc-heading  }
Bases: `#!py CopyableConfig`

Réglages spécifiques aux terminaux.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TermsConfig.cut_feedback') }}`#!py cut_feedback = C.Type(bool, default=True)` { #pyodide_macros.terms.cut_feedback .doc .doc-heading .fake-h4 }
Si activé (`True`), les entrées affichées dans les terminaux sont tronquées si elles sont trop longues, afin d'éviter des problèmes de performances d'affichage des outils `jQuery.terminal`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TermsConfig.stdout_cut_off') }}`#!py stdout_cut_off = C.Type(int, default=200)` { #pyodide_macros.terms.stdout_cut_off .doc .doc-heading .fake-h4 }
Nombre maximal de lignes restant affichées dans un terminal : si de nouvelles lignes sont ajoutées, les plus anciennes sont éliminées au fur et à mesure.

??? note "Performances d'affichage des terminaux"

    ___Les éléments `jQuery.terminal` deviennent horriblement lents lorsque le nombre de caractères affichés est important.___

    Cette option permet de limiter ces problèmes de performance lorsque la sortie standard n'est pas tronquée (voir le bouton en haut à droite du terminal).

    Noter par contre que cette option _ne limite pas_ le nombre de caractères dans une seule ligne, ce qui veut dire qu'une page figée est toujours possible, tandis que l'option de troncature, `cut_feedback` évitera ce problème aussi.


<br><br>

{{sep()}}

<br INSERTION_TOKEN_2>


### {{anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.PyodideMacrosConfig')}} `#!py PyodideMacroConfig` { #pyodide_macros .doc .doc-heading  }
Bases: `#!py CopyableConfig`

La configuration du plugin, `PyodideMacrosConfig`, reprend également toutes les options du plugin original `MacrosPlugin`, ce qui permet d'en réutiliser toutes les fonctionnalités. <br>Ces options, décrites succinctement ci-dessous, sont disponibles à la racine de la configuration du plugin, dans `mkdocs.yml:plugins.pyodide_macros` (voir [en haut de cette page](--global-architecture)).

Pour plus d'informations à leur sujet ou concernant le fonctionnement general des macros :

- [GitHub repository][mkdocs-macros]{: target=_blank }
- [Help page](https://mkdocs-macros-plugin.readthedocs.io/en/latest/){: target=_blank }
- [Configuration information](https://mkdocs-macros-plugin.readthedocs.io/en/latest/# configuration-of-the-plugin){: target=_blank }



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.include_dir') }}`#!py include_dir = C.Type(str, default='')` { #pyodide_macros.include_dir .doc .doc-heading .fake-h4 }
Répertoire de [fichiers externes à inclure][macros-include_dir]{: target=_blank }.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.include_yaml') }}`#!py include_yaml = C.ListOfItems(C.Type(str), default=[])` { #pyodide_macros.include_yaml .doc .doc-heading .fake-h4 }
Pour inclure des [fichiers de données externes][macros-include_yaml]{: target=_blank }.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_block_end_string') }}`#!py j2_block_end_string = C.Type(str, default='')` { #pyodide_macros.j2_block_end_string .doc .doc-heading .fake-h4 }
Pour changer la syntaxe par défaut des fermetures de blocs Jinja2 (défaut: {% raw %}`%}`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_block_start_string') }}`#!py j2_block_start_string = C.Type(str, default='')` { #pyodide_macros.j2_block_start_string .doc .doc-heading .fake-h4 }
Pour changer la syntaxe par défaut des ouvertures de blocs Jinja2 (défaut: {% raw %}`{%`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_variable_end_string') }}`#!py j2_variable_end_string = C.Type(str, default='')` { #pyodide_macros.j2_variable_end_string .doc .doc-heading .fake-h4 }
Pour changer la syntaxe par défaut des fermetures de variables Jinja2 (défaut: {% raw %}`}}`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_variable_start_string') }}`#!py j2_variable_start_string = C.Type(str, default='')` { #pyodide_macros.j2_variable_start_string .doc .doc-heading .fake-h4 }
Pour changer la syntaxe par défaut des ouvertures de variables Jinja2 (défaut: {% raw %}`{{`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.module_name') }}`#!py module_name = C.Type(str, default='main')` { #pyodide_macros.module_name .doc .doc-heading .fake-h4 }
Nom du module/package python contenant vos macros personnalisées, filtres et variables. Utiliser nom de fichier (sans extension), un nom de dossier, ou un chemin relatif (dossiers séparés par des slashes: `dossier/module`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.modules') }}`#!py modules = C.ListOfItems(C.Type(str), default=[])` { #pyodide_macros.modules .doc .doc-heading .fake-h4 }
Liste de [pluglets][macros-pluglets]{ target=_blank } à ajouter aux macros (= modules de macros qui peuvent être installés puis listés  avec `pip list`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.on_error_fail') }}`#!py on_error_fail = C.Type(bool, default=False)` { #pyodide_macros.on_error_fail .doc .doc-heading .fake-h4 }
Interrompt le `build` si une erreur est levée durant l'exécution d'une macro.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.on_undefined') }}`#!py on_undefined = C.Type(str, default='keep')` { #pyodide_macros.on_undefined .doc .doc-heading .fake-h4 }
Comportement à adopter quand une macro rencontre une variable non définie lors des rendus. Par défaut, les expressions Jinja ne sont alors pas modifiées dans la page markdown. Utiliser `'strict'` pour provoquer une erreur.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.render_by_default') }}`#!py render_by_default = C.Type(bool, default=True)` { #pyodide_macros.render_by_default .doc .doc-heading .fake-h4 }
Exécute les macros dans toutes les pages ou non.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.verbose') }}`#!py verbose = C.Type(bool, default=False)` { #pyodide_macros.verbose .doc .doc-heading .fake-h4 }
Affiche plus d'informations dna le terminal sur les étapes de rendu des macros si passé à `True` lors d'un build/serve.


<br INSERTION_TOKEN_3>