
# Les bibliothèques python dans Pyodide et le thème { #python-libs }


## Packages natifs

L'environnement de [Pyodide][Pyodide]{: target=_blank } propose la quasi intégralité des packages qui viennent avec une installation locale de python. Il y a cependant quelques exceptions notables.

Voici {{red("ceux qui ne sont pas disponibles ou ne fonctionnent pas")}} dans l'environnement de pyodide :

* `turtle`
* `turtledemo`
* `tkinter`
* `multiprocessing`
* `threading`
* `sockets`
* `requests`
* `urllib`

Pour plus d'informations à ce sujet, se [référer à la documentation de Pyodide][pyodide-modules]{: target=_blank }.


## Packages de PyPI

Tous les packages de [`PyPI`][PyPI]{: target=_blank } qui mettent à disposition un format `.whl` ("wheel") peuvent être automatiquement téléchargés et installés par le thème, via les fonctionnalités proposées par [`Pyodide`][Pyodide]{: target=_blank }.

![installation messages](!!terminal_messages_png){: loading=lazy .w40 align=right }

Notez cependant que le processus est relativement long, surtout pour des packages comme [`numpy`][numpy]{: target=_blank } ou [`matplotlib`][matplotlib]{: target=_blank }...

Pour cette raison, un message est affiché dans les terminaux, annonçant à l'utilisateur le début et la fin des installations de packages.

Voir aussi le [commentaire concernant la sécurité](--security).







## Bibliothèques personnalisées {{anchor_redirect(id="bibliotheques-personnalisees")}} { #custom-libs }

!!! tip inline w45 end margin-top-packages "Packages & `__init__.py`"

    Pour des informations sur les packages python, se référer à la [documentation en ligne][package-python]{: target=_blank } sur python.org.

Si vous avez besoin de mutualiser du code pour différents exercices, le thème propose la logistique pour automatiser cela.

<br><br>

!!! help "Des archives `.zip` à la racine durant un `serve`..."

    Pour chaque bibliothèque déclarée, une archive `.zip` temporaire sera créée à la racine du projet, lors du build. Elles seront supprimées automatiquement en fin de build.

    Si une erreur a été levée et que la ou les archives sont toujours présentes, elles seront écrasées puis supprimées au prochain build. Il n'est pas nécessaire de gérer manuellement ces fichiers.



### Guide rapide

1. Créer un dossier `py_libs` à la racine du projet.
{{ pmt_note("Chaque sous-dossiers doit contenir son propre fichier `__init__.py`{.pmt-note}.") }}

1. Y ajouter un fichier `__init__.py`.
{{ pmt_note("Ceci fait que le dossier `py_libs`{.pmt-note} devient un package python.") }}


1. Ajouter au dossier autant de fichiers python et/ou sous-dossiers que voulu.
{{ pmt_note("Chaque sous-dossiers doit contenir son propre fichier `__init__.py`{.pmt-note}.") }}

1. Dans les fichiers python des exercices, importer le code comme n'importe quel autre module/bibliothèque python : `from py_libs import ...`.
{{ pmt_note("Le thème identifie s'il s'agit d'une bibliothèque personnalisée, et utilise alors la méthode d'installation appropriée.") }}


Exemple :

=== "Organisation du projet"

    ```
    projet
    ├── docs
    │   ├── POO
    │   │   ├── exercice1
    │   │   │   ├── index.md
    │   │   │   ├── exo.py
    │   │   │   └── exo_REM.md
    │   ... ...
    ├── py_libs
    │   ├── __init__.py
    │   ├── structures_lineaires.py
    │   └── arbres.py
    ...
    ```

=== "Fichier `arbres.py`"

    ```python
    # Depuis l'intérieur même d'un package, les imports peuvent relatifs...
    from .structures_lineaires import Pile

    # ...ou en spécifiant le chemin complet (import absolu):
    from py_libs.structures_lineaires import File

    class BSTree:
        ...
    ```

=== "Fichier `exo.py`"

    ```python
    # --- PYODIDE:env --- #

    # Depuis l'extérieur d'un package les imports doivent être faits
    # avec les chemins complets (absolus) :
    from py_libs.structures_lineaires import Pile, File
    from py_libs.arbres import BSTree as ABR

    ...
    ```


### Avantages & inconvénients

* Placer les packages à la racine du projet permettent d'exécuter les fichiers pythons de la documentation en local, si nécessaire, sans modifier le code par rapport à la version en ligne ou celle disponible en utilisant `mkdocs serve`.

* Les imports peuvent être réalisés depuis n'importe quelle [section](--ide-files) d'un exercice, donc aussi depuis une des sections de tests ou par l'utilisateur lui-même. Ceci peut en partie être mitigé en modifiant les `python_libs` grâce aux fichiers {{meta()}} ou aux métadonnées dans les entêtes de pages ([voir ici](--custom/metadata/)).


Il y a un revers conséquent à cette médaille.

* ___Les imports marchent depuis n'importe où sur le site___.
<br>Sauf si des restrictions ont été mises en place, un utilisateur ayant connaissance de l'existence d'un package fournissant une classe Pile peut très bien l'importer depuis un autre exercice où il est sensé implanter lui-même la classe, si aucune [restriction d'imports](--IDE-SANS) n'est mise en place.
<br>Concernant la mise en place de restrictions de bibliothèques, préférer la [configuration des `python_libs` via les métadonnées](--libs-vs-meta), plutôt que l'argument [`SANS`](--IDE-SANS) des macros.

* ___Le contenu de ces fichiers N'EST PAS caché___.
<br>Une fois une bibliothèque installée, les fichiers sont disponibles à la racine du système de fichier de Pyodide. Un utilisateur ayant la présence d'esprit de fouiller le disque virtuel avec `#!py os.listdir()` pourra découvrir l'intégralité du contenu de l'environnement, afficher leur contenu, ...


!!! warning "Responsabiliser les utilisateurs vis-à-vis des restrictions..."

    Plutôt que de tenter (en vain) de tout verrouiller, il vaut sans doute mieux responsabiliser les utilisateurs, afin que le runtime ne devienne pas complètement imbuvable pour avec des restrictions dans tous les sens.<br>Encore une fois, le thème est conçu dans l'optique de créer des sites d'entraînement, pas des sites d'évaluation.



### Configuration

Il est possible de définir plusieurs dossiers de bibliothèques personnalisées, via l'option {{ config_link('build.python_libs', 1) }} du plugin :

```yaml
plugins:
    - pyodide_macros:
        build:
            {{ config_validator("build.python_libs",1)}}:
                - lib1
                - libxyz
                - ...
```

Chaque dossier constitue une bibliothèque différente, qui devrait être indépendante des autres.

!!! warning "Contraintes"

    1. Seuls des dossiers sont acceptés, pour les items de l'option `python_libs`.
    1. Tous les dossiers doivent être placés à la racine du projet.
    1. Importer un unique élément d'une bibliothèque installe tout le contenu de cette bibliothèque.

!!! tip "Conseil"

    1. Utiliser différents dossiers permet de ne pas charger tous les fichiers pour un seul exercice.
    1. Préférer une organisation plutôt "horizontale" des packages, avec peu de profondeur, sans quoi il est très vite pénible d'écrire les importations...



## Sécurité {{anchor_redirect(id="securite")}} { #security }

Une attaque particulièrement classique de hackers malveillants est d'uploader des packages sur PyPI avec des fautes de frappes dans les noms, en attendant ensuite qu'un malheureux fasse une requête avec la-dite faute de frappe. Par exemple, `request` au lieu de `requests` (nota: exemple pris au hasard, rien de factuel !).

!!! danger "Installations de paquets automatiques depuis Pyodide"

    L'environnement pyodide réalise les installations automatiquement, ___sans demander de confirmation à l'utilisateur___.

L'environnement de Pyodide est donc sujet à ce type de problèmes et il serait bon que les utilisateurs soient conscients du fait.

Il faut cependant relativiser la sévérité de la "faille" :

- L'environnement n'est à priori pas conventionnel, et donc pas une cible privilégiée pour ce type d'attaque.
- L'environnement est sensé être isolé de la machine...
- ... mais gardez en tête qu'il est possible d'accéder au DOM et donc au navigateur depuis la couche python. Donc il est à priori possible de récupérer certaines informations de ce côté (assertion non vérifiée de ma part !).





## Aller plus loins

### Bibliothèques personnalisée et méta { #libs-vs-meta }

Les fichiers {{meta()}} ainsi que les entêtes de fichiers markdown peuvent être mis à profit pour empêcher l'utilisateur d'utiliser certaines bibliothèques personnalisées sans requérir à l'argument [`SANS`](--IDE-SANS) des différentes macros.

* {{orange("La localisation des bibliothèques est toujours renseignée par rapport au dossier racine du projet")}}, quand les `python_libs` sont modifiées dans un fichier {{meta()}}. La localisation des fichiers {{meta()}} n'a donc aucun impact.

* Il est impératif de {{red("déclarer _toutes_ les bibliothèques personnalisées dans le fichier `mkdocs.yml`{.red}")}}, et ensuite de redéclarer l'option `python_libs` là où approprié dans les fichiers {{meta()}} ou dans les entêtes de pages markdown, pour limiter les bibliothèques effectivement accessibles.
<br>Ceci présente deux intérêts :

    1. Cela permet d'éviter des imports accidentels de paquets avec des noms similaires sur PyPI :

        Le thème garde une trace de l'intégralité des noms des bibliothèques personnalisées, et si un utilisateur tente d'importer une bibliothèque personnalisée non disponible pour la page en cours, le thème la reconnaîtra et évitera toute requête vers PyPI pour tenter d'installer un paquet automatiquement. (1)
        { .annotate }

        1. Cela peut paraître anodin, mais gardez en tête que la plupart des noms de packages classiques existent déjà (`py-libs`, `lib`, `lib2`, ...) et l'installation automatique de pyodide installera n'importe quel paquet correspondant à un import demandé par l'utilisateur.

            En l'occurrence, si `py_libs` n'est pas disponible en temps que bibliothèque personnalisée et que l'utilisateur écrit `import py_libs` dans son code, `micropip` ___va___ installer `py-libs` à la place, qui est une bibliothèques existant sur PyPI, mais qui n'a rien à voir avec le thème...

    2. Cela permet également de valider les `python_libs` déclarées via les méta-données :

        Toutes les `python_libs` modifiées via les méta données qui n'apparaissent pas dans la déclaration du fichier `mkdocs.yml` lèveront une erreur.



<br>

!!! help "Exemple"

    Voici un exemple concret de modification des `python_libs` via des fichiers {{meta()}}.

    {{md_include("docs_tools/inclusions/meta_python_libs.md")}}




### Rangement des bibliothèques

Il a été dit plus haut que tous les packages doivent être à la racine du projet.

Cependant, si vous utilisez beaucoup de dossiers différents, il peut devenir difficile de s'y retrouver...

Le thème supporte en fait la définition des bibliothèques dans des sous-dossiers, mais cela va avec certaines contraintes dont il faut être conscient.

<br>

En prenant l'exemple d'un item `other_libs/that_lib` ajouté aux `python_libs` :

1. Seul le dossier `that_lib` sera exporté "vers pyodide". Cela signifie que le code python utilisera des imports de la forme:<br>`#!py from that_lib import ...`.
1. Cella implique également qu'il n'est plus possible d'exécuter les fichiers python de la documentation utilisant cette bibliothèque en local : le code nécessaire pour importer leur contenu dans le contexte dans la documentation construite (le site web) ne correspond plus au contexte des exécutions en local.
1. Pour cette raison, et afin d'éviter d'avoir un code qui fonctionne en local mais qui ne marche plus sur le site construit, le thème lève une erreur durant le build, si une de ces bibliothèques est importable depuis le répertoire de travail du projet.
<br>Concrètement, cela signifie que le dossier `other_libs` ne doit __SURTOUT PAS__ contenir de fichier `__init__.py`.
1. Par ailleurs, une bibliothèque ne peut pas en contenir une autre : une erreur est aussi levée si les `python_libs` contiennent par exemple les chemins `other_libs/lib` et `other_libs/lib/sub_lib`.

<br>

!!! danger "À éviter"
    Cette façon de procéder est déconseillée, pour les raisons évoquées ci-dessus.
    <br>Cependant, si le nombre de dossiers de bibliothèques personnalisées commence à devenir trop important pour pouvoir gérer le projet facilement, cela peut rester une solution...
