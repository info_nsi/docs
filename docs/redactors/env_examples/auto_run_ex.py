# --- PYODIDE:env --- #
def show(section):
    print(f'Dans { section } :')
    for s in ('passing', 'failing'):
        print(s, globals().get(s, 'non définie'))

# --- PYODIDE:code --- #
show('code')

def user_func():
    raise KeyError()

# --- PYODIDE:secrets --- #
@auto_run
def passing():
    pass            # Simule succès

@auto_run
def failing():
    user_func()     # Lève une erreur

# --- PYODIDE:post --- #
show('post')
