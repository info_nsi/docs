# --------- PYODIDE:env --------- #
print('...env! (once only)')

# --------- PYODIDE:env_term --------- #

print('In env_term, with __USER_CMD__ =', __USER_CMD__)

def inf_rec():
    def rec(n):
        if n<50: rec(n+1)
    rec(0)

# --------- PYODIDE:post_term --------- #
print('In post_term...')
print("Try inf_rec(), sorted()")

# --------- PYODIDE:post --------- #
print('...post! (once only)')