# --- HDR --- #

# Cette section est supprimée...

# --- HDR --- #

def mod3(n):
    """ do something appropriate... """
    ...

# Tests

assert mod3(4) == 1, 'mod(4)'
assert mod3(3) == 0, 'mod(3)'
assert mod3(2) == 2, 'mod(2)'
