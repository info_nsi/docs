
# --- PYODIDE:code --- #

def mod3(n):
    """ do something appropriate... """
    ...

# --- PYODIDE:corr --- #

def mod3(n):
    """ Correction """
    return not n%3

# --- PYODIDE:tests --- #

assert mod3(4) == 1, 'mod(4)'
assert mod3(3) == 0, 'mod(3)'
assert mod3(2) == 2, 'mod(2)'

# --- PYODIDE:secrets --- #

assert mod3(3) == 0, 'mod(3)'