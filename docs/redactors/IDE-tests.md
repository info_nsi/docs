# Écrire des tests {{anchor_redirect(id="ecrire-des-tests")}} { #tests-writing-tests }


La façon de rédiger les tests peut _drastiquement_ changer le confort de l'utilisateur, lorsqu'il essaie de résoudre un problème.

En particulier, si  la sortie standard est désactivée durant les tests secrets, il est critique que les assertions donnent bien le niveau d'information souhaité par le rédacteur, sans quoi l'utilisateur peut se retrouver sans aucune information utile, et donc très peu de chances de trouver comment modifier son code.



## Stratégies {{anchor_redirect(id="strategies")}} { #tests-strategies }

De manière générale, les "stratégies" suivantes sont conseillées :

1. Mettre tous les cas particuliers dans les tests publics.

1. Mettre suffisamment de tests publics (section `tests`) pour couvrir au moins une fois tous les types de cas présents dans les tests de la section `secrets`.

1. {{red("Les assertions sans message d'erreur sont à proscrire dans les tests secrets.")}}
<br>Si vous comptez utiliser la fonctionnalité génération automatique des messages d'erreurs des macros `IDE` (voir [l'argument `LOGS`](--IDE-LOGS) et sa configuration globale), il faut ___à minima___ que les valeurs des arguments soient définies dans le code de chaque assertion.

1. On peut adapter le niveau de feedback des messages d'erreur selon le but recherché et le contexte de l'exercice.<br>Typiquement :

    - Un message donnant uniquement le ou les arguments peut être considéré comme le strict minimum acceptable.

        ```python
        assert func(arg) == attendu, f"func({arg})"
        ```

    - Un message donnant en plus la réponse de l'utilisateur est un confort ajouté certain. Cela ne change rien au niveau d'information accessible à l'utilisateur, mais cela lui évite d'avoir à créer lui-même le test public correspondant pour savoir ce que sa fonction a renvoyé.

        ```python
        val = func(arg)
        assert val == attendu, f"func({arg}): a renvoyé {val}"
        ```

    - Un message présentant en plus la réponse attendue est la "Rolls" du message d'assertion (mais n'est pas toujours souhaitable pour des tests "secrets").

        ```python
        val = func(arg)
        assert val == attendu, f"func({arg}): {val} devrait être {attendu}"
        ```

    ??? tip "Ne pas appeler plusieurs fois la fonction de l'utilisateur pour un même test"

        Il est _vivement_ déconseillé d'appeler plusieurs fois la fonction de l'utilisateur pour le même test. Cela peut rendre le code plus difficile à déboguer.

        === "{{ red('Pas bien') }}"
            ```python
            assert func(arg) == attendu, f"func({arg}): {func(arg)} devrait être {attendu}"
            ```

        === "{{ green('Bien') }}"
            ```python
            val = func(arg)
            assert val == attendu, f"func({arg}): {val} devrait être {attendu}"
            ```

1. Si des tests aléatoires sont implantés, il est _INDISPENSABLE_ de leur associer un niveau de feedback élevé (voir ci-dessus) :

    Gardez en tête que des tests aléatoires _vont_ générer des cas particuliers pour les fonctions alambiquées de vos utilisateurs, que vous n'avez aucune chance de prévoir dans les tests publics. Les insuffisances du feedback peuvent alors transformer l'exercice que vous aurez passé des heures à peaufiner en véritable cauchemar pour vos élèves, ce qui serait tout de même dommage...



## Aspects techniques {{anchor_redirect(id="aspects-techniques")}} { #tests-technique }

_^^Problématique :^^_

Gardez en mémoire que l'environnement Pyodide est commun à toute la page, ou jusqu'à un éventuel rechargement de celle-ci. Cela implique que de nombreux effets de bords peuvent se présenter, dont les utilisateurs n'auront pas conscience.

En particulier toute variable ou fonction définie dans les tests est visible depuis la fonction de l'utilisateur. Ceci signifie que l'environnement de la seconde exécution n'est souvent pas le même que celui de la première exécution des tests.

Concrètement, cela peut amener à des erreurs très dures à tracer côté utilisateur, car les comportements ne sont pas toujours reproductibles. On a par exemple vu des codes lever ou pas `NameError`, selon l'ordre d'utilisation des boutons de l'IDE après un chargement de page...

<br>

_^^Solution :^^_

* Il est vivement conseillé d'écrire les tests dans une fonction afin que leur contenu ne puisse pas "leaker" dans l'environnement de l'utilisateur.
* Ne pas oublier d'appeler la fonction des tests après l'avoir écrite... :wink:
* Comme les fonctions restent définies dans l'environnement global, après avoir lancé une première validation, un utilisateur malin pourrait exécuter des fonctions définies dans la section `secrets` {{ annotate("Le nom des fonctions est visible dans les stacktraces des erreurs, ou encore, accessible via `dir()` dans le terminal.") }} en ajoutant les appels aux tests publics dans l'éditeur de l'IDE. Ce qui permettrait de contourner le compteur d'essais. <br>Dans le contexte du thème, cela n'est sans doute pas un problème (il n'y a rien à gagner, après tout... !), mais si vous voulez l'empêcher, il vous faudra supprimer la fonction de l'environnement en utilisant `del` comme montré  dans le second exemple ci-dessous.

 [Le décorateur `@auto_run`](--tests-auto_run) proposé par le thème facilite tout ceci.

<br>



=== "Exemple simple"

    ```python title="De bons tests secrets..."
    ---8<--- "docs_tools/inclusions/is_even_tests_example.py"
    ```

=== "Exemple complexe (effacer les traces)"

    !!! tip "Voir l'outil `@auto_run`, plus bas dans la page"

        Le thème met à disposition un décorateur faisant exactement tout cela, et gérant également le nettoyage régulier de l'environnement.

    Effacer les fonctions de tests peut devenir assez fastidieux, notamment de par le fait que si une erreur est levée, le code suivant la fonction de test n'est pas exécuté, sauf à utiliser des blocs `try/except` ou assimilés...

    Il existe une solution très rapide qui permet :

    1. D'être certain de ne pas oublier d'appeler les fonctions de tests.
    1. D'être certain que la fonction de tests ne sera pas disponible du côté de l'utilisateur, et ce quelle que soit le déroulement des tests (succès/erreur).
    1. Ne fournit aucun élément exploitable à l'utilisateur pour investiguer le contenu des tests.

    === "La méthode..."

        {{ section("env_examples/decorate", "code")}}

        <br>

        ![résultat](!!tests_decorator_png){: loading=lazy .w35 align=right }

        Après les exécutions :

        - `test` est définie et accessible à l'utilisateur, mais n'a strictement aucun intérêt pour lui.
        - `passing` est également définie, mais est en fait `None` au lieu d'être la fonction `passing`.
        - `failing` n'est pas définie car la fonction a levé une erreur avant que le décorateur n'affecte la variable dans le scope global.

    === "Pourquoi cela marche ?"

        `test` repose sur la syntaxe des décorateurs, mais n'en est en fait pas vraiment un : `test` appelle directement la fonction `f` passée en argument, au lieu de renvoyer une fonction comme un décorateur normal.

        {{green("Ceci fait que toute fonction décorée avec `test`{.green} est une fonction de test exécutée.")}}

        <br>

        Par ailleurs, la variable du scope global correspondant au nom d'une fonction décorée se voit en fait associée ce que l'appel au décorateur renvoie. Ici, `test` renvoie le résultat de `f()`, qui est la fonction décorée, mais les fonctions de tests ne renvoient rien, et `test` renvoie donc `None`, si aucune erreur n'est levée.

        {{green("Une fonction décorée avec `test`{.green} n'est donc en fait jamais assignée à la variable correspondant dans le scope globale, et ne sera donc jamais accessible à l'utilisateur.")}}

    <br>

    {{ IDE('env_examples/decorate', ID=1) }}


<br>

Vous pouvez appliquer la même stratégie aux tests publics, mais cela surcharge notablement le code et n'est donc probablement pas souhaitable.<br>
D'autant plus que pour ces tests, si les assertions sont écrites avec toutes les données "en dur", la construction automatique des messages d'erreur donne presque tout le feedback nécessaire à l'utilisateur, tout en faisant gagner du temps au rédacteur (le seul élément non affiché étant alors la valeur renvoyée par la fonction de l'utilisateur).

```python
# Suffisant pour des tests publics, mais pas forcément pour des tests secrets !
assert est_pair(3) is False
assert est_pair(24) is True
...
```







## {{anchor_redirect(id="outil-auto-run")}}Outil `auto_run` { #tests-auto_run }


À partir de la version 2.0, le thème propose un décorateur qui __permet d'exécuter automatiquement la fonction qu'il décore, et empêche de la réutiliser par la suite__.

Ceci est particulièrement utile pour écrire des fonctions devant exécuter certaines tâches en gardant leur contenu isolé de l'environnement global, comme des fonctions de tests pour les sections `secrets` des IDEs, par exemple.

<br>

=== "Avec décorateur"

    ```python
    @auto_run
    def test():
        """ tests dans un scope isolé """
        assert func() == 42
    ```

    * Le code executant la fonction (le décorateur) est écrit au plus proche de la déclaration de fonction, ce qui évite d'oublier l'appel de fonction dans le code.
    * La façon de fonctionner du décorateur fait que la fonction décorée n'est en fait jamais affectée dans l'environnement, et l'utilisateur ne pourra pas y accéder lors d'une exécution ultérieure.

=== "Comportement équivalent sans le décorateur"

    ```python
    def test():
        """ tests dans un scope isolé """
        assert func() == 42

    try:
        test()
    finally:
        test=None
    ```

    * Obtenir le même comportement sans le décorateur est très laborieux, car il faut garantir l'effacement de la fonction, si `test()` lève une erreur.
    * Si le code de la fonction est long, on ne sait voit pas de suite si la fonction est bien appelée ou pas.

<br>

___Spécifications :___

* Le décorateur exécute automatiquement la fonction décorée.
* {{red("__Le décorateur renvoie `None`{.red}__")}}, donc la fonction d'origine n'est jamais disponible dans le scope après exécution.
* Le décorateur est redéfini à chaque exécution lancée par l'utilisateur, de manière à fiabiliser son utilisation.
* Une fonction `func` décorée va laisser une variable `func=None` dans l'environnement. Le thème supprime automatiquement ces variables, à différents moments des exécutions :

    - juste {{orange("_avant_")}} d'exécuter le code ou la commande de l'utilisateur,
    - à la toute fin d'une exécution, {{orange("_après_")}} `post_term` et `post`.

* Il est possible de déclencher manuellement le nettoyage de l'environnement en appelant la méthode `auto_run.clean()`, si besoin.

<br>

!!! warning "Il est déconseillé d'utiliser le décorateur dans les tests publics"

    L'utilisateur n'a pas besoin de connaître l'existence du décorateur : il pourrait sinon interagir avec lui et potentiellement changer le comportement des exécutions, s'il sait comment faire.

<br>
<br>

Voici in mise en évidence du comportement des fonctions décorées, et des variables présentent dans l'environnement après exécutions :

{{ IDE_py(
    'env_examples/auto_run_ex', ID=1, TERM_H=23, MODE='no_reveal',
    before='!!!', admo_kls='tip inline end w50'
) }}





## Tester la correction et les tests {{anchor_redirect(id="test-corr")}} { #tests-test-corr }

![Tester la correction](!!test_corr_png){: loading=lazy .w40 align=right }

{{orange("Durant le développement en local")}}, via `mkdocs serve`, {{green("__les IDEs disposant d'un contenu pour la")}} [section `corr`](--ide-sections)__ se voient automatiquement ajoutés un nouveau bouton qui permet d'exécuter une validation, mais avec le contenu de la correction en lieu et place du contenu de l'éditeur de code. Cela permet de :

- Tester le code de la correction, dans le même contexte que le code de l'utilisateur.
- Tester aussi le bon fonctionnement des tests publics et secrets, puisque les tests des sections `tests` et `secrets` sont tous deux exécutés lors des validations (voir les informations sur le [déroulement des exécutions](--runtime)).

Ce bouton n'est jamais présent dans le site construit avec la commande `mkdocs build`.





![Tester la correction](!!show_corr_REMs_png){: loading=lazy .w40 .margin-top-h2 align=right }

## Voir les contenus `corr` & `REM`s {{anchor_redirect(id="corr-REM-reveal")}} { #tests-corr-REM-reveal }

De manière similaire, et toujours {{orange("durant le développement en local")}}, via `mkdocs serve`, {{green("__les IDEs disposant d'un contenu `corr`{.green} ou de fichiers `{exo}_REM.md`{.green} ou `{exo}_VIS_REM.md`{.green}__")}} se voient automatiquement ajoutés un autre bouton permettant de révéler le contenu caché sans exécuter les tests.

Ce bouton n'est jamais présent dans le site construit avec la commande `mkdocs build`.
