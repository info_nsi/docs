

Les boutons sont en gros des "IDEs sans éditeur et sans terminal". Ou des terminaux sans terminal...

Ils permettent de lancer un code python arbitraire dans la page (en général, pour utiliser le résultat et modifier le contenu de la page depuis le code python).


## Exemples

{{ md_include("docs_tools/inclusions/py_btns_examples.md") }}



## Signature

{{ macro_signature('py_btn') }}


## Arguments

{{ macro_args_table('py_btn', with_headers=True, with_text=True) }}


- Voir la [page des IDEs](--redactors/IDE-details/) concernant l'argument `py_name` et la structure des fichiers associés.

- Seule les sections `env` et `ignore` de ces fichiers peuvent être utilisées avec la macro `py_btn`. Il est rappelé que la section `env` est exécutée en mode [`async`](--async), ce qui permet plus de souplesse.

- Si une autre section ou des fichiers `{exo}_REM.md` ou `{exo}_VIS_REM.md` sont trouvés, une erreur est levée.


Un bouton ira en général de paire avec une fonctionnalité pour modifier quelque chose dans la page. Cette modification peut être faite via le code python, en utilisant les objets proxy proposés par [Pyodide][Pyodide]{: target=_blank }, qui permettent d'interagir avec la couche JS/html de la page. Voir par exemple les utilisations avec [`matplotlib`](--custom/matplotlib/) ou [`mermaid`](--custom/mermaid/).