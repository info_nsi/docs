La macro `{% raw %}{{ figure() }}{% endraw %}` permet d'insérer une balise `div` en spécifiant son identifiant html, pour ensuite y afficher :

* Des [figures dessinées avec `matplotlib`](--custom/matplotlib/)).
* Des graphes mermaid.
* ... Ou toute autre chose qui aurait besoin d'être inséré dans une balise de la page durant les exécutions de codes python.


## Signature

{{ macro_signature('figure') }}

<br>

??? tip "L'astérisque seule dans `figure(..., *, ...)` ?"

    Cette astérisque fait qu'il n'est pas possible d'utiliser les arguments autres que `div_id` sans renseigner le nom de l'argument en plus de la valeur. Tous les arguments suivant sont donc _arguments nommés_.

    {{ gray("_Nota: `div_id` est un argument positionnel avec valeur par défaut, et non un argument nommé._") }}

<br>

Exemples :

{{ figure_md(
    ('Défaut',          ''),
    ('Personnalisée',   '\n"figure3", \ndiv_class = "py_mk_figure orange", \nadmo_kind = "???", \nadmo_title = "Repliée...", \ninner_text = "_Here!<br>(Note: the `orange`{.orange} class is not defined in the theme)_", \n'),
    ('Sans admonition', '\n"figure2", \ndiv_class="py_mk_figure pmt-border", \nadmo_kind=""\n'),
)}}


## Arguments

{{ macro_args_table('figure', with_headers=True, width2=11) }}





## Personnalisations


### Formatage css de la `div`

La classe html `.py_mk_figure` est définie par le thème et gère le formatage par défaut des balises `div` qui accueillent les figures.

Il est possible de surcharger cette règle dans un fichier `.css` ajouté dans `docs` et référencé dans le fichier `mkdocs.yml`, dans la section `extra_css`.

Il est également possible de changer la classe, ou d'en ajouter d'autres via l'argument `div_class`.


### Plusieurs figures dans la page

Si la même page de documentation doit intégrer plusieurs figures, il est indispensable de renseigner l'argument `div_id` de la macro `figure`, en renseignant à chaque fois un nouvel identifiant. Sans cela, tous les graphiques seront dessinés dans la première `div`.
