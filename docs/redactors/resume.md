# Généralités concernant le runtime et l'environnement


Cette page donne des informations sur les macros disponibles avec le thème, leurs réglages et le fonctionnement de l'environnement lui-même.





## Résumé des macros disponibles



=== "`IDE`"

    Création d'un IDE comportant :

    --8<-- "docs_tools/inclusions/IDE_shorts.md"

    {{ sep() }}

    #### Syntaxe :

    ```markdown
    {% raw %}{{ IDE('exo_mod3') }}{% endraw %}
    ```

    #### Résultat :

    {{IDE("env_examples/exo_mod3", ID=1, TERM_H=3)}}

    <br>

    !!! tip "Arguments"

        - Voir [la page dédiée aux IDEs](--redactors/IDE-details/) pour le détail des options ou leurs valeurs par défaut.
        {{ macro_args_table('IDE', with_headers=True, with_text=True) }}






=== "`IDEv`"

    Création d'un IDE sur deux colonnes avec :

    --8<-- "docs_tools/inclusions/IDE_shorts.md"

    {{ sep() }}

    #### Syntaxe :

    ```markdown
    {% raw %}{{ IDEv(MAX_SIZE=6) }}{% endraw %}
    ```

    #### Résultat :

    {{IDEv(ID=2, MIN_SIZE=10)}}

    <br>

    !!! warning "`IDEv` et panneaux coulissants..."

        Comme vous l'aurez _peut-être_ constaté avec l'`IDEv` ci-dessus, il arrive parfois que le terminal reste vide quand on change d'onglet après chargement de la page (idem avec les admonitions initialement repliées).

        On peut alors retrouver un état normal du terminal, soit en cliquant dessus, soit en exécutant les codes python associés à l'IDE.

    <br>

    !!! tip "Arguments"

        - Voir [la page dédiée aux IDEs](--redactors/IDE-details/) pour le détail des options ou leurs valeurs par défaut.
        {{ macro_args_table('IDE', with_headers=True, with_text=True) }}






=== "`terminal`"

    Création d'un terminal isolé, proposant :

    - Auto-complétion avec ++tab++
    - Rappel de l'historique avec ++ctrl+"R"++

    Voir la [page dédiée](--redactors/terminaux/) aux terminaux pour des explications détaillées.

    {{ sep() }}

    #### Syntaxe :

    ```markdown
    {% raw %}{{ terminal('exo', ...) }}{% endraw %}
    ```

    #### Résultat :

    {{ terminal(SIZE=8) }}

    <br>

    !!! tip "Arguments"

        - Voir [la page dédiée aux terminaux](--redactors/terminaux/) pour le détail des options ou leurs valeurs par défaut.
        {{ macro_args_table('terminal', with_headers=True, with_text=True) }}

    <br>

    ??? note "L'ancien argument `SIZE`"

        Une version antérieure du thème proposait un argument `SIZE`, qui est maintenant remplacé par `TERM_H`.<br>
        L'ancien argument est cependant toujours supporté, et il n'est pas nécessaire de mettre à jour les appels de macros.






=== "`py_btn`"

    <br>
    ```markdown
    {% raw %}{{ py_btn('exo', TIP="Lancer pour faire ci ou ça" ...) }}{% endraw %}
    ```

    <br>
    Les boutons sont en gros des "IDEs sans éditeur et sans terminal". Ou des terminaux sans terminal...

    Ils permettent de lancer un code python arbitraire dans la page (en général, pour utiliser le résultat et modifier le contenu de la page depuis le code python).

    Voir la [page dédiée](--redactors/py_btns/) aux boutons pour des explications détaillées.

    {{ sep() }}

    #### Exemples


    {{ md_include("docs_tools/inclusions/py_btns_examples.md") }}

    <br>

    !!! tip "Arguments"
        {{ macro_args_table('py_btn', with_headers=True, with_text=True) }}






=== "`section`"

    Insère dans le document en cours la section indiquée de l'exercice, dans un bloc de code.<br>
    Utilisable quel que soit le [format des fichiers utilisé](--ide-files).


    {{ sep() }}

    #### Syntaxe :

    ```markdown
    {% raw %}{{ section('exo_mod3', 'corr') }}{% endraw %}
    ```

    #### Résultat :

    {{ section_py(
        'env_examples/exo_mod3', 'corr',
         after='!!!', admo_kls='help', add_empty_line=1
    ) }}


    <br>

    !!! tip "Arguments"
        {{ macro_args_table('section', with_headers=True, with_meta_info=False) }}






=== "`py`"

    Permet l'insertion du code d'un fichier python dans la page.

    Si elle est utilisée avec un fichier python respectant le format des anciens fichiers sources utilisés par [pyodide-mkdocs](--version-pyodide-mkdocs-3-fichiers-py), seul l'équivalent de la section `code` sera inséré.

    {{ sep() }}


    === "Fichier .py quelconque"

        Intègre tout le contenu du fichier à la page :

        #### Syntaxe :

        ```markdown
        {% raw %}{{ py('exo_term') }}{% endraw %}
        ```

        #### Résultat :

        {{ py_py('env_examples/exo_term', after='!!!', admo_kls='help', add_empty_line=1) }}


    === "Vieux fichier .py"

        Intègre uniquement l'équivalent de la section `code` des fichiers python utilisés par la thème :

        #### Syntaxe :

        ```markdown
        {% raw %}{{ py('exo_mod3_old') }}{% endraw %}
        ```

        #### Résultat :

        {{ py('env_examples/exo_mod3_old') }}

        <br>

        !!! note "Contenu du fichier `exo_mod3_old.py`"

            ```python
            ---8<--- "docs/redactors/env_examples/exo_mod3_old.py"
            ```

    <br>

    !!! tip "Arguments"
        {{ macro_args_table('py', with_headers=True, with_meta_info=False) }}






=== "`multi_qcm`"

    Permet d'insérer un ensemble de questions à choix multiples (ou uniques), avec différentes fonctionnalités disponibles :

    - Correction automatique.
    - Option pour ne pas afficher les réponses correctes lors de la correction.
    - Option pour mélanger automatique des questions et de leurs items.
    - ...

    Voir la [page dédiée](--redactors/qcms/) pour des explications détaillées.

    {{ sep() }}

    #### Syntaxe :

    ```markdown
    {% raw %}{{ multi_qcm(...) }}{% endraw %}
    ```

    #### Résultat :

    === "Exemple fonctionnel"

        {{ md_include('docs_tools/inclusions/qcm_resume_ex.md') }}

    === "La déclaration exacte"

        ```markdown
        --8<-- "docs_tools/inclusions/qcm_resume_ex.md"
        ```

    <br>

    !!! tip "Arguments"

        {{ macro_args_table('multi_qcm', with_headers=True, width1=13) }}

        <br>

        Détail de la structure d'un élément (= une question) de l'argument `*inputs` (plus de détails [sur cette page](--qcm_question).) :

        ```python
        multi_qcm(
            [
                "intitulé de question (du md, possiblement multilignes)",
                ["choix 1", "choix2", ...],
                [1,3],       # numéro des réponses correctes
                # Un dictionnaire optionnel en 4e argument. Couples clef-valeur possible:
                #   {'multi': bool}
            ],
            ...,
        )
        ```





=== "`figure`"

    La macro `figure` permet d'insérer une div avec l'identifiant html indiqué, potentiellement à l'intérieur d'une admonitions, pour pouvoir ensuite y afficher des graphiques dessinés durant les exécutions (ex: une [figure dessinée avec `matplotlib`](--custom/matplotlib/)).

    {{ sep() }}

    #### Syntaxe générale :

    ```markdown
    {% raw %}{{ figure('id', ...) }}{% endraw %}
    ```

    #### Résultats :

    {{ figure_md(
        ('Défaut',          ''),
        ('Sans admonition', '"figure2", admo_kind=""'),
        ('Personnalisée',   '\n"figure3", \ndiv_class = "py_mk_figure orange", \nadmo_kind = "???", \nadmo_title = "Repliée...", \ninner_text = "_Here!<br>(Note: the `orange`{.orange} class is not defined in the theme)_",\n'),
    )}}

    <br>

    ??? note "Autre utilisation possible: `turtle` dans pyodide"

        Il est également possible d'utiliser cette macro pour obtenir un élément dans lequel tracer ensuite des dessin avec l'émulateur de `turtle` pour Pyodide, mis en place par Romain Janvier.

        Un tutoriel de son utilisation avec des exemples est disponible ici : [`turtle` dans pyodide][tuto-turtle]{: target=_blank } (développement: Romain Janvier / tutoriel : Mireille Coilhac)

    <br>

    !!! tip "Arguments"

        {{ macro_args_table('figure', with_headers=True, width2=11) }}










## Étendue de l'environnement pyodide

L'environnement pyodide est commun à l'intégralité des éléments se trouvant dans une page du site.

Cela présente l'avantage de pouvoir séparer des exercices complexes dans plusieurs IDE ou terminaux, à l'image de ce qui peut se fait avec un notebook.

Il faut cependant garder en tête les points suivants :

* Si l'un des IDEs comporte des [restrictions](--IDE-SANS) (fonctions interdites, ...), tous les IDEs et terminaux de la page doivent contenir ces mêmes restrictions (les [réglages via les entêtes markdown](--meta-pmt-yml-files) peuvent être mis à profit pour cela).
* Si une tâche est répartie sur plusieurs IDE, ils doivent tous être complétés et exécutés dans le bon ordre pour que tout se passe bien (responsabilité de l'utilisateur).
* Il est possible de mettre à profit les sections `env` des IDE pour faire en sorte que chaque IDE dispose de tout le nécessaire pour fonctionner indépendamment des autres. Garder cependant en tête que dans ce cas, un utilisateur ne lançant pas les IDE dans l'ordre peut se retrouver à ne plus avoir besoin de coder ses propres fonctions si les premiers IDEs ne font pas eux aussi le ménage en supprimant ce qui n'est pas sensé être défini (La tâche devient rapidement fastidieuse pour le rédacteur, cependant...).






## Réglages/arguments globaux

{{ md_include("docs_tools/inclusions/meta_data.md") }}

Ceci peut grandement faciliter la déclaration des macros avec des réglages semi-globaux, pouvant être appliqués à toutes les macros d'un fichier, ou toutes celles d'un dossier ou d'une hiérarchie de sous-dossiers.

Plus d'informations sur [les configurations semi-globales ici](--custom/metadata/).
