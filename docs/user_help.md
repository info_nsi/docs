

![un IDE](!!simple_IDE_png){: loading=lazy .w35 .margin-top-h2 align=right }

## Les IDEs { #users-IDEs }

Voici quelques informations utiles pour utiliser les "IDE", c'est-à-dire des éléments regroupant :

- un éditeur de code, qui contient le code de l'utilisateur et les éventuels tests publics.
- un terminal,
- et les boutons associés.

Le nombre d'essais indiqué sous l'IDE est le nombre d'essais de validations possibles (voir ci-dessous) avant que la solution et d'éventuelles remarques ne soient révélées. Elles seront affichées sous l'IDE, dans une admonition à déplier pour pouvoir en voir le contenu.




### Boutons sous l'IDE

| Bouton | Raccourci {{width(12, center=1)}} | Effet |
|:-:|:-:|:-|
| {{btn('play')}} | ++ctrl+s++ | Exécute le contenu de l'éditeur, c'est-à-dire, le code de l'utilisateur et les tests dits "publics" qui sont en-dessous dans l'éditeur.<br>Ces tests sont modifiables.<br>{{green('La fonction `print`{.green} est active')}}. |
| {{btn('check')}} | ++ctrl+enter++ | Lance une validation du code, si elle est disponible pour cet exercice.<br>Les tests de validation exécutent :<br>{{ ul_li([
    "Le contenu de l'éditeur (comme le bouton précédent).",
    "La version d'origine des tests publics (ceux visibles au départ dans l'éditeur).",
    "Des tests secrets.",
], tag='ol') }}{{
    orange('La fonction `print`{.orange} est normalement désactivée durant les étapes 2 et 3')
}} (mais le créateur du site pourrait avoir changé ce réglage).<br>Une validation échouée diminue le compteur d'essais de 1. |
| {{btn('download')}} |  | Télécharge le contenu actuel de l'éditeur dans le répertoire de téléchargement de l'ordinateur. |
| {{btn('upload')}} |  | Remplace le contenu actuel de l'éditeur avec celui d'un fichier sélectionné sur le disque. |
| {{btn('restart')}} |  | Réinitialise le contenu de l'éditeur. __Le contenu enregistré pour cet IDE sera perdu.__ |
| {{btn('save')}} |  | Enregistre le contenu actuel de l'éditeur dans le navigateur.<br>Nota : une sauvegarde automatique est effectuée toutes les 30 frappes de caractères. |


### Les éditeurs

| Bouton | Raccourci {{width(12, center=1)}} | Effet |
|:-:|:-:|:-|
| `###` | ++ctrl+i++ | Active ou désactive les tests publics dans l'éditeur. |
| | ++ctrl+s++ | Exécute le contenu de l'éditeur. |
| | ++ctrl+enter++ | Lance une validation (voir tableau précédent). |
| | ++f1++ | Ouvre le panneau d'aide de l'éditeur ACE : permet d'accéder à des options supplémentaires.<br>Nota : toutes ne sont pas supportées. |
| | ++ctrl+space++ | Ouvre les suggestions d'autocompletion (l'outil est assez rudimentaire). |

À noter qu'un menu complet gérant les fonctionnalités des éditeurs est accessible via la touche ++f1++ (permet notamment d'activer l'auto-complétion sans avoir besoin de l'activer manuellement).



### Les terminaux { #user-terminal }

| Bouton | Raccourci {{width(12, center=1)}} | Effet |
|:-:|:-:|:-|
| {{stdout_svg(True)}} {{stdout_svg(False)}}  | | Les messages dans le terminal sont tronqués automatiquement, ou sont laissés intactes (respectivement).<br>Attention, ce dernier réglage peut occasionner des problèmes de performances de l'interface. |
| :material-format-text-wrapping-overflow:{ style="font-size:1.3em" } | | Si activé, le texte sélectionné dans un terminal est automatiquement réassemblé en une seule ligne lors d'un ++ctrl+c++ (voir ci-dessous). |
| | ++ctrl+i++ | Active ou désactive les tests publics dans l'éditeur. |
| | ++ctrl+s++ | Exécute le contenu de l'éditeur. |
| | ++ctrl+enter++ | Lance une validation (voir tableau en haut de page). |
| | ++tab++ | Autocompletion (comme dans un terminal classique). |
| | ++tab++ (2 fois) | Suggestions d'autocompletion (comme dans un terminal classique). |
| | ++ctrl+r++ | Recherche dans l'historique des commandes exécutées. |

??? tip "Copier le contenu d'un terminal avec :material-format-text-wrapping-overflow:"

    L'affichage du terminal ajoute des retours à la ligne automatiquement, de manière à ce que tout le texte soit toujours visible dans le terminal. Cependant, cela impacte également le texte copié.

    Comme la structure du DOM dans les terminaux ne permet pas de différencier des contenus "réellement multilignes" des contenus dans lesquels des retours à la ligne ont été insérés pour la présentation des données, le bouton :material-format-text-wrapping-overflow: a été ajouté, pour permettre à l'utilisateur de choisir s'il faut supprimer les retours à la ligne ou pas lors d'une copie.

    Cela permet notamment de récupérer du code valide pour mettre en place des tests via le feedback affiché par les terminaux :

    | :material-format-text-wrapping-overflow:{ style="font-size:1.3em; opacity:30%" } | :material-format-text-wrapping-overflow:{ style="font-size:1.3em; opacity:80%" } |
    |:-:|:-:|
    | ![Wrapped terminal](!!term-copy-wrapping1_png){: loading=lazy } | ![Unwrapped terminal](!!term-copy-wrapping2_png){: loading=lazy } |
    | ![Wrapped copy](!!term-copied-wrapping1_png){: loading=lazy } | ![Unwrapped copy](!!term-copied-wrapping2_png){: loading=lazy } |





### Dés-/Activation des tests publics

Pour que cette fonctionnalité puisse être utilisée, il faut que l'éditeur contienne un commentaire spécifique entre les tests publics et le code de l'utilisateur.

* Par défaut, il s'agit du commentaire `#!py # Tests`.
* Le créateur du site aurait pu le modifier : survoler le bouton `###` dans le coin supérieur droit de l'éditeur, pour voir quel est le commentaire à utiliser.

<br>

!!! tip "Écriture de commentaires dans les tests publics"

    Si vous ajoutez vos propres commentaires dans les tests publics, ne pas oublier de leur ajouter un espace après le `#`, sans quoi la fonctionnalité de dés-/activation des tests publics essaiera de transformer le commentaire en code.

    ```python title="Code désactivé vs commentaires, dans les éditeurs"
    # Tests

    # Ceci est un commentaire et le restera même après un Ctrl+I

    assert True     # Ctrl+I désactiverait cette ligne et activerait la suivante.
    #assert False   # Cette ligne n'est pas un commentaire, mais du code désactivé.
    ```


### Sauvegardes & transferts

Le contenu d'un éditeur est enregistré dans le navigateur :

* Automatiquement, toutes les 30 frappes de caractères.
* Lorsque le code est exécuté, que ce soit avec les tests publics ou les validations.
* En utilisant le bouton de sauvegarde sous l'IDE.

Ainsi, quand on revient sur une page, on peut retrouver le code sur lequel on travaillait précédemment, sous réserve qu'on utilise la même machine et le même navigateur.

!!! warning "Les sauvegardes sont spécifiques"

    Les contenus sauvegardés de cette façon sont spécifiques... :

    - ...à chaque IDE...
    - ...mais aussi à chaque combinaison `machine + navigateur` utilisée !

    Pour transférer des solutions d'un navigateur et/ou d'un poste à un autre, il faut :

    1. Télécharger le contenu de l'éditeur.
    1. Transférer le fichier sur le support ou la machine voulu.
    1. Téléverser le fichier dans l'IDE approprié ou coller le contenu du fichier dans l'éditeur voulu.






## Les QCMs

{{ md_include('docs_tools/inclusions/qcms.md') }}





## Divers

### Version du thème

Si l'information n'est pas visible dans le pied de page du document, il est normalement possible d'accéder au numéro de version du thème en tapant dans n'importe quel terminal :

```pycon
>>> version()
pyodide-mkdocs-theme v...
```


### Purger le navigateur

Il est possible de supprimer tous les contenus enregistrés (contenus des IDEs, mais aussi historique des terminaux) dans un navigateur donné, avec le bouton en forme de poubelle, à côté de la barre de recherche.

{{trash_svg()}}

* Une confirmation est demandée avant suppression. Le message indique notamment combien d'éditeurs ont des contenus enregistrés à ce moment là.
* Si la suppression est confirmée :
    - Les contenus enregistrés de tous les IDEs du site sont supprimés.
    - Les historiques de tous les terminaux sont également purgés.