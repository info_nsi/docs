Un éditeur dans lequel faire des essais quelconques...

`{% raw %}{{ IDE() }}{% endraw %}`

{{ IDE('exemple/await_sandbox', MIN_SIZE=8) }}

??? tip "Essais async"

    L'IDE de cette page est en fait créé avec un fichier python contenant une section `post` uniquement.

    Lorsque la section `post` est exécutée, si une fonction `#!py async def post_async()` a été définie dans l'éditeur, elle sera automatiquement appelée.

    __Ceci permet d'essayer du code `async` dans le bac à sable.__

    _Rappel : il est aussi possible d'exécuter du code async depuis le terminal._