!!! warning "Attention"

    L'essentiel des fonctionnalités de [pyodide-mkdocs][pyodide-mkdocs]{: target=_blank } restent présentes dans le thème.

    Cependant, certaines modifications ont dû être apportées au projet, qui font que le passage à la version "thème" nécessitera quelques changements dans votre documentation, aussi bien du côté de l'environnement (packages installés) que de la configuration ou encore du contenu.





## Évolutions des macros {: #macros_table }


Voici une vue d'ensemble des macros conservées, modifiées ou supprimées, en passant de [`pyodide-mkdocs`][pyodide-mkdocs]{: target=_blank } à [`pyodide-mkdocs-theme`][pmt-pypi]{: target=_blank } :

| Devenir | {{ insecable(3) }} Macros |
|-|-|
| Conservées mais modifiées {: .blue } | {{ul_li_code("IDE IDEv multi_qcm py terminal")}} |
| Ajoutées | {{ul_li_code("section py_btn")}} |
| Supprimées {: .red } | {{ul_li_code("qcm")}} |
| Conservées mais non maintenues {: .orange } | {{ul_li_code("cours exercice ext html_fig numworks python_carnet python_ide tit mult_col")}} |



<p id="ID-modifs"></p>

???+ help "À propos des macros <span class="blue">modifiées</span>"

    Les anciens arguments restent valides, mais la plupart de ces macros se sont vues ajoutées d'autres arguments.

    En particulier, la méthode d'identification des IDEs sur le site a été modifiée, et les macros `IDE` et `IDEv` on maintenant un argument `ID:Optional[int]`, qui permet au code du thème d'identifier sans ambiguïtés deux IDEs qui utiliseraient les mêmes fichiers python. C'est cet argument que vous risquez de devoir ajouter ici ou là dans la documentation existante, après être passés au thème.

    Exemples:

        * Page A: `IDE('py_sources/exo1', ID=1)`
        * Page A: `IDE('py_sources/exo1', ID=2)` (ailleurs dans la même page)
        * Page B: `IDE('py_sources/exo1', ID=3)`




???+ help "À propos des macros {{orange("conservées mais non maintenues")}}"

    __Ces macros sont vouées à être supprimées dans une version ultérieure du thème.__
    { .red }

    Si votre site utilise certaines d'entre elles :

    * Vous devriez en [récupérer le code](https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme/-/blob/main/pyodide_mkdocs_theme/pyodide_macros/macros/autres.py?ref_type=heads) actuellement présent dans le dépôt du thème, les mettre à jour si nécessaire, et les intégrer à vos projets en tant que [macros personnalisées](--reincorporer-personnalisations).

    * Lors d'un `mkdocs serve`, une erreur sera levée dans la console à chaque fois qu'une fonctionnalité non supportée est activée :

        ```code
        ERROR -  pyodide-mkdocs-theme: The macro '...' has not been maintained since the original
                   pyodide-mkdocs project, may not currently work, and will be removed in the future.
                   Please open an issue on the pyodide-mkdocs-theme repository, if you need it.

                   https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme
        ```

        Si vous devez impérativement faire en `build` en urgence, cette erreur peut être transformée en simple `warning` en passant l'option {{ config_link("build.deprecation_level") }} `à `warn`.

<br>

En cas de problèmes ou pour mettre à jour une des macros, vous pouvez demander de l'aide sur le [forum NSI-Inria][forum-inria] ou [ouvrir un ticket][pmt-issues] sur le dépôt.



## Breaking changes

Certaines fonctionnalités ne sont plus supportées :

* Les noms de dossiers et les noms des fichiers python et markdown dans la documentation ne peuvent plus contenir d'espaces, caractères accentués/spéciaux, ... Ceci permet de garantir le bon fonctionnement des IDE ou terminaux isolé.
<br>Si des caractères non autorisés sont rencontrés, une erreur est levée (voir la [section 7 pour gérer ce type d'erreur](--MAJ-fichiers-docs)).
<br>Les noms de chemins doivent être constitués des 52 lettres (majuscules et minuscules), de chiffres, points traits d'union ou underscores.

* Le [mode benchmark][benchmark] de pyodide-mkdocs, pour les tests, n'est plus supporté.

* L'argument `SIZE` des macros IDE est remplacé par l'argument `SIZE_MAX`, qui fixe la hauteur maximale d'un éditeur (en nombre de lignes).

* Le mécanisme de création automatique des messages d'erreur pour les assertions dans les IDE a été remplacé par un mécanisme plus robuste, mais plus simple.

    ??? help "À propos des nouveaux messages d'erreur automatiques pour les assertions..."

        ---8<--- "docs_tools/inclusions/IDE_assertions_feedback.md"

        {{ red("_Notez que ce changement de comportement par rapport à `pyodide-mkdocs` opère aussi sur les tests publics._") }}

        Voir également la configuration globale de [l'argument `LOGS`](--IDE-LOGS) des IDE.





## Mise à jour

Les informations données ci-dessous partent du principe que vous travaillez en local et téléversez vos modifications ensuite via `git` sur un dépôt.
<br>Si vous construisez votre site en ne travaillant qu'en ligne, via la [forge EN][forge-en]{: target=_blank } par exemple, il pourrait être plus simple de suivre le [tuto dédié créé par Mireille Coilhac][tuto-mireille]{: target=_blank }, en revenant ici pour d'éventuelles informations complémentaires.


### 1. Créer une copie de votre documentation/projet

Selon votre aisance avec les outils de contrôle de versions ou d'environnement (git, venv, ...), modifiez soit la copie, soit la version d'origine.
<br>Si vous n'êtes pas à l'aise avec tout cela, il est conseillé de garder la copie comme sauvegarde et de modifier le dossier d'origine. Gardez simplement en tête que vous aurez à finaliser la transition vers le thème avant de pouvoir refaire une mise à jour de votre site en ligne.

!!! tip "Quelques pièges à éviter"

    * Conséquence directe : ne pas démarrer la transition vers le thème le dimanche soir si vous avez des choses à mettre à jour pour le lundi matin ! :sweat_smile:

    * Si vous utilisez une nouvelle branche `git`, n'oubliez pas que les packages installés sont indépendants des branches de travail (si vous utilisez git de manière conventionnelle...). Donc travailler dans une nouvelle branche revient au même que de faire la mise à jour sans utiliser d'environnement virtuel, la branche de départ servant alors de sauvegarde du contenu projet, mais pas de l'environnement python.

    * Le travail avec un environnement virtuel n'est pas indispensable mais il facilite la transition, en permettant notamment de garder les deux versions du projet fonctionnelles. Si vous utilisez un environnement virtuel, il vous faudra par contre cloner votre projet, pour que les deux versions ne partagent pas le même environnement virtuel.





### 2. Supprimer les anciens fichiers

* Si vous avez apporté des modifications à certains éléments de pyodide-mkdocs (notamment le css ou des fonctions javascript) récupérez les fichiers concernés dans un dossier à part pour réintroduire ces changements ultérieurement.
<br>Notez cependant que les modifications apportées au JS issu de pyodide-mkdocs ne pourront plus être incorporées facilement dans le thème. Si vous êtes dans cette situation, mieux vaut [demander de l'aide sur le dépôt][pmt-repo]{: target=_blank }, ou sur le [forum de l'INRIA][forum-inria]{: target=_blank } si vous pouvez vous y inscrire.

* Supprimez ensuite tout le contenu du `custom_dir` (qui devrait s'appeler `my_theme_customizations` si vous avez gardé l'organisation d'origine).

    ??? warning "Conservez les personnalisations ajoutées dans votre `custom_dir` !"

        Si vous avez modifié le contenu du `custom_dir`, conservez ces modifications pour pouvoir les réintroduire plus tard.

        <br>Typiquement à conserver :

        * Le contenu de son sous-dossier `partials` (attention, pyodide-mkdocs-theme surcharge les fichiers suivants : {{ partials_overrides() }}. Si vous en aviez surchargés certains, il vous faudra [récupérer ceux du thème][pmt-partials]{: target=_blank } et y incorporer vos modifications).

        * Les modifications du fichier `main.html` peuvent être réintroduite __dans certaines situations__, mais il est fortement déconseillé de travailler via le fichier `main.html` avec le thème ([raisons](--override-main) / [solutions potentielles: les hooks de pyodide-mkdocs-theme](--main-html-hooks)).

        * Des fonctions JS peuvent souvent être réintroduites via des fichiers `docs/extras/javascripts/`, moyennant une éventuelle réécriture et un enregistrement dans `mkdocs.yml:extra_javascript`.

        * Les scripts chargés vias CDN peuvent en général être réintroduits dans [`mkdocs.yml:extra_javascript`](https://www.mkdocs.org/user-guide/configuration/#extra_javascript){: target=_blank }.

        Voir la section sur la [personnalisation via un custom_dir](--custom/custom_dir/).

* Supprimez le dossier `zip/my_theme_customizations` si vous l'aviez récupéré et/ou le fichier zip lui-même.

* Supprimez le fichier `main.py`, __si vous n'avez pas de macros personnalisées__.
<br>Sinon, vous pouvez garder le fichier/module là où il est, mais il vous faudra supprimer toutes les macros ayant les mêmes noms que celles définies par le thème (voir [tableau](--macros_table) en haut de cette page).

* Dans votre dossier `docs` (ou ce que vous utilisez comme `docs_dir`), supprimez ces fichiers, si présents :

<div class="annotate" markdown>

- `javascripts/config.js`
- `javascripts/mermaid.js`
- `xtra/javascripts/mathjax-config.js`
- `xtra/stylesheets/qcm.css`
- Nettoyer les sections `extra_javascript` et `extras_css` du fichier `mkdocs.yml` en supprimant les références aux fichiers ci-dessus quand elles existent.
{{ pmt_note("Si toutes les entrées sont supprimées, il faut également supprimer l'intitulé de section, `extra_javascript`{ .pmt-note }") }}

</div>.





### 3. MAJ `requirements.txt`

Il faut mettre à jour les dépendances de manière à ce que ce soit le thème qui impose les contraintes.

Si vous utilisez un fichier `requirements.txt`, l'idée est la suivante :

* {{green('Ajouter')}} `pyodide-mkdocs-theme` __en tout premier__.
* {{red('Supprimer')}} les dépendances ci-dessous si présentes dans votre fichier (le thème impose les contraintes sur ces packages) :
    - `mkdocs`
    - `mkdocs-material`
    - `mkdocs-macros-plugin`




### 4. Installer le thème

!!! tip "Rappels concernant le processus d'installation"

    Si vous êtes familiers des environnements virtuels, il est conseillé de faire la mise à jour dans un nouvel environnement.

    Si ce n'est pas votre tasse de thé, gardez simplement en tête les points suivants :

    - Il faudra finir la mise à jour complète avant de retrouver un projet fonctionnel.
    - Sans environnement virtuel, les deux versions ne peuvent pas cohabiter en étant toutes les deux fonctionnelles, car les dépendances ne sont pas exactement les mêmes (version de material et du plugin des macros, notamment).
    - Il est toujours possible de passer d'une version à l'autre, mais il faut pour cela réinstaller toutes les dépendances à chaque fois.

<br>

L'[installation](--installation/) du thème lui-même et de ses dépendances est aussi simple qu'un...

```
pip install pyodide-mkdocs-theme
```





### 5. Modifier le fichier `mkdocs.yml`

Voici ci-dessous une comparaison des sections que vous devriez trouver dans votre fichier actuel, avec ce par quoi elles devraient être remplacées après installation du thème.

{{green("__Toutes les options et les items qui apparaissent dans votre fichier d'origine et ne sont pas mentionnés ci-dessous doivent être conservés__.")}}

<br>

* ___Modifications pour la section `theme` :___

    === "Avant"

        ```yaml
        theme:
            name: material

            custom_dir: my_theme_customizations

            font: false
            language: fr

            palette:
                - media: "(prefers-color-scheme: light)"
                scheme: slate
                primary: dark blue
                accent: dark blue
                toggle:
                    icon: material/weather-night
                    name: Passer au mode jour
                - media: "(prefers-color-scheme: dark)"
                scheme: default
                primary: dark blue
                accent: dark blue
                toggle:
                    icon: material/weather-sunny
                    name: Passer au mode nuit

            features:
                - navigation.instant
        ```


    === "Après"

        ```yaml
        theme:
            name: pyodide-mkdocs-theme

        #   features:
        #       - navigation.instant    # /!\ ceci est INCOMPATIBLE avec le thème
        ```

        <br>

        * La palette de couleur est maintenant intégrée par défaut au thème.
        {{pmt_note("Si vous souhaitez en changer, vous pouvez redéclarer la section entière et vos réglages prendront le pas sur les valeurs par défaut.")}}

        * Mêmes choses pour la langue et la suppression des google-fonts (non RGPD).

        * La désactivation de `navigation.instant` est nécessaire car son fonctionnement est devenu incompatible avec l'environnement des ACE-editor, suite à la MAJ de material vers sa version 9^+^.
        {{pmt_note("Une conséquence indésirable de ceci est que l'environnement pyodide doit être rechargé à chaque chargement d'une page utilisant un terminal ou un IDE. Ceci implique des temps d'attente... Il est prévu de contourner cette limitation dans une mise à jour ultérieure du thème.")}}



<br>

* ___Modifications pour les `plugins` :___

    Là aussi, conservez tout réglage de votre fichier non discuté dans cette section.

    Si vous avez des réglages particuliers concernant les `macros`, transférez-les sans modifications dans la section `pyodide_macros`, le plugin du thème étendant lui-même celui de [mkdocs-macros-plugin][mkdocs-macros]{: target=_blank } (voir la note dans le panneau `Après` ci-dessous).

    === "Avant"

        ```yaml
        plugins:
            - search
            - macros:
                on_error_fail: true   # Vous avez peut-être vos propres réglages, ici?
            - exclude:                # Si vous utilisiez mkdocs-exclude, vous devriez pouvoir
                ...                   # le supprimer en utilisant à la place `exclude_docs`.
        ```

    === "Après"

        ```yaml
        plugins:
            - search
            - pyodide_macros:
                # Vous pouvez ajouter ici tout réglage que vous auriez ajouté concernant les macros:
                on_error_fail: true     # Il est conseillé d'ajouter celui-ci si vous ne l'utilisez pas.

        # En remplacement de mkdocs-exclude. Tous les fichiers correspondant aux patterns indiqués seront
        # exclu du site final et donc également de l'indexation de la recherche.
        # Nota: ne pas mettre de commentaires dans ces lignes !
        exclude_docs: |
            **/*_REM.md
            **/*.py
        ```

        <br>

        Le plugin `pyodide_macros` est installé avec le thème. Il contrôle les comportements des macros du thème et étend également les fonctionnalités de [`mkdocs-macros-plugin`][mkdocs-macros]{: target=_blank }.
        {{pmt_note("Si vous avez besoin d'ajouter vos propres macros au thème, ou de modifier certains comportements associés normalement au plugin `mkdocs-macros-plugin`{.pmt-note}, collez simplement vos réglages dans la section `pyodide_macros`{.pmt-note}.")}}


    <br>

    ??? tip "Pièges à éviter lors de l'enregistrement d'autres plugins"

        {{ md_include("docs_tools/inclusions/mkdocs_plugins_declaration.md") }}

<br>

* ___Modifications pour les `markdown_extensions` :___

    Il n'y a normalement pas de modifications à apporter à la section `markdown_extensions`.<br>
    Vérifiez simplement que toutes [les extensions requises](--installation/) sont bien présentes dans votre projet.





### 6. Réincorporer vos personnalisations {{anchor_redirect(id="macros-perso")}} {{anchor_redirect(id="6-reincorporer-vos-personnalisations")}} { #reincorporer-personnalisations }

* ___Les macros personnalisées :___

    <br>Le plugin `pyodide_macro` "étend" le plugin `mkdocs-macros-plugin` et fonctionne donc exactement de la même façon, avec le même mode de déclaration des macros et les mêmes options dans le fichier `mkdocs.yml` (mais sous le nom de plugin `pyodide_macros` au lieu de `macros`).

    Si vous n'avez pas supprimé le fichier `main.py` d'origine parce que vous y aviez ajouté vos macros personnalisées, vous devriez donc pouvoir le réutiliser directement avec le thème, {{red("à la seule condition de supprimer toutes les macros dont le nom correspond à une de celles définies dans le thème")}} (Le tableau en haut de cette page indique les [noms de macros existant dans le thème](--macros_table)).

    Voir aussi la page dédiée au [fonctionnement des macros personnalisées](--custom/extra_macros/), pour plus d'informations sur leur utilisation avec `pyodide-mkdocs-theme`.

<br>

* ___Les fichiers de styles ou les scripts :___

    Placez ces fichiers dans `docs/extras/` et référencer ces fichiers dans les sections `extras_css` et `extra_javascript` du fichier `mkdocs.yml`.

    Cela devrait être suffisant pour retrouver la plupart de vos anciens réglages, mais pour ce qui concerne les règles css ou les recherches d'éléments dans le DOM, il est probable qui vous ayiez besoin de modifier des noms de classes/ids, si vous aviez personnalisé les IDEs, terminaux, ... : la structure du DOM autour de ces éléments a été totalement refondue et il vous faudra donc explorer la page et modifier vos règles en conséquence.

<br>

* ___Personnalisations plus complexes :___

    Il peut être nécessaire de travailler avec un [`custom_dir`](--custom/custom_dir/) pour réintégrer certaines personnalisations.

    Notez également que le thème propose des ["crochets"](--main-html-hooks) spécifiques pour insérer facilement de la logique personnalisée dans le fichier `main.html` du thème.





### 7. Mettre à jour les fichiers de la documentation {{anchor_redirect(id="7-mettre-a-jour-les-fichiers-de-la-documentation")}} { #MAJ-fichiers-docs }

Deux types de problèmes peuvent se poser à ce stade, lors d'un `mkdocs serve` ou d'un `mkdocs build` :

* Le thème impose par défaut que les noms de dossiers et les fichiers `.py` et `.md` soient limités au jeu de caractères `[a-zA-Z0-9_.-]+`. Ceci permet de garantir le bon fonctionnement des IDE dans le site construit.

    Si cette contrainte n'est pas respectée, une erreur est levée, et il faudrait donc que vous modifiiez les noms de fichiers et dossiers en question.

    Si cela n'est pas envisageable (liens existants que vous ne souhaitez pas briser, par exemple) et que vous êtes sûrs que les noms que vous utilisez sont tout de même compatibles, vous pouvez désactiver cette vérification dans le fichier `mkdocs.yml`:

    ```yaml
    plugins:
        - pyodide_macros:
            build:
                {{ config_validator("build.skip_py_md_paths_names_validation",1)}}: true
    ```

    !!! note "Utiliser des redirections, si besoin"

        __Il est vivement déconseillé d'activer cette option, sauf le temps de faire la mise à jour vers le thème.__
        { .red }

        Si vous voulez absolument garder les anciennes adresses actives, il est toujours possible de mettre en place des redirections 301 vers les nouveaux noms de pages. Le plugin [mkdocs-redirects][mkdocs-redirects]{: target=_blank } permet de mettre en place ce type de redirections.

    À noter que ce réglage permet de passer un `mkdocs serve` ou un `build` sans erreur, mais les IDEs seront probablement non fonctionnels, ou au moins certains d'entre eux. Ce n'est donc pas une solution pour éviter de faire la mise à jour de la documentation lors du passage au thème.

<br>

* Le thème introduit diverses options de configuration relative au `build`, qui imposent certains contrats qui n'existaient pas dans `pyodide-mkdocs`. Selon la façon dont vous souhaitez utiliser les IDEs, vous pourriez vouloir désactiver certaines de ces options, notamment :

    - {{ config_link("ides.forbid_secrets_without_corr_or_REMs") }}
    - {{ config_link("ides.forbid_hidden_corr_and_REMs_without_secrets") }}
    - {{ config_link("ides.forbid_corr_and_REMs_with_infinite_attempts") }}



### 8. Annuler les réglages spécifiques à la mise à jour

Une fois que tout marche à nouveau, ne pas oublier de supprimer les réglages temporaires.


```yaml
plugins:
    - pyodide_macros:
        build:
            {{ config_validator("build.skip_py_md_paths_names_validation",1)}}: false  # supprimer cette ligne uniquement, ou toute
                                                      # la section `build` si c'est la seule option
```

<br>
