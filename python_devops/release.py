
import json
import re

from myutils import CodeModifier, docstring_teller
from myutils.terminal import os_system

from python_devops.config import *





def mkdocs_yml_updater(prop:str):

    pattern = re.compile(f"{ prop }:[^\\n]+")

    def wrapper(repl:str, source:str=None):
        update_file = source is None

        if update_file:
            source = MKDOCS.read_text(encoding='utf-8')
        source = pattern.sub(f"{ prop }: { repl }", source)
        if update_file:
            MKDOCS.write_text(source, encoding='utf-8')

        return source

    return wrapper


set_is_pipeline_to = mkdocs_yml_updater('is_pipeline')
set_plugin_off_to  = mkdocs_yml_updater('plugin_off')





@docstring_teller
def update_files_from_material_async():
    """
    Updates json schemas, overrides in partials, and add short licence to all files.
    (running async code in the context of the .venv, using a system process)
    """
    os_system('.venv/bin/python python_devops/releases/async_release_setup_material_updates_in_venv.py')




@docstring_teller
def set_js_logger_to(that:bool):
    """ Update js-logger-libs.js ACTIVATE value. """
    print('    -> set to ', that)

    was = None

    def update_logger(m):
        nonlocal was
        was = json.loads(m[2])
        return f'{ m[1] }{ json.dumps(that) }'

    CodeModifier(JS_LOGGER_LIBS).re_sub_once(r'(ACTIVATE:\s*)(\w+)', update_logger).dump()
    return was



@docstring_teller
def make_sure_mkdocs_yml_is_correctly_configured_for_release(skip_exclusions_check=False):
    """ Check the mkdocs.yml config is compatible with a release. """

    code = MKDOCS.read_text(encoding='utf-8')

    if not skip_exclusions_check and '# **/*.md' not in code:
        raise ValueError("Global exclusions in mkdocs.yml:exclude_docs are not deactivated!")

    code = set_is_pipeline_to('true', code)
    code = set_plugin_off_to('false', code)

    MKDOCS.write_text(code, encoding='utf-8')
