

import re
import json
from myutils import CodeModifier, docstring_teller

from python_devops.config import *



@docstring_teller
def export_repo_url_from_yml_to_maestro_base():
    """
    Update the pmt_url in BaseMaestro if needed
    """
    repo_url = CodeModifier(MKDOCS).extract_between('repo_url:', '\n').strip("'\" ")
    maestro  = CodeModifier(MAESTRO)
    pmt_url  = maestro.extract_between('pmt_url:str', "\n", archive=True).strip(("'\" ="))

    if pmt_url != repo_url:
        maestro.replace_current(f" = '{ repo_url }'\n").dump()
        print('Updated to: ', repo_url)




# @docstring_teller
# def set_mkdocs_yml_dev_mode_to(that:bool):
#     """ Update plugins.pyodide_macros._dev_mode """
#     print("   -> Set to:", that)

#     CodeModifier(
#         MKDOCS
#     ).re_sub_once(
#         r'_dev_mode:\s*\w+', lambda *_: f"_dev_mode: { json.dumps(that) }"
#     ).dump()
