# pylint: disable=all

from pathlib import Path
from typing import Union
from pyodide_mkdocs_theme.__main__ import PMT_SCRIPTS
from .docs_dirs_config_source import *

def inclusion_with_lang_factory(file):
    def inclusion_getter(lang:str='.') -> Path:
        return INCLUSIONS / lang / file
    return inclusion_getter



def write_file(target:Path, src:Union[str,Path], as_bytes=0):
    """
    Write @src (or its content if it's a Path object) to the @target location,
    handling all the tedious logic:

    * read/write as text or bytes automatically)
    * handles encoding arguments for text
    * apply target.touch(exist_ok=True) automatically
    """

    if isinstance(src,Path):
        if as_bytes:
            src = src.read_bytes()
        else:
            src = src.read_text(encoding='utf-8')

    target.touch(exist_ok=True)

    if as_bytes:
        target.write_bytes(src)
    else:
        target.write_text(src, encoding='utf-8')



GIT_LAB_PAGES = "https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/"
MATERIAL_RAW  =  'https://raw.githubusercontent.com/squidfunk/mkdocs-material/master'


MKDOCS               = Path('mkdocs.yml')
PYODIDE_PLOT         = Path('pyodide_plot.py')
TOOLBOX              = Path('toolbox.py')
YAML_SCHEMAS         = Path('yaml_schemas')
PLUGIN_YAML_SCHEMA   = YAML_SCHEMAS / 'pyodide-macros-schema.json'
PMT                  = Path('pyodide_mkdocs_theme')
PMT__MAIN__          = PMT / '__main__.py'



INCLUSIONS           = Path('docs_tools/inclusions')
DOCS_QCMS_INCLUSION  = inclusion_with_lang_factory('qcms_working_ex.md')
DOCS_CONF_INCLUSION  = inclusion_with_lang_factory('config_plugin.md')



PYODIDE_MACROS       = PMT / "pyodide_macros"
PYM_MESSAGES         = PYODIDE_MACROS / 'messages'
PYM_PARSING          = PYODIDE_MACROS / 'parsing.py'
PYM_JS_TEMPLATES     = PYODIDE_MACROS / "scripts_templates.py"
MAESTRO              = PYODIDE_MACROS / 'plugin' / 'maestro_base.py'
PYM_PAGE_IDES_CONFIG = PYODIDE_MACROS / 'plugin'  / 'maestro_IDE.py'
PYM_DOCS_DIRS_CONFIG = PYODIDE_MACROS / 'plugin'  / 'config' / 'definitions' / 'docs_dirs_config.py'

THEME_SCRIPTS        = PMT / PMT_SCRIPTS
SCRIPTS_MODELS       = THEME_SCRIPTS / "models"


CUSTOM_DIR           = PMT / "templates"
TEMPLATES_PARTIALS   = CUSTOM_DIR / 'partials'
MAIN_HTML            = CUSTOM_DIR / 'main.html'
JS_CONFIG_LIBS       = CUSTOM_DIR / "js-libs" / "0_config-libs.js"
JS_FUNCTOOLS_LIBS    = CUSTOM_DIR / "js-libs" / "1_functools-libs.js"
JS_LOGGER_LIBS       = CUSTOM_DIR / "js-libs" / "jsLogger-libs.js"
JS_0_PYTHON_SNIPPETS = CUSTOM_DIR / "js-per-pages" / "0_genericPythonSnippets-pyodide.js"
JS_2_PYTHON_RUNNER   = CUSTOM_DIR / "js-per-pages" / "2_pyodideSectionsRunner-pyodide.js"

DEVOPS_DOCS_DIRS     = Path('python_devops') / 'docs_dirs_config_source.py'
