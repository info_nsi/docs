

from dataclasses import dataclass
import aiofiles
import aiohttp
import asyncio

from typing import Awaitable, Callable, List
from pathlib import Path


from python_devops.config import *
from python_devops.releases import material_partials_updates as partials
import python_devops.releases.material_yaml_schema_updates as schemas
import python_devops.releases.licences as licences
from python_devops.releases.mkdocs_hooks import align_config_codes_and_docs

# pylint: disable=all






async def http_updater(
    session: aiohttp.ClientSession,
    src_url: Path,
    local: Path,
    converter: Callable[[str],str],
    blocking: bool,
    reason:str,
):
    print(f"Update { local }")

    async with session.get(src_url) as resp:                            # extract file from url
        code = await resp.text()

    async with aiofiles.open(local, mode='r', encoding='utf-8') as f:   # read local file and compare
        current = await f.read()

    updated:str = converter(code)                                       # modify file
    if blocking and updated != current:                                 # store flag if needed
        RAISE.append(f"[{ reason }] { local } has been updated.")

    async with aiofiles.open(local, mode='w', encoding='utf-8') as f:   # write new version
        await f.write(updated)





async def file_updater(
    file: Path,
    converter: Callable[[str],str],
    blocking: bool,
    reason: str
):
    async with aiofiles.open(file, mode='r', encoding='utf-8') as f:   # read local file and compare
        content = await f.read()

    updated = converter(content)

    if content != updated:
        msg = f"[{ reason }] { file } has been updated."
        async with aiofiles.open(file, mode='w', encoding='utf-8') as f:   # write new version
            await f.write(updated)

        if blocking:
            RAISE.append(msg)
        else:
            print(msg)






async def async_release_updater():

    async with aiohttp.ClientSession() as session:

        session.headers.update({
            "Host": "raw.githubusercontent.com",
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:129.0) Gecko/20100101 Firefox/129.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/png,image/svg+xml,*/*;q=0.8",
            "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br, zstd",
            "Referer": "https://github.com/squidfunk/mkdocs-material/blob/master",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "cross-site",
            "Sec-Fetch-User": "?1",
            "Priority": "u=0, i",
            "Pragma": "no-cache",
            "Cache-Control": "no-cache",
        })


        TODO: List[Awaitable] = [

            schemas.schemas_updater(  'schema',    http_updater, session),
            schemas.schemas_updater(  'theme',     http_updater, session),
            schemas.schemas_updater(  'plugins',   http_updater, session),

            partials.partials_updater('content',   http_updater, session),
            partials.partials_updater('header',    http_updater, session),
            partials.partials_updater('footer',    http_updater, session),
            partials.partials_updater('copyright', http_updater, session),
            partials.partials_updater('social',    http_updater, session),

            *( licences.short_licence_adder(file, file_updater) for file in licences.TODO_FILES )
        ]
        await asyncio.gather(*TODO)

        print('\nDone with async stuff...\n\n')

        if RAISE:
            raise ValueError(
                f"\nThese files have been changed: check the git revisions!\n"
                + "\n".join(RAISE) + '\n'
            )


RAISE = []








if __name__ == '__main__':
  asyncio.run( async_release_updater() )
  align_config_codes_and_docs(True)

else:
    raise ImportError(
        "This file should never be imported: it should be run through os_system, using"
        "the .venv python interpreter."
    )
