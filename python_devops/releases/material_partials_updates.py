
from pathlib import Path
import re
from typing import Awaitable, Callable

import aiohttp

from python_devops.config import MATERIAL_RAW, TEMPLATES_PARTIALS



GIT_PARTIALS_TEMPLATE = f'{ MATERIAL_RAW }/material/templates/partials/{ "{name}" }.html'



def indent(lvl, s:str):
    return s.replace('\n', '\n' + 2*lvl * ' ')



def plugin_prefixer(code:str):
    """
    Replace material prefixes with pyodide-mkdocs-theme.
    """
    return re.sub(
        "material/(search|tags)",
        r"pyodide-mkdocs-theme/\1",
        code
    )


def footer_updater(html:str):
    """
    Remove the social section of the footer (moved to the copyright).
    """
    return html.replace("""
      {% if config.extra.social %}
        {% include "partials/social.html" %}
      {% endif %}""",  "")


def copyright_updater(html:str):
    """
    Restructuring entirely the footer, inserting also the social part.

    Initial structure is:

            START_DIV
              COPYRIGHT_BLOC
              CENTER
                MAT
              END

    Output is:

            BEGIN
              <div class="md-copyright_pyodide">
                COPYRIGHT_BLOCK
                social_insertion
              </div>
              CENTER
                <div class="md-copyright_pyodide">
                  <div>
                    MAT
                  </div>
                  footer_insertion
                </div>
              END

    """

    # Chunks from source (all parts present for presentational purpose):
    __begin, copyright_bloc, __center, mat, __end = (
"""\
<div class="md-copyright">""",
"""
  {% if config.copyright %}
    <div class="md-copyright__highlight">
      {{ config.copyright }}
    </div>
  {% endif %}""",
"""
  {% if not config.extra.generator == false %}""",
"""
    Made with
    <a href="https://squidfunk.github.io/mkdocs-material/" target="_blank" rel="noopener">
      Material for MkDocs
    </a>""",
"""
  {% endif %}
</div>
"""
)

    # Chunks to insert:
    social_insertion = """
    {% if config.extra.social %}
      {% include "partials/social.html" %}
    {% endif %}"""

    footer_insertion = """
      <div id="pyodide-footer">
        <a href="{{ config.plugins.pyodide_macros.pmt_url }}" target="_blank" rel="noopener">
          Pyodide MkDocs Theme
        </a> v.{{ config.plugins.pyodide_macros.version }}
      </div>"""


    upper = f"""
  <div class="md-copyright_pyodide">{ indent(1, copyright_bloc) }{ social_insertion }
  </div>"""

    lower = f"""
    <div class="md-copyright_pyodide">
      <div>{ indent(2, mat) }
      </div>{ footer_insertion }
    </div>"""

    return html.replace(copyright_bloc, upper).replace(mat, lower)



def social_updater(html:str):
    """
    Update social.html...
    """
    target = """<a href="{{ social.link }}" target="_blank" rel="{{ rel }}" title="{{ title | e }}" class="md-social__link">
      {% include ".icons/" ~ social.icon ~ ".svg" %}
    </a>"""
    return html.replace(target, f"<p>{ target }</p>")



NAME_TO_UPDATER = {
    'content':   plugin_prefixer,
    'header':    plugin_prefixer,
    'footer':    footer_updater,
    'copyright': copyright_updater,
    'social':    social_updater,
}



def partials_updater(
    name: str,
    coroutine_factory: Callable[
        [aiohttp.ClientSession, Path, Path, Callable, bool, str],
        Awaitable
    ],
    session: aiohttp.ClientSession,
) -> Awaitable :

    src     = GIT_PARTIALS_TEMPLATE.format(name=name)
    local   = TEMPLATES_PARTIALS / f'{ name }.html'
    updater = NAME_TO_UPDATER[name]
    return coroutine_factory(session, src, local, updater, True, 'partials')
