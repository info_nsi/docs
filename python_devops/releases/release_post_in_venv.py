
from python_devops.releases.mkdocs_hooks import transfer_PM_IdeConfig_properties_as_js_PyodideSectionsRunner_getters


if __name__ == '__main__':
    transfer_PM_IdeConfig_properties_as_js_PyodideSectionsRunner_getters(False)

else:
    raise ImportError(
        "This file should never be imported: it should be run through os_system, using"
        "the .venv python interpreter."
    )
