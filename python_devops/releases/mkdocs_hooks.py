"""
mkdocs events:  https://www.mkdocs.org/dev-guide/plugins/#events

All the functions in this files involve using logistic coming from the PMT package, which imports
stuff from the .venv, so these operations CANNOT be done through the devops hooks (which work
from a global python interpreter).
"""
# pylint: disable=all

import re
import json
from typing import Dict, List, Union
from pathlib import Path
from collections import defaultdict

from mkdocs.exceptions import BuildError
from mkdocs.plugins import event_priority
from mkdocs.config.defaults import MkDocsConfig

import python_devops.config as DEV_CONF
from python_devops.config import *
from pyodide_mkdocs_theme.pyodide_macros.plugin.config import MkdocstringsPageDocsDumper

from pyodide_mkdocs_theme.pyodide_macros.messages import Lang
from pyodide_mkdocs_theme.pyodide_macros.pyodide_logger import get_plugin_logger
from pyodide_mkdocs_theme.pyodide_macros.parsing import camel, eat, replace_chunk #, MEAN
from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import Kinds, ScriptKind, ICONS_IN_TEMPLATES_DIR
from pyodide_mkdocs_theme.pyodide_macros.plugin import (
    BaseMaestroGettersDumper,
    ConfigTreeInclusionDumper,
    PLUGIN_CONFIG_SRC,
    PluginConfigSrc,
    PyodideMacrosPlugin,
    IdeConfig,
)



logger = get_plugin_logger("DEV-pyodide", color=31)   # in RED



_ICONS = CUSTOM_DIR / ICONS_IN_TEMPLATES_DIR
if not _ICONS.is_dir():
    raise BuildError(f"{ _ICONS } does not exist.")

MAESTRO_GETTERS_TOKEN = "\n    # ARGS EXTRACTOR TOKEN"

JS_CONFIG_DUMP  = "//JS_CONFIG_DUMP"
INSERTION_TOKEN = "<!-- PYODIDE - insertion token -->"

BASE_URL = '{{ config.plugins.pyodide_macros.rebase(base_url) }}'

CODE_INSERTIONS = {
    '.js':  """<script type="application/javascript" src="{BASE_URL}/{rel_path}"></script>""",
    '.css': """<link rel="stylesheet" href="{BASE_URL}/{rel_path}">""",
}

RAISE = []





def write_with_feedback_if_needed(
    path:Path,
    content:str,
    src_content:str=None,
    raise_on_update:bool=False
) -> bool:
    """
    Update the content of the given @path file if @content differs from @scr_content.

    If @scr_content isn't given, use the current content of the file as source instead.

    If @raise_on_update is True and the file is updated, BuildError will be raised (this is
    generally used when the updated content makes the current running implementation different
    from the written code).
    Otherwise, returns a bool telling if the file got updated or not.

    WARNING: raising won't be done right away, actually, so that all the critical updates can
    be done. So a flag is set, and the error will be raised only at the end of the hook.
    """
    if src_content is None:
        src_content = path.read_text(encoding='utf-8')

    if content != src_content:
        logger.info(f"[hooks] - { path } updated")
        path.write_text(content, encoding=('utf-8'))
        if raise_on_update:
            RAISE.append(f"{ path } code has been modified.")
        return True
    return False





event_priority(3000)
def on_config(config:MkDocsConfig):
    # roughly 30ms => no need to "optimize" that with async stuff...
    if config.extra['is_pipeline']:
        return

    logger.info("Refresh project")
    is_dev = config.plugins['pyodide_macros']._dev_mode

    align_config_codes_and_docs(False)




def align_config_codes_and_docs(is_release_setup:bool):

    RAISE.clear()

    # Update python
    validate_all_config_paths()
    copy_docs_dirs_devops_src_to_plugin_config()
    rebuild_base_maestro_config_extractors_getters()
    handle_langs_related_logic()
    gather_all_scripts_and_css_kinds()

    # Update JS
    transfer_lzw_data()
    rebuild_js_0_config_libs_CONFIG_model()
    transfer_PM_IdeConfig_properties_as_js_PyodideSectionsRunner_getters(is_release_setup)
    transfer_PyodidePlot_to_js_generics_snippets()

    # Update docs
    rebuild_dev_docs_summary()
    add_PyodideMacrosConfig_to_docs_custom_config_md()
    rebuild_PyodideMacrosConfig_yaml_schema()

    if RAISE and not is_release_setup:
        raise BuildError(
            "You need to restart serving so that the runtime will actually be up to date:\n  "
            +'\n  '.join(RAISE)
        )
    elif RAISE:
        print(
            "Files modified:\n  "
            +'\n  '.join(RAISE)
        )





# @event_priority(-2000)
# def on_page_context(ctx, page, **_):
#     print(page.content)


# def on_serve(*_,**__):
#     init,encoded = map(sum,zip(*MEAN))
#     print("\nMEAN:")
#     print(len(MEAN))
#     print(f"{init} -> {encoded} => x{encoded/init:.3f}")




def validate_all_config_paths():
    print("Validate all the paths in python_devops.config")

    invalid = []
    for data in dir(DEV_CONF):
        path = getattr(DEV_CONF, data)
        if isinstance(path, Path) and not path.exists():
            invalid.append(f'\n{data} = Path("{path}")')
    if invalid:
        raise FileNotFoundError(f"Missing files :" + "".join(invalid))





def copy_docs_dirs_devops_src_to_plugin_config():
    write_file(PYM_DOCS_DIRS_CONFIG, DEVOPS_DOCS_DIRS, as_bytes=1)





def rebuild_base_maestro_config_extractors_getters():

    code    = MAESTRO.read_text(encoding='utf-8')
    getters = BaseMaestroGettersDumper.apply(PLUGIN_CONFIG_SRC)
    token   = MAESTRO_GETTERS_TOKEN
    updated = replace_chunk(code, token, token, getters, keep_limiters=True)

    write_with_feedback_if_needed(MAESTRO, updated, code, raise_on_update=True)







def handle_langs_related_logic():
    __make_sure_all_xx_lang_files_are_imported_in_init()
    __check_all_lang_classes_properties_equal()
    __check_custom_messages_has_all_Lang_props()





def __make_sure_all_xx_lang_files_are_imported_in_init():
    """
    Logic:

    - The languages are seen in `Lang.get_lang_dct` only if the subclasses are created.
    - The classes are created only if the
    """

    langs = sorted(
        name.stem.split('_')[0] for name in PYM_MESSAGES.glob('*_lang.py')
                                if not name.stem.startswith('fr')  # already imported
    )
    imports = '\n    '.join(
        f"from .{ lang }_lang import Lang{ lang.title() }" for lang in langs
    )

    init = PYM_MESSAGES / '__init__.py'
    code = init.read_text(encoding='utf-8')
    i    = code.index('def __enforce_definitions_but_hidden_classes')
    repl = f"""\
def __enforce_definitions_but_hidden_classes():
    { imports }

__enforce_definitions_but_hidden_classes()
"""
    write_with_feedback_if_needed(init, code[:i]+repl, code, raise_on_update=True)






def __check_all_lang_classes_properties_equal():

    def diff(ref, other, lang):
        a = ref-other
        if a: bad.append(f"{lang} is missing: { ', '.join(a) }")

        b = other-ref
        if b: bad.append(f"{lang} shouldn't contain: { ', '.join(b) }")

    dct = Lang.get_langs_dct()
    ref = set(Lang.__annotations__)
    bad = []
    for lang,obj in dct.items():
        diff(ref, set(obj.__class__.__annotations__), lang)

    if bad:
        raise BuildError("Wrong LangXx declaration(s):\n  " + '\n  '.join(bad))





def __check_custom_messages_has_all_Lang_props():

    expected = set(Lang.__annotations__)
    done     = set()
    messages = DOCS_MESSAGES.read_text(encoding="utf-8")

    i = 0
    while (i:=messages.find("pyodide_mkdocs_theme.pyodide_macros.messages.Lang", i)) >= 0:
        i = messages.find('members', i)
        j = messages.find('\n\n', i)
        matches = re.findall(r"(?<=- )\w+", messages[i:j])
        done.update(matches)

    missing = expected - done
    if missing:
        missing_s = ''.join( f'\n    - {s}' for s in missing )
        raise BuildError(f'Missing Lang message properties in { DOCS_MESSAGES }:{ missing_s }')







def gather_all_scripts_and_css_kinds():

    find_block_tokens_in_main_pattern = re.compile(
        r"\{% block \w+ %\}|\{% endblock %\}|"+INSERTION_TOKEN
    )
    main_block_opening_pattern = re.compile(
        r"\{% block (?P<block_name>\w+) %\}"
    )

    got_js_config = 0

    def get_named_html_block(name:Union[str,ScriptKind]):
        """ Rebuild a starting block of the main.html file """
        return "{% block " + str(name) + " %}"

    def get_code_to_insert(path:Path) -> str :
        nonlocal got_js_config
        rel_path = path.relative_to(CUSTOM_DIR)
        uri_rel_path = str(rel_path).replace('\\', '/')         # Windows compatibility
        code = CODE_INSERTIONS[ path.suffix ].format(
            BASE_URL=BASE_URL,
            rel_path=uri_rel_path
        )
        if path.stem == JS_CONFIG_LIBS.stem:
            got_js_config += 1
            code += '\n\n{{ config.plugins.pyodide_macros.dump_to_js_config(base_url) }}\n'
        return code


    def build_insertions_string(paths:List[Path]):
        """
        Take a list of Paths (.js or .css) and build the full string of scripts and links tags to
        insert the content of the related files in a html document.
        """
        insertions = [*map( get_code_to_insert, paths)]
        code_repl = '\n'.join(insertions)
        return code_repl


    def build_main_blocks_dict_and_pages_dict():
        """
        Build the base dict of blocks, gathering all kind of blocks in main.html that contains
        an INSERTION_TOKEN.
        This will allows to empty blocks that are not used anymore (for example, when if a
        script/css file got moved somewhere else, or renamed).
        """
        main_code = MAIN_HTML.read_text(encoding='utf-8')
        blocks_data:    Dict[str,List[Path]] = defaultdict(list)
        per_pages_data: Dict[str,List[Path]] = defaultdict(list)

        # Gather all jinja blocks in ain.html that contains an INSERTION_TOKEN:
        markers = find_block_tokens_in_main_pattern.findall(main_code)
        for current, follow, third in zip(markers, markers[1:]+[''], markers[2:]+['']*2):

            if re.search(r'block \w+', current) and follow==INSERTION_TOKEN:
                block_name = main_block_opening_pattern.match(current)['block_name']
                blocks_data[block_name] = []

                if third!=INSERTION_TOKEN:
                    raise ValueError(
                        "Invalid jinja block in main.html: isolated {INSERTION_TOKEN!r} found."
                    )

        # Explore all css and js files checking they all have a "target" tail and registering
        # their path:
        css_and_js_files = [*CUSTOM_DIR.rglob('*.css')] + [*CUSTOM_DIR.rglob('*.js')]
        for path in css_and_js_files:
            kind_name = path.stem.split('-')[-1]
            kind: ScriptKind = getattr(Kinds, kind_name, None)

            if not kind:
                logger.warning(
                    f"[building { MAIN_HTML.name }] a '{ path.suffix }' file without identifier name "
                    f"has been found:\n   { path }"
                )
            elif kind.for_block:
                blocks_data[kind_name].append(path)

            elif kind.for_page_ctx:
                per_pages_data[kind_name].append(path)

            else:
                pass    # ignored


        # Sort the insertions in each block in lexicographic order of their filename (this allows to
        # chose the scripts order definitions, for example, to define all the globals first):
        def js_first_then_by_name(p:Path):
            return p.suffix=='.css', p.name

        for lst in blocks_data.values():
            lst.sort(key=js_first_then_by_name)

        return blocks_data, per_pages_data



    def rebuild_main_html_and_update_if_needed(main_blocks_data:Dict[str,List[Path]]):
        main_code = MAIN_HTML.read_text(encoding='utf-8')
        fresh_main = main_code
        for block_name, paths in main_blocks_data.items():

            code_repl = build_insertions_string(paths)
            html_block = get_named_html_block(block_name)
            i_start_block, _ = eat(fresh_main, html_block)

            fresh_main = replace_chunk(
                fresh_main, INSERTION_TOKEN, INSERTION_TOKEN, f"\n{ code_repl }\n",
                at=i_start_block, keep_limiters=True
            )

        write_with_feedback_if_needed(MAIN_HTML, fresh_main, main_code)



    def dump_per_page_templates_as_py_file(per_pages_data:Dict[str,List[Path]]):
        dct = {kind: build_insertions_string(paths) for kind, paths in sorted(per_pages_data.items())}
        rebuilt = f'""" Generated file, do not modify """\n\nSCRIPTS_TEMPLATES = { json.dumps(dct, indent=4) }'
        write_with_feedback_if_needed(PYM_JS_TEMPLATES, rebuilt)



    #-------------------------------------------------------------------


    main_blocks_data, per_pages_data = build_main_blocks_dict_and_pages_dict()

    rebuild_main_html_and_update_if_needed(main_blocks_data)
    assert got_js_config==1, (
        f"Should have handled the {JS_CONFIG_LIBS.stem}.js file 1 time, but was: {got_js_config}"
    )
    dump_per_page_templates_as_py_file(per_pages_data)








def transfer_lzw_data():

    def extract(name):
        _,i = eat(code, name)
        j,_ = eat(code, '\n', start=i)
        data = code[i:j].strip(' =')
        if data[0] != data[-1] or len(data)<2 or data[0] not in "\"'":
            raise ValueError(f"Invalid {name} string for LZW compression. Found:\n{data}")
        return data

    def replace(name, data):
        nonlocal repl
        repl = replace_chunk(repl, f"const {name} = ", "\n", data, keep_limiters=True)

    code    = PYM_PARSING.read_text(encoding='utf-8')
    code_js = JS_FUNCTOOLS_LIBS.read_text(encoding='utf-8')
    repl    = code_js

    no_html = extract('NO_HTML')
    replace("NO_HTML", no_html)

    alpha   = extract('TOME_BASE')
    replace("ALPHA", alpha)

    write_with_feedback_if_needed(JS_FUNCTOOLS_LIBS, repl, code_js)





def rebuild_js_0_config_libs_CONFIG_model():
    js_file = JS_CONFIG_LIBS
    code_js = js_file.read_text(encoding='utf-8')
    repl    = PyodideMacrosPlugin.dump_to_js_config(None,None)      # HACK!
    fresh   = replace_chunk(
        code_js, JS_CONFIG_DUMP, JS_CONFIG_DUMP, repl+"\n   ", keep_limiters=True
    )
    write_with_feedback_if_needed(js_file, fresh, code_js)





def transfer_PM_IdeConfig_properties_as_js_PyodideSectionsRunner_getters(is_pipeline:bool):

    js_file  = JS_2_PYTHON_RUNNER
    py_props = set(IdeConfig.__annotations__)

    # Update JS getters in PyodideSectionsRunner!
    op, clo = '{}'
    getter_template = (
        "\n  get {call: <20}{op} return this.data.{prop} {clo}"
            if is_pipeline else
        "\n  get {call: <20}{op} return this.no_undefined('{camel}')(this.data.{prop}) {clo}"
    )
    getters = ''.join(
        getter_template.format(call=camel(p)+'()', camel=camel(p), prop=p, op=op, clo=clo)
        for p in sorted(py_props) # if p != "corr_content"
    )+"\n  "

    code_js = js_file.read_text(encoding='utf-8')
    fresh   = replace_chunk(code_js, JS_CONFIG_DUMP, JS_CONFIG_DUMP, getters, keep_limiters=True)
    write_with_feedback_if_needed(js_file, fresh, code_js)




def transfer_PyodidePlot_to_js_generics_snippets():

    mocking = 'from unittest.mock import Mock\njs = Mock()'
    pyodide = 'import js\nmatplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")'
    token   = "# THIS IS A PLOT TOKEN"
    js_file = JS_0_PYTHON_SNIPPETS
    plot    = PYODIDE_PLOT.read_text(encoding='utf-8')

    if mocking not in plot:
        raise BuildError("Couldn't find the mocking jay in pyodide_plot.py")

    plot     = plot.replace(mocking, pyodide)
    plot     = f"\n{ plot }\n".replace("`", "\\`").replace('\n', '\n    ')
    plot     = re.sub(r'^ +$', '', plot, flags=re.MULTILINE)
    src_code = js_file.read_text(encoding='utf-8')
    fresh    = replace_chunk(src_code, token, token, plot,keep_limiters=True)

    write_with_feedback_if_needed(js_file, fresh, src_code)






def rebuild_dev_docs_summary():

    def build_link(file:Path):
        """
        Assuming use_directory_urls = True
        """
        rel_loc = file.relative_to(DEV_DOCS_INDEX.parent)

        if rel_loc.stem == 'index':
            link_name = rel_loc.parent
            link_uri  = rel_loc.parent
        else:
            link_name = rel_loc
            link_uri  = rel_loc.with_suffix('')

        link = f"* [{ link_name }](./{ link_uri }/)"
        return link


    links = sorted(
        build_link(file) for file in DEV_DOCS_INDEX.parent.rglob("*.md")
                         if not file.stem.endswith('REM') and file != DEV_DOCS_INDEX
    )

    write_with_feedback_if_needed( DEV_DOCS_INDEX, "\n".join(links) )







def add_PyodideMacrosConfig_to_docs_custom_config_md():
    """ Dump the full plugin config in the docs config files """

    # Handle the docs_tools/inclusions file (config tree hierarchy of defaults)
    #--------------------------------------------------------------------------

    inclusion_file = DOCS_CONF_INCLUSION()
    output = ConfigTreeInclusionDumper.apply(PLUGIN_CONFIG_SRC)
    write_with_feedback_if_needed(inclusion_file, output)



    # Update the entire config md page:
    #----------------------------------

    token1 = "<br INSERTION_TOKEN>"
    token2 = "<br INSERTION_TOKEN_2>"
    token3 = "<br INSERTION_TOKEN_3>"

    md_source = DOCS_CONFIG.read_text(encoding='utf-8')


    # Build all the sub-configs (not recursive, for `args`):
    markdown = []
    elements = sorted(PLUGIN_CONFIG_SRC.elements, key=MkdocstringsPageDocsDumper.ordering)
    for src in elements:
        if src.in_yaml_docs and src.is_config:
            recurse =  src.name != 'args'
            md = MkdocstringsPageDocsDumper.apply(src, recurse=recurse)
            markdown.append( md + "\n\n<br><br>\n\n{{sep()}}\n\n" )

    markdown  = "\n\n" + ''.join(markdown)
    md_config = replace_chunk(md_source, token1, token2, markdown, keep_limiters=True)


    # Build the source level, with the original macros-plugin properties:
    options = PluginConfigSrc(
        docs = PLUGIN_CONFIG_SRC.docs,
        elements = tuple(elt for elt in elements if not elt.is_config)
    )
    markdown2 = "\n\n" + MkdocstringsPageDocsDumper.apply(options, limit_depth=2) + '\n\n'
    md_config = replace_chunk(md_config, token2, token3, markdown2, keep_limiters=True)


    # Merge updates...
    write_with_feedback_if_needed(DOCS_CONFIG, md_config, md_source)





def rebuild_PyodideMacrosConfig_yaml_schema():
    """
    Dump the full plugin config in the docs config file
    """
    output = PLUGIN_CONFIG_SRC.to_yaml_schema(GIT_LAB_PAGES)
    write_with_feedback_if_needed(PLUGIN_YAML_SCHEMA, output)
