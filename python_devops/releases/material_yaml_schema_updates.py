
from pathlib import Path
import re
import json
from typing import Awaitable, Callable

import aiohttp

from python_devops.config import MATERIAL_RAW, YAML_SCHEMAS


GIT_SCHEMAS         = f'{ MATERIAL_RAW  }/docs/schema'
GIT_SCHEMA_RAW      = f'{ GIT_SCHEMAS }.json'
GIT_SCHEMA_TEMPLATE = f'{ GIT_SCHEMAS }/{ "{name}" }.json'




def schema_schema_updater(code:str):
    """
    Update the main `schema.json` file, keeping theme and plugins schemas local, while
    outsourcing other $refs to mkdocs-material repo.

    NOTE: If changed to use json logic, be careful about the markdown_extensions schema which is
          named `extensions;json` only...
    """
    return re.sub(
        r'"\$ref": "schema/(\w+)[.]json"',
        lambda m: f'"$ref": "{ m[1] }.json"' if m[1] in ('theme', 'plugins') else
                  f'"$ref": "{ GIT_SCHEMAS }/{ m[1] }.json"',
        code
    )


def theme_schema_updater(code:str):
    """
    Update `theme.json`, replacing all "material" hardcoded values to PMT name, and outsourcing
    $refs to "assets" to mkdocs-material repo.
    """
    out = re.sub(
        r'"(const|default)":\s*"material"',
        r'"\1": "pyodide-mkdocs-theme"',
        code,
        count=2,
    )
    out =  re.sub(
        r'"[$]ref":\s*"(assets/\w+\.json)"',
        f'"$ref": "{ GIT_SCHEMAS }/\\1"',
        out
    )
    return out


def plugins_schema_updater(code:str):
    """
    Update `plugins.json`, adding PyodideMacrosPlugin ref in external plugins, and outsourcing
    other $refs to the "plugins" sub-directory to mkdocs-material repo.
    """

    # Replace all refs to plugins with links to raw GitHub:
    updated =  re.sub(
        r'"[$]ref":\s*"plugins/',
        f'"$ref": "{ GIT_SCHEMAS }/plugins/',
        code
    )

    # Insert PyodideMacroPlugin external ref:
    obj = json.loads(updated)
    arr: list = obj['$defs']['external-community']['anyOf']
    arr.append(
        { "$ref": "pyodide-macros-schema.json" }
    )
    return json.dumps(obj, indent=2)




NAME_TO_UPDATER = {
    'schema':  schema_schema_updater,
    'theme':   theme_schema_updater,
    'plugins': plugins_schema_updater,
}



def schemas_updater(
    name: str,
    coroutine_factory: Callable[
        [aiohttp.ClientSession, Path, Path, Callable, bool],
        Awaitable
    ],
    session: aiohttp.ClientSession,
) -> Awaitable :

    template = GIT_SCHEMA_TEMPLATE if name!= 'schema' else GIT_SCHEMA_RAW
    src      = template.format(name=name)
    local    = YAML_SCHEMAS / f'{ name }.json'
    updater  = NAME_TO_UPDATER[name]
    return coroutine_factory(session, src, local, updater, False, 'yaml schema')
