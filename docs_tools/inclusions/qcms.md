
### Utilisation

Il suffit de cocher ses réponses, et quand on a terminé, on clique sur le bouton de vérification : le compteur de réponses correctes s'affiche alors.

!!! note
    * Un qcm devient inactif une fois l'évaluation déclenchée avec {{ btn("check",'span') }} : cliquer sur les items ne modifie plus les choix.
    * Une réponse est comptée bonne si tous les choix corrects, et seulement ceux-ci, ont été cochés pour une question.
    * Si le qcm a un masque juste au-dessus du bouton de vérification, les points seront comptabilisés, mais les réponses correctes ou fausses ne seront pas affichées.
    * Il est possible de recommencer le QCM en cliquant sur le bouton {{ btn("restart", "span") }}


### Légendes

| Bouton/icône | Description |
|:-:|:-|
| {{ btn("check") }} | Évalue et corrige le qcm (pas de correction si le masque est affiché).<br>Après avoir clické sur ce bouton, le qcm est inactif. Il peut alors être réactivé avec le bouton suivant : |
| {{ btn("restart") }} | Recommencer le qcm : Efface les réponses, le compteur, et réactive l'ensemble. Mélange les questions et les items, si l'argument `shuffle` est `True`. |
| {{ qcm_mask(35) }} | Si le masque est affiché au-dessus du bouton de validation, les réponses sont évaluées mais pas corrigées : seul le compteur de bonnes réponses apparaît. |

![qcm corrigé](!!qcm_correction_png){: loading=lazy .w35 align=right }

| Type<br>choix | Corrigé | Élément | Description |
|:-:|:-:|:-:|:-|
| multiple | non | {{ qcm_svg("multi unchecked") }}  | non choisie par l'utilisateur |
| multiple | non | {{ qcm_svg("multi checked") }}    | choisie par l'utilisateur |
| multiple | oui | {{ qcm_svg("multi correct") }}    | correcte, choisie par l'utilisateur |
| multiple | oui | {{ qcm_svg("multi incorrect") }}  | incorrecte, choisie par l'utilisateur |
| multiple | oui | {{ qcm_svg("multi missed") }}     | correcte mais non choisie |
| unique   | non | {{ qcm_svg("single unchecked") }} | non choisie par l'utilisateur |
| unique   | non | {{ qcm_svg("single checked") }}   | choisie par l'utilisateur |
| unique   | oui | {{ qcm_svg("single correct") }}   | correcte, choisie par l'utilisateur |
| unique   | oui | {{ qcm_svg("single incorrect") }} | incorrecte, choisie par l'utilisateur |
| unique   | oui | {{ qcm_svg("single missed") }}    | correcte mais non choisie |


En résumé :

* Les carrés pour les QCMs : l'utilisateur peut en choisir autant qu'il veut.
* Les ronds pour les QCSs : chaque nouveau choix annule le précédent.
* En {{ gray("gris") }} : non corrigé.
* En {{ green("vert") }} : correct.
* En {{ red("rouge") }} : incorrect.



### En action...

{{ md_include('docs_tools/inclusions/qcms_working_ex.md') }}