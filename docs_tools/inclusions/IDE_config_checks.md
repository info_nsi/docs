* Selon la [configuration de la section `ides`](--pyodide_macros_ides) du plugin `pyodide_macros`, des contraintes peuvent être imposées ou non sur l'existence de certaines sections ou fichiers par rapport à d'autres.

    Si toutes les options sont laissées par défaut, {{red("une erreur est levée")}} dans les cas suivants :


    | `secrets` { style=width:8em } | `corr` ou REMarques { style=width:8em } | [`IDE(..., MAX=...)`](--IDE-MAX) { style=width:8em } | Option de configuration correspondant |
    |:-:|:-:|:-:|:-|
    | {{qcm_svg("multi correct")}}   | {{qcm_svg("multi incorrect")}} |     /    | {{ config_link("ides.forbid_secrets_without_corr_or_REMs") }} |
    | {{qcm_svg("multi incorrect")}} | {{qcm_svg("multi correct")}}   |     /    | {{ config_link("ides.forbid_hidden_corr_and_REMs_without_secrets") }} |
    | {{qcm_svg("multi correct")}}   | {{qcm_svg("multi correct")}}   | $\infty$ | {{ config_link("ides.forbid_corr_and_REMs_with_infinite_attempts") }} |
