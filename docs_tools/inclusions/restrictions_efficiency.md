La logique des interdictions fonctionne bien du moment que le contexte d'exécution est restreint.<br>
En l'occurrence, les restrictions appliquées à un IDE sont valables pour son éditeur et son terminal.

En revanche tout autre IDE ou terminal apparaissant dans la même page est un autre point d'entrée vers l'environnement `pyodide` qui, rappelons-le, _est commun à toute la page_.<br>
Ceci a pour conséquences que :

- Si un IDE comporte des restrictions, tous les IDE de la page doivent les avoir aussi.
- Même contrainte pour les terminaux isolés, qui doivent également porter les mêmes restrictions.

Les [fichiers {{meta()}}](--meta-pmt-yml-files) ou les métadonnées dans les [entêtes des fichiers markdown](--pages-meta) peuvent être mis à profit pour régler ce type de paramètres pour un groupe de fichiers ou une page entière.
