La configuration du plugin `pyodide_macros` comporte, outre les réglages du plugin lui-même, les valeurs par défaut pour la quasi totalité des macros proposées par le thème.

Ces réglages peuvent être modifiés de différentes façons pour affecter un fichier, un dossier ou une hiérarchie de dossiers :

1. Via les métadonnées placées dans [les entêtes des pages markdown](--pages-meta) elles-mêmes (nécessite d'activer l'extension markdown `meta`).
1. Via [des fichiers {{meta()}}](--custom/metadata/), qui peuvent être placés n'importe où dans la hiérarchie de la documentation et qui affectent tous les fichiers "descendants".
1. Via [le fichier `mkdocs.yml`](--custom/config/) du thème, bien sûr (dans la section `plugins.pyodide_macros`).
