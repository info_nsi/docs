
??? warning "Pour les versions de `pyodide-mkdocs-theme` antérieures à la `v2.2.0`"

    Pour les versions strictement antérieures à la `2.2.0`, les plugins issus de [mkdocs-material][mkdocs-material]{: target=_blank } devaient être préfixés en leur ajoutant `material/` lors de leur enregistrement dans `mkdocs.yml:plugins` :

            material/blog
            material/group
            material/info
            material/offline
            material/privacy
            material/search
            material/social
            material/tags

    <br>

    À partir de la version `2.2.0`, c'est l'inverse : ces plugins doivent maintenant être enregistrés sans préfixes ou ils ne fonctionneront plus correctement.

    !!! note "Si la notion de dossier `partials` vous parle, vous pourriez être également concernés par ceci :"

        À partir de la même version, PMT utilise 2 nouvelles surcharges de fichiers de material, dans `custom_dir/partials`. Si vous avez surchargé l'un des fichiers ci-dessous, il vous faudra aller récupérer [les versions utilisée dans PMT][pmt-partials]{: target=_blank } por y inclure vos modifications.

            content.html
            header.html

        Pour rappel, les autres fichiers également surchargés par le thème sont :

            copyright.html
            footer.html
            social.html