Tous les plugins ne s'ignorent pas les uns les autres, et il peut parfois y avoir des interactions indésirables lors de leur enregistrement, voire des plantages durant un build/serve s'ils sont enregistrés dans un ordre inapproprié. Le problème principal étant que c'est en général à l'utilisateur de découvrir quel est le bon ordre...

Voici quelques informations glanées ici et là concernant les enregistrements des plugins.

| Plugin | Contrainte d'enregistrement |
|-|-|
| `awesome-pages` | Toujours en premier (en fait, avant `tags`) |
| `search` | Toujours en premier (ou juste après `awesome-pages` / plugin de `material`) |
| `tags` | Toujours après `awesome-pages` / plugin de `material` |
| [`sqlite-console`][sqlite-console]{: target=_blank } | Toujours après `search` et `pyodide_macros` |
| `exclude-search` | Toujours après `search` |
