Toutes les sections sont optionnelles, mais pour être considéré valide, un fichier python doit respecter les règles suivantes :

* Les noms des sections doivent être ceux décrits dans le [tableau en haut de cette page](--ide-sections). Tout autre nom lèvera une erreur.
* Chaque section ne peut être déclarée qu'une fois (sauf la section `ignore`, qui peut être réutilisée autant de fois que nécessaire).
* Quand une section est définie, elle ne peut pas être vide. Si vous ne souhaitez pas utiliser une section, supprimez son contenu ainsi que le commentaire annonçant cette section, ou bien ajoutez un commentaire dans la section en question.
* L'ordre des sections dans le fichier n'a techniquement aucune importance.