```yaml
plugins:
    - pyodide_macros:
        args:
            IDE:
                SANS: ""
                WHITE: ""
                REC_LIMIT: -1
                MERMAID: false
                MAX: 5
                LOGS: true
                MODE: null
                MIN_SIZE: 3
                MAX_SIZE: 30
                TERM_H: 10
    
            terminal:
                SANS: ""
                WHITE: ""
                REC_LIMIT: -1
                MERMAID: false
                TERM_H: 10
                FILL: ""
    
            py_btn:
                SANS: ""
                WHITE: ""
                REC_LIMIT: -1
                ICON: ""
                HEIGHT: null
                WIDTH: null
                SIZE: null
                TIP: "Exécuter le code"
                TIP_SHIFT: 50
                TIP_WIDTH: 0.0
                WRAPPER: "div"
                MERMAID: false
    
            multi_qcm:
                description: ""
                hide: false
                multi: false
                shuffle: false
                shuffle_questions: false
                shuffle_items: false
                admo_kind: "!!!"
                admo_class: "tip"
                qcm_title: "Question"
                tag_list_of_qs: null
                DEBUG: false
    
            figure:
                div_id: "figure1"
                div_class: "py_mk_figure"
                inner_text: "Votre tracé sera ici"
                admo_kind: "!!!"
                admo_class: "tip"
                admo_title: "Votre figure"
    
        build:
            deprecation_level: "error"
            encrypted_js_data: true
            forbid_macros_override: true
            ignore_macros_plugin_diffs: false
            load_yaml_encoding: "utf-8"
            macros_with_indents: []
            meta_yaml_encoding: "utf-8"
            python_libs: ["py_libs"]
            skip_py_md_paths_names_validation: false
            tab_to_spaces: -1
    
        ides:
            deactivate_stdout_for_secrets: true
            decrease_attempts_on_user_code_failure: "editor"
            encrypt_alpha_mode: "direct"
            encrypt_corrections_and_rems: true
            forbid_corr_and_REMs_with_infinite_attempts: true
            forbid_hidden_corr_and_REMs_without_secrets: true
            forbid_secrets_without_corr_or_REMs: true
            show_only_assertion_errors_for_secrets: false
    
        qcms:
            forbid_no_correct_answers_with_multi: true
    
        terms:
            cut_feedback: true
            stdout_cut_off: 200
    
        include_dir: ""
        include_yaml: []
        j2_block_end_string: ""
        j2_block_start_string: ""
        j2_variable_end_string: ""
        j2_variable_start_string: ""
        module_name: "main"
        modules: []
        on_error_fail: false
        on_undefined: "keep"
        render_by_default: true
        verbose: false
```
