1. Ajouter un fichier `main.py` à la racine de votre propjet (_PAS_ dans le `docs_dir` !)
1. Y déclarer une fonction `define_env(env:PyodideMacrosPlugin)`, dans laquelle déclarer vos macros :

```python title="Exemple de fichier `main.py`"
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

def define_env(env:PyodideMacrosPlugin):

    @env.macro
    def macro1(...) -> str :
        return ...
```