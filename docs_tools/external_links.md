[pmt-pypi]:             https://pypi.org/project/pyodide-mkdocs-theme/ "Le thème sur PyPI"
[pmt-repo]:             https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme "Le dépôt sur Gitlab"
[pmt-issues]:           https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme/-/issues "Ouvrir un ticket sur le dépôt de pyodide-mkdocs-theme"
[pmt-templates]:        https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme/-/tree/main/pyodide_mkdocs_theme/templates?ref_type=heads "Le dossier du custom dir défini par Pyodide-MkDocs-Theme"
[pmt-partials]:         https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme/-/tree/main/pyodide_mkdocs_theme/templates/partials?ref_type=heads "Les fichiers surchargés par le thème, dans le custom_dir (dépôt PMT)."

[PyPI]:                 https://pypi.org/ "Python Packages Index"
[python]:               https://www.python.org/ "python - home"
[mkdocs]:               https://www.mkdocs.org/ "mkdocs - home"
[mkdocs-local]:         https://www.mkdocs.org/user-guide/deploying-your-docs/#local-files "Build a local version of the documentation"

[mkdocs-material]:      https://squidfunk.github.io/mkdocs-material/ "mkdocs-material - home"
[material-custom]:      https://squidfunk.github.io/mkdocs-material/customization/ "Explanations about theme customization on mkdocs-material"
[material-templates]:   https://github.com/squidfunk/mkdocs-material/tree/master/src/templates "Le dossier du custom dir défini par material-mkdocs"
[material-schemas]:     https://squidfunk.github.io/mkdocs-material/creating-your-site/#configuration "Utilisation des schémas de validation de mkdocs-material, pour le fichier mkdocs.yml"

[mkdocs-macros]:        https://mkdocs-macros-plugin.readthedocs.io/en/latest/ "mkdocs-macros - home"
[macros-meta]:          https://mkdocs-macros-plugin.readthedocs.io/en/latest/post_production/#html-templates "Utiliser les meta données dans les macros"
[macros-include_dir]:   https://mkdocs-macros-plugin.readthedocs.io/en/latest/advanced/#changing-the-directory-of-the-includes
[macros-include_yaml]:  https://mkdocs-macros-plugin.readthedocs.io/en/latest/advanced/#declaring-external-yaml-files
[macros-pluglets]:      https://mkdocs-macros-plugin.readthedocs.io/en/latest/pluglets/ "Writting pluglets"

[mkdocs-redirects]:     https://github.com/mkdocs/mkdocs-redirects "mkdocs-redirects repository"
[sqlite-console]:       https://epithumia.github.io/mkdocs-sqlite-console/usage/ "SQL queries dans mkdocs"

[Pyodide]:              https://pyodide.org/en/stable/ "Pyodide, Python with the scientific stack, compiled to WebAssembly"
[pyodide-modules]:      https://pyodide.org/en/stable/usage/wasm-constraints.html "Détails sur les modules disponibles, ou non dans l'environnement de Pyodide"
[jq-terminal]:          https://terminal.jcubic.pl/api_reference.php "jQuery terminal: integrated terminals in a website"
[ACE-editor]:           https://ace.c9.io/ "High performance code editor for the web"
[showPicker]:           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/showPicker#browser_compatibility "Navigateurs compatibles"
[typesMimes]:           https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types "Les types MIME, sur MDN."

[forum-inria]:          https://mooc-forums.inria.fr/moocnsi/t/pyodide-mkdocs-theme-le-theme-utilise-sur-codex/11393 "Le file dédié sur le forum des enseignants de NSI"
[forge-en]:             https://portail.apps.education.fr: "La forge des communs numériques de l'Éducation Nationale"
[tuto-mireille]:        https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/maj/mise_a_jour_theme_pyodide/ "Tuto pour passer de pyodide-mkdocs au thème, en travaillant uniquement en ligne"
[tuto-turtle]:          https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/bibs_graphiques/bib_images/#ii-utiliser-la-tortue-par-romain-janvier "Utiliser un émulateur turtle dans Pyodide."
[pyodide-mkdocs]:       https://bouillotvincent.gitlab.io/pyodide-mkdocs/ "pyodide-mkdocs: le projet original de Vincent Bouillot"
[benchmark]:            https://bouillotvincent.gitlab.io/pyodide-mkdocs/install_ide/#prise-en-main "Le mode de tests benchmark de pyodide-mkdocs"

[numpy]:                https://numpy.org/ "The fundamental package for scientific computing with Python"
[matplotlib]:           https://matplotlib.org/ "Matplotlib: Visualization with Python"
[mermaid]:              https://mermaid.js.org/ "Mermaid - Diagramming and charting tool"
[package-python]:       https://docs.python.org/3/tutorial/modules.html#packages "Documentation sur python.org, concernant les packages"
[mocks]:                https://docs.python.org/3/library/unittest.mock.html "unittest.mock documentation"