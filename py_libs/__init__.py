print('installed py_libs')

def auto_N(globs, var='N', reset_on=None):
    global GLOBS,n,VAR
    if var not in globs:
        GLOBS = globs
        VAR   = var
        globs[VAR] = 0
    if reset_on is not None and globs[VAR] >= reset_on:
        globs[VAR] = 0
    globs[VAR] = n = globs[VAR]+1



def do_it(ok=None, reset=False):
    global n
    if reset: GLOBS[VAR] = 0
    if reset or not (n := n-1):
        if ok is not None:
            print(f"case {GLOBS[VAR]}: should { 'pass' if ok else 'FAIL' }")
        return True
    return False



def should_raise(func, kind=AssertionError, msg=""):
    print("Test against", kind.__name__, ': ', msg, end='\n\t')
    e = '"Did not raise..."'
    try:
        func()
    except kind:
        print(f'Raised', kind.__name__)
        return
    except Exception as exc:
        e = exc
    assert False, f'Should have raised {kind.__name__} but was {e} (type={type(e)})'


def not_defined(name:str, explain=""):
    assert name not in GLOBS, f"{name!r} shouldn't be defined/accessible"
    if explain:
        print(explain)


def not_importable(name:str):
    should_raise(lambda: __import__(name), ExclusionError, "pydantic shouldn't be importable")


def available(that):
    assert that, str(that)
    that = str(that)
    print('Available:', that[:80] + '...' * (len(that) > 80))


import sys

class StdOutAsserter:
    def __init__(self):
        self.store = []
    def gather(self):
        self.store.append(sys.stdout.getvalue())
    def check(self,exp, remove_spaces=False):
        act = ''.join(self.store)
        if remove_spaces:
            act = ''.join(c for c in act if not c.isspace())
        assert act==exp, f'{act!r} should be {exp!r}'
        print('StdOut test : OK!')