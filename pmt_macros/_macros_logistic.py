


from typing import List
from pyodide_mkdocs_theme.pyodide_macros.plugin.pyodide_macros_plugin import PyodideMacrosPlugin



class Macros(dict):

    md_include_path: List[str]
    env: PyodideMacrosPlugin = None

    def __init__(self):
        super().__init__()
        self.md_include_path = []


    def set_env(self, env):
        self.env = env

    def get_env(self) -> PyodideMacrosPlugin :
        return self.env




    def register(self, env: PyodideMacrosPlugin):
        self.env = env
        for macro in self.values():
            env.macro(macro)




    def render_inner_macros(self, content, disk_location='<unknown>', src=None):

        env = self.env
        need_indents = env.is_macro_with_indent()

        if need_indents:
            indentations = env._parser.parse(content, disk_location)
            env._indents_store += [*reversed(indentations)]

        template = env.env.from_string(content)
        if src is not None:
            self.md_include_path.append(src)
        try:
            rendered = template.render(**env.variables)
        finally:
            if src is not None:
                self.md_include_path.pop()

        indent = need_indents and env.get_macro_indent()
        if indent:
            rendered = rendered.replace('\n', '\n' + indent)
        return rendered



MACROS = Macros()



def global_macro(name:str=None):

    def decorator(func):
        macro_name = name or func.__name__
        func.__qualname__ = func.__name__ = macro_name
        MACROS[macro_name] = func
        return func

    if callable(name):
        name, func = None, name
        return decorator(func)
    return decorator
