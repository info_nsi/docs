
import re
from pathlib import Path
from typing import Callable
from mkdocs.exceptions import BuildError

from pmt_macros._macros_logistic import MACROS, global_macro
from python_devops.releases.licences import LICENSE_COMMENTS

# pylint: disable=missing-function-docstring, invalid-name

md_include_path = MACROS.md_include_path
render_inner_macros = MACROS.render_inner_macros





def _include(
    src:    str,
    *,
    before: Callable[[str],str] = None,
    after:  Callable[[str],str] = None,
):
    """ Generic md_include logistic """

    if src in md_include_path:
        raise BuildError(
            'Cyclic md_include on path:' + ''.join(f'\n   {p}' for p in md_include_path)
        )

    path     = Path(src)
    disk_loc = str( MACROS.env.docs_dir_cwd_rel / path )
    content  = Path(path).read_text(encoding='utf-8')

    if before:
        content = before(content)
    rendered = render_inner_macros(content, disk_loc, src)
    if after:
        rendered = after(rendered)

    return rendered


global_macro('md_include')(_include)






@global_macro
def md_include_rm_license(file:str):
    path    = Path(file)
    content = path.read_text(encoding='utf-8')

    license = LICENSE_COMMENTS[path.suffix]         # pylint: disable=redefined-builtin
    if content.startswith(license):
        content = content[len(license):]

    indent = MACROS.env.get_macro_indent()
    return content.replace('\n', '\n'+indent)






def _page_include_factory(
    macro_name:str,
    src:str,
    *,
    before:Callable=None,
    after:Callable=None,
):
    @global_macro(macro_name)
    def page_include():
        return _include(src, before=before, after=after)




def _increase_title_lvl_after(rendered:str) -> str:
    """ Add one '#' to each title in a markdown content """
    rendered = re.sub(r'^#+', lambda m: '#' * (len(m[0])+1), rendered, flags=re.M)
    return rendered


def _repl_install_before(md):
    """ Post process all html ids """
    out = re.sub(r'(#[\w-]+?)-install', r'\1', md)
    return out

def _update_IDE_refs_and_increase_IDs_before(content:str) -> str:
    content = re.sub(r'\s*\{\s*#tests-', ' { #IDEs-', content)
    content = re.sub(r',\s*ID=\d+', lambda m:f'{m[0]}1', content)
    return content



_page_include_factory( 'include_installation',
    "docs/installation.md",
    before = _repl_install_before,
    after  = _increase_title_lvl_after
)

_page_include_factory( 'IDE_tests_to_details',
    "docs/redactors/IDE-tests.md",
    before = _update_IDE_refs_and_increase_IDs_before,
    after  = _increase_title_lvl_after
)
