
from functools import lru_cache
from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros.plugin import (
    ARGS_MACRO_CONFIG,
    MacroConfigSrc,
    PLUGIN_CONFIG_SRC,
)

from pmt_macros._macros_logistic import MACROS, global_macro

# pylint: disable=missing-function-docstring, invalid-name, line-too-long


render_inner_macros = MACROS.render_inner_macros




@global_macro
def Env():
    return MACROS.env





def _get_config_data(option_path:str, tail:int):
    yaml_path  = PLUGIN_CONFIG_SRC.get_plugin_path(option_path, no_deprecated=True)
    identifier = yaml_path.replace('.','_')
    if tail:
        option_path = ''.join( option_path.split('.')[-tail:] )
    return option_path, identifier


@global_macro
@lru_cache(None)
def config_link(option_path:str, tail:int=0):
    """
    To make sure no outdated links/paths are used n the docs (get_plugin_path will implicitly
    validate de path).
    Declared here so that the cache is global to a serve and not just a build.

    @tail: takes the tail last elements of the option_path as text link (full it tail=0)
    """
    option_path, identifier = _get_config_data(option_path, tail)
    return f"[`{ option_path }`](--{ identifier })"


@global_macro
@lru_cache(None)
def config_validator(option_path:str, tail:int=0):
    """
    Same idea than config_link, but just validate the path and return the bare string
    (handling tail).
    """
    option_path, _ = _get_config_data(option_path, tail)
    return option_path






@global_macro
def macro_signature(config_name):
    config: MacroConfigSrc = ARGS_MACRO_CONFIG.subs_dct[config_name]
    return config.signature_for_docs()



@global_macro
def macro_args_table(
    config_name,            # Class name
    with_headers=False,     # Add the headers on top of the table rows
    with_text=False,        # add generic text before the table (specific, see documentation... :p )
    only:str=None,          # Argument name: if given create a table with headers and only this argument
    width1:float = None,
    width2:float = None,
    with_meta_info: bool = True,
):
    width = MACROS.env.macros['width']     # accessed at runtime only, to avoid any import order troubles

    config: MacroConfigSrc = ARGS_MACRO_CONFIG.subs_dct[config_name]
    table  = config.as_docs_table() if only is None else config.subs_dct[only].as_table_row()

    if with_headers:
        op,clo  = '{}'
        content = """\
- Tous les arguments sont optionnels.
- À part pour le premier, déclarer les arguments en précisant leur nom (donc sous forme d'arguments nommés).

<br>

""" * with_text + f"""
| Argument { width(width1, 9) } | Type/défaut { width(width2, 9) } | Rôle |
|-|-|-|
{ table }
""" + f"""
<br>

{op+op} md_include("docs_tools/inclusions/config_defaults_info.md") {clo+clo}
""" * with_meta_info

    elif only:
        content = f"""\
| Type { width(width1, 8) } | Défaut { width(width2, 0) } | Résumé |
|-|-|-|
{ table }"""

    rendered = render_inner_macros(content)
    return rendered






@global_macro
def meta_tests_config(**expected_dct):
    """
    For dev_docs "testing"
    """
    names = '''
        args_IDE_LOGS
        args_IDE_MAX
        python_libs
        deactivate_stdout_for_secrets
        decrease_attempts_on_user_code_failure
        show_only_assertion_errors_for_secrets
    '''.split()

    table = ['\n<br>\n\n## Configuration PMT vue depuis ce fichier','','| name | value | expected |', '|:-|:-:|:-:|']
    wrongs = []
    for name in names:
        exp = expected_dct[name]
        actual = getattr(MACROS.env, name)
        fmt = ''
        if exp!=actual:
            fmt = '{.red}'
            wrongs.append((actual,exp))
        table.append(f'| `{ name }` | `#!py { actual }`{fmt} |`#!py { exp }` |')
    table.append('\n<br>\n\n')

    if wrongs:
        msg = ''.join( f"\n    {actual} should be {exp}" for actual,exp in wrongs)
        raise BuildError(f"Wrong meta config in {MACROS.env.file_location()}:{msg}")
    else:
        print(f'Meta config test: \033[32mPASS\033[0m ({ MACROS.env.file_location() })')

    return '\n'.join(table)
