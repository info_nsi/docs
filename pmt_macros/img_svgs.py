import re
from pathlib import Path
from functools import lru_cache

from pyodide_mkdocs_theme.pyodide_macros.html_builder._html_builder import _BTN_STDOUT_SVG
from pyodide_mkdocs_theme.pyodide_macros.plugin.config._string_tools import inline_md

from ._macros_logistic import global_macro

# pylint: disable=line-too-long, missing-function-docstring


DIV_BTN_STYLE  = 'style="width:100%;display:flex;justify-content:center;margin:0"'
SPAN_BTN_STYLE = 'style="display:unset;justify-content:center;margin:0"'

SVG_MARKERS = inline_md("""
<svg class="qcm" viewBox="0 0 12 12" role="img" version="1.1" xmlns="http://www.w3.org/2000/svg"
        xmlns:svg="http://www.w3.org/2000/svg" style="stroke-width:1.25;stroke-linecap:round">
    <path class="bgd-svg" style="fill:var(--qcm-fill);stroke:none;"
            d="M 5.93,1.93 3.29,2.40 2.70,2.75 1.86,5.70 2.38,8.76 2.75,9.29 5.82,10.13 9.07,9.45 9.36,9.13 10.12,6.11 9.49,2.93 9.12,2.65 Z"></path>
    <path class="tick" style="display:var(--tick);fill:var(--qcm-light);stroke:var(--qcm-light);stroke-width:0;stroke-linecap:butt;stroke-linejoin:round"
            d="M 6.34,8.49 C 6.49,7.32 7.07,5.36 9.05,4.06 L 8.93,3.91 C 7.13,4.50 6.38,5.52 5.63,7.03 5.36,6.61 3.91,5.92 3.47,5.86 L 3.32,6.00 C 4.41,6.54 5.06,7.30 5.63,8.77"></path>
    <g style="display:var(--cross);fill:var(--qcm-light);stroke:var(--qcm-light)"
        transform="matrix(0.91,0,0,0.91,0.52,0.52)">
        <rect width="8.33" height="0.59" x="-5.86" y="8.02" transform="rotate(-56.54)"></rect>
        <rect width="8.33" height="0.59" x="-12.47" y="-1.99" transform="matrix(-0.55,-0.83,-0.83,0.55,0,0)"></rect>
    </g><g style="fill:none;stroke:var(--qcm-border)">
        <circle style="display:var(--circle)" cy="6" cx="6" r="4.2"></circle>
        <rect style="display:var(--square)" class="square" width="7.41" height="7.36" x="2.29" y="2.32"></rect>
    </g>
</svg>
"""
)

SVG_MASK = inline_md("""
<svg width={width} version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 130 512 250" xml:space="preserve">
    <path style="fill:var(--md-primary-fg-color)"
        d="M508.692,167.211c-14.625,0.984-64.391,5.859-128.781,5.859c-90.734,0-96.594,32.188-123.922,32.188 c-31.203,0-29.25-32.188-123.906-32.188c-64.391,0-114.156-4.875-128.781-5.859c-10.531-0.703,3.906,103.406,46.828,142.438 c32.5,29.547,111.234,54.656,172.703,13.656c23.422-18.516,33.156-18.516,33.156-18.516s9.781,0,33.188,18.516 c61.453,41,140.203,15.891,172.688-13.656C504.786,270.617,519.239,166.508,508.692,167.211z M202.442,255.305 c-45.656,50.922-120.297,15.797-121.172-35.141C165.552,207.008,205.755,251.602,202.442,255.305z M309.552,255.305 c-3.313-3.703,36.875-48.297,121.172-35.141C429.849,271.102,355.224,306.227,309.552,255.305z">
    </path>
</svg>
"""
)


TRASH_SVG = inline_md("""
<svg height="20px" version="1.1" id="trash-can-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" xml:space="preserve" fill="#ffffff">
    <g>
        <path d="M88.594,464.731C90.958,491.486,113.368,512,140.234,512h231.523c26.858,0,49.276-20.514,51.641-47.269 l25.642-335.928H62.952L88.594,464.731z M420.847,154.93l-23.474,307.496c-1.182,13.37-12.195,23.448-25.616,23.448H140.234 c-13.42,0-24.434-10.078-25.591-23.132L91.145,154.93H420.847z"></path>
        <path d="M182.954,435.339c5.877-0.349,10.35-5.4,9.992-11.269l-10.137-202.234c-0.358-5.876-5.401-10.349-11.278-9.992 c-5.877,0.357-10.35,5.409-9.993,11.277l10.137,202.234C172.033,431.231,177.085,435.696,182.954,435.339z"></path>
        <path d="M256,435.364c5.885,0,10.656-4.763,10.656-10.648V222.474c0-5.885-4.771-10.648-10.656-10.648 c-5.885,0-10.657,4.763-10.657,10.648v202.242C245.344,430.601,250.115,435.364,256,435.364z"></path>
        <path d="M329.046,435.339c5.878,0.357,10.921-4.108,11.278-9.984l10.129-202.234c0.348-5.868-4.116-10.92-9.993-11.277 c-5.877-0.357-10.92,4.116-11.277,9.992L319.054,424.07C318.697,429.938,323.17,434.99,329.046,435.339z"></path>
        <path d="M439.115,64.517c0,0-34.078-5.664-43.34-8.479c-8.301-2.526-80.795-13.566-80.795-13.566l-2.722-19.297 C310.388,9.857,299.484,0,286.642,0h-30.651H225.34c-12.825,0-23.728,9.857-25.616,23.175l-2.721,19.297 c0,0-72.469,11.039-80.778,13.566c-9.261,2.815-43.357,8.479-43.357,8.479C62.544,67.365,55.332,77.172,55.332,88.38v21.926h200.66 h200.676V88.38C456.668,77.172,449.456,67.365,439.115,64.517z M276.318,38.824h-40.636c-3.606,0-6.532-2.925-6.532-6.532 s2.926-6.532,6.532-6.532h40.636c3.606,0,6.532,2.925,6.532,6.532S279.924,38.824,276.318,38.824z"></path>
    </g>
</svg>
"""
)
TRASH_SVG_BTN = f'<div style="width:40px;height:40px;padding:10px; background-color:var(--md-primary-fg-color);">{ TRASH_SVG }</div>'


STDOUT_SVG_BTN_STRIKE = f'<div { DIV_BTN_STYLE[:-1] };height:30px;padding:4px; background-color:var(--md-default-bg-color);">{ _BTN_STDOUT_SVG }</div>'
STDOUT_SVG_BTN_FULL   = f'<div { DIV_BTN_STYLE[:-1] };height:30px;padding:4px; background-color:var(--md-default-bg-color);">{ _BTN_STDOUT_SVG.replace("var(--md-default-bg-color)", "transparent") }</div>'





@global_macro
@lru_cache(None)
def qcm_svg(classes:str):
    return f'<div class="{ classes } no-hover" {DIV_BTN_STYLE}>{ SVG_MARKERS }</div>'

@global_macro
@lru_cache(None)
def qcm_mask(width):
    return SVG_MASK.format(width=width)

@global_macro
def trash_svg():
    return TRASH_SVG_BTN

@global_macro
def stdout_svg(strike=True):
    return STDOUT_SVG_BTN_STRIKE if strike else STDOUT_SVG_BTN_FULL



@global_macro
@lru_cache(None)
def btn(name, in_tag='div'):
    """
    Build one of the button found bellow IDEs
    """
    style = DIV_BTN_STYLE if in_tag == 'div' else SPAN_BTN_STYLE
    return (
        f'<{ in_tag } markdown class="tooltip header-btn" { style }>'
        f'<img src="!!icons8-{name}-64_png" alt="{name}" class="skip_light_box"/>'
        f'</{ in_tag }>'
    )


@global_macro
@lru_cache(None)
def div_svg(
    svg_path:str,
    *,
    kls:str="",
    padding=0.5,
    style:str="",
    center=False,
    **attrs
):
    """
    Formatted div with a single svg file inside (ex: runtime schema).
    """
    style = f'padding:{padding}em;' + style
    attrs = {
        **({'class':kls} if kls else {}),
        'style': style,
        **attrs,
    }
    attrs_str = ' '.join( f'{p}="{s}"' for p,s in attrs.items() )
    code = Path(svg_path).read_text(encoding='utf-8')
    code = code.replace('class="{placeholder}"', attrs_str, 1)
    if center:
        code = f'<div class="img center">{code}</div>'
    out = re.sub(r"\s*?\n\s*", " ", code)
    return out
